<?php
/**
* @package		SP Upgrade
* @subpackage	Components
* @copyright	SP CYEND - All rights reserved.
* @author		SP CYEND
* @link		http://www.cyend.com
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*/
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');
JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_spupgrade'.DS.'tables');

class SPUpgradeController extends JController
{
    public function display($cachable = false, $urlparams = false)
    {
        //$url = JURI::base().'components/com_spupgrade/log.htm';
        //echo ('<a href="'.$url.'" target="_blank">Monitor Log</a>');

        $jAp = & JFactory::getApplication();
        $db16 =& JFactory::getDBO();
        $view	= JRequest::getWord('view', 'spupgrade');
        $layout = JRequest::getWord('layout', 'default');
        $id		= JRequest::getInt('id');				
        $post		= JRequest::getVar('jform', array(), 'post', 'array');

        if (count($post) > 0) {

            //monitor log
            $message= '<META HTTP-EQUIV="REFRESH" CONTENT="15">';
            $this->writeLog($message, 'w'); // create monitor log
            
            //check for MySQLi
            if (!$this->checkDbType(JPATH_SITE.'/configuration.php')) return false;

            //check site path validity
            $pathValidation = TRUE;
            $fileConfiguration = $post['db_path'].'/configuration.php';
            if (!file_exists($fileConfiguration)) {
                $pathValidation = FALSE;
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_INVALIDPATH1', $post['db_path']).'</p>';
                $this->writeLog($message);         
            }

            if ($pathValidation) {
                $mBool = FALSE;
                $strLength = strlen($post['db_name']);
                $handleConfiguration = @fopen($fileConfiguration, "r");
                while (($buffer = fgets($handleConfiguration, 4096)) !== false) {
                    if (strpos($buffer, "\$db") > 0 ) {
                        $pos = strpos($buffer,"'") + 1;
                        if ( strcmp(substr($buffer, $pos ,$strLength), $post['db_name']) == 0 ) {         
                            $mBool = TRUE;  
                        }
                    }
                }
                fclose($handleConfiguration);

                if (!$mBool) {
                    $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_INVALIDPATH2', $post['db_path']), 'error');
                    $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_INVALIDPATH2', $post['db_path']).'</font></b></p>';
                    $this->writeLog($message);
                    return false;
                }
            }

            //Login to old Database		  
            $db15 = mysql_connect($post[db_host], $post[db_user], $post[db_pass]);
            if (!$db15) {
                $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_DB1', mysql_error()), 'error');
                $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_DB1', mysql_error()).'</font></b></p>';
                $this->writeLog($message);  
                return false;      
            }      

            if (!mysql_select_db($post[db_name], $db15)) {
                $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_DB2', mysql_error()), 'error');
                $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_DB2', mysql_error()).'</font></b></p>';
                $this->writeLog($message);
                return false;
            }

            //character set
            $sql    = "SHOW VARIABLES LIKE 'character_set_database'";
            $result = mysql_query($sql, $db15);
            if (!$result) {
                $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_DB3', mysql_error()), 'error');
                $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_DB3', mysql_error()).'</font></b></p>';
                $this->writeLog($message);
                return false;
            }
            $row = mysql_fetch_assoc($result);
            //mysql_set_charset($row['Value'], $db15);
            mysql_set_charset('utf8', $db15);

            $message= ('<div align=left>');
            $this->writeLog($message);
            $message = '<h1>'.JText::_('COM_SPUPGRADE_MSG_TITLE').'</h1>';
            $urlLog = JURI::base().'components/com_spupgrade/log.htm';
            $message .= '<p><em>'.JText::_('COM_SPUPGRADE_MSG_REFRESH').'</p>';
            $this->writeLog($message);      

            set_time_limit(0);
            // Choose what to upgrade  
            $this->initCategories($db15, $db16, $post);
            if ($post[up_users]) $this->upgradeUsers($db15, $db16, $post);
            if ($post[up_content]) $this->upgradeSections($db15, $db16, $post);
            if ($post[up_content]) $this->upgradeCategories($db15, $db16, $post);            
            if ($post[up_content]) $this->upgradeContent($db15, $db16, $post);   
            if ($post[up_contacts]) $this->upgradeContactCategories($db15, $db16, $post);
            if ($post[up_contacts]) $this->upgradeContacts($db15, $db16, $post);
            if ($post[up_weblinks]) $this->upgradeWeblinksCategories($db15, $db16, $post);
            if ($post[up_weblinks]) $this->upgradeWeblinks($db15, $db16, $post);
            if ($post[up_newsfeeds]) $this->upgradeNewsfeedsCategories($db15, $db16, $post);
            if ($post[up_newsfeeds]) $this->upgradeNewsfeeds($db15, $db16, $post);
            if ($post[up_banners]) $this->upgradeBannersCategories($db15, $db16, $post);
            if ($post[up_banners]) $this->upgradeBannerClients($db15, $db16, $post);
            if ($post[up_banners]) $this->upgradeBanners($db15, $db16, $post);
            if ($post[up_images] && $pathValidation ) $this->upgradeImages($db15, $db16, $post);
            if ($post[up_template] && $pathValidation ) $this->upgradeTemplate($db15, $db16, $post);
            if ($post[up_menus]) $this->upgradeMenus($db15, $db16, $post);
            if ($post[up_menus]) $this->upgradeMenuItems($db15, $db16, $post);
            if ($post[up_modules]) $this->upgradeModules($db15, $db16, $post);
            if ($post[up_modules]) $this->upgradeModulesMenu($db15, $db16, $post);
            $this->fixAlias($db15, $db16, $post);

            mysql_close($db15);
            set_time_limit(30);

            $message = JText::sprintf('COM_SPUPGRADE_MSG_EPILOGUE', $urlLog);
            $this->writeLog($message);
            $jAp->enqueueMessage(JText::_('COM_SPUPGRADE_MSG_END'));
        }

        $message= ('</div>');
        $this->writeLog($message);
        parent::display();

        return $this;
    }

    /******************************************
    * Initialize categories
    *******************************************/	 
    function initCategories($db15, $db16, $post) {	

        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(1);
        if ($tableStatus->completed) return true;
        
        $jAp = & JFactory::getApplication();
        $message= ('<h2>'.JText::_('COM_SPUPGRADE_HEADER_INITIALIZE').'</h2>');
        $this->writeLog($message);
        $tableCategory = $this->getTable('Category', 'JTable');        
        $user = JFactory::getUser(0);
        
        //Find total number of categories
        $db16->setQuery(
            "SELECT ".$db16->nameQuote('id') .
            " FROM ".$db16->nameQuote('#__categories') .
            " WHERE id > 1 AND id < 8 AND alias LIKE ".$db16->quote('uncategorised').
            " ORDER BY id DESC"
            );
        $db16->query();
        $newcatnum1 = $db16->loadRow();

        for ($i = 2; $i <= $newcatnum1[0]; $i++) {

            $tableCategory->reset();
            $tableCategory->load($i);
            //Correction for bug with users.notes category. Remove for future releases.
            if ($tableCategory->extension == 'com_users') $tableCategory->extension = 'com_users.notes';
            $tableCategory->delete($i);
            
            //Find total number of categories
            $sql    = 'SELECT id FROM '.$post['db_prefix'].'categories ORDER BY id DESC';
            $result3 = mysql_query($sql, $db15);
            $row3 = mysql_fetch_assoc($result3);
            $id = $i + $row3['id'] + 98;

            $tableCategory->id = $id;
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->level		= null;
            //create record

            $db16->setQuery(
                "INSERT INTO ".$db16->nameQuote('#__categories').
                " (".$db16->nameQuote('id').")".
                " VALUES (".$db16->quote($id).")"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $tableCategory->id, $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }      

            // store			
            if (!$tableCategory->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $tableCategory->id, $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                return false;
            }

            // Rebuild the hierarchy.
            if (!$tableCategory->rebuild()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_REBUILD', $tableCategory->id, $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                return false;
            }

            // Rebuild the tree path.
            if (!$tableCategory->rebuildPath($tableCategory->id)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_REBUILDPATH', $tableCategory->id, $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                return false;
            }
        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /******************************************
    * Fix Alias
    *******************************************/	 
    function fixAlias($db15, $db16, $post) {

        $jAp = & JFactory::getApplication();
        $message= ('<hr/><h3>'.JText::_('COM_SPUPGRADE_HEADER_FIXALIAS').'</h3>');
        $this->writeLog($message);
        $user = JFactory::getUser(0);
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        

        // Cagegories
        if ( $post[up_content] || $post[up_contacts] || $post[up_weblinks] || $post[up_newsfeeds] || $post[up_banners] ) {
            
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(3);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(4);
            $completed2 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(21);
            $completed3 = $tableStatus->completed;
            if ($completed1 && $completed2 && $completed3) $completed = true;

            if (!$completed) {
                $table = $this->getTable('Category', 'JTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__categories" .
                    " WHERE alias LIKE '%-v15-%' AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();

                $message = "<h4>".JText::_('COM_SPUPGRADE_HEADER_CATEGORIES')."</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percCounter) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }

                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                            $this->writeLog($message);
                        }
                    }
                    // Rebuild the tree path.
                    if (!$table->rebuildPath($table->id)) {
                        $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_REBUILDPATH', $row['id'], $table->getError ).'</p>';
                        $this->writeLog($message);
                        return false;
                    }                            
                    $tableStatus->store();
                }
                // Rebuild the hierarchy.
                if (!$table->rebuild()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_REBUILD', $row['id'], $table->getError() ).'</p>';
                    $this->writeLog($message);
                    return false;
                }
                $tableStatus->completed = true;
                $tableStatus->store();         
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // MenuItems
        if ($post[up_menus]) {
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(17);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(18);
            $completed2 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(22);
            $completed3 = $tableStatus->completed;
            if ($completed1 && $completed2 && $completed3) $completed = true;
           
            if (!$completed) {
                if ($tableStatus->item_id == 0) $tableStatus->item_id = 1;

                $table = $this->getTable('Menu', 'JTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__menu WHERE menutype NOT LIKE ".$db16->quote('menu') .
                    " AND alias LIKE '%-v15-%'" .
                    " AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();
                $message = "<h4>Menus</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percTen) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }

                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $table->alias .= '-'.$table->menutype;
                            if (!$table->store()) {
                                $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                                $this->writeLog($message);
                            } else {
                                $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $table->alias).'</p>');
                                $this->writeLog($message);
                            }
                        }
                    }
                    // Rebuild the tree path.
                    if (!$table->rebuildPath($table->id)) {
                        $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_REBUILDPATH', $row['id'], $table->getError() ).'</p>';
                        $this->writeLog($message);
                        return false;
                    }
                    $tableStatus->store();
                }
                // Rebuild the hierarchy.
                if (!$table->rebuild()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_REBUILD', $row['id'], $table->getError() ).'</p>';
                    $this->writeLog($message);
                    return false;
                }
                $tableStatus->completed = true;
                $tableStatus->store();
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // content
        if ( $post[up_content] ) {
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(5);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(23);
            $completed2 = $tableStatus->completed;
            if ($completed1 && $completed2 ) $completed = true;
           
            if (!$completed) {
            
                $table = $this->getTable('Content', 'JTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__content" .
                    " WHERE alias LIKE '%-v15-%'" .
                    " AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();
                $message = "<h4>Articles</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percTen) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }

                    /*
                    $db16->setQuery(
                    "SELECT * " .
                    " FROM #__content" .
                    " WHERE id = ".$value->id                    
                    );
                    $db16->query();
                    $tmpItem = $db16->loadObject();
                    $alias = $tmpItem->alias;
                    $alias2 = $alias;
                    $pos = strpos($alias, '-v15-');
                    if ($pos > 0 ) {
                        $alias = substr($alias, 0, $pos);
                        $db16->setQuery(
                        "SELECT * " .
                        " FROM #__content" .
                        " WHERE alias LIKE '".$alias."'"
                        );
                        $db16->query();
                        $tmpList = $db16->loadObjectList();
                        if (count($tmpList) == 0) {
                            $db16->setQuery(
                            "UPDATE #__content".
                            " SET alias = ".$db16->quote($alias).
                            " WHERE id = ".$db16->quote($tmpItem->id)
                            );
                            $db16->query();
                        } else {
                            $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $tmpItem->title, $tmpItem->id, $alias2).'</p>');
                            $this->writeLog($message);
                        }                        
                    }
                     * 
                     */
                
                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $pos = strpos($alias, '-v15-');
                            if ($pos > 0) $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                            $this->writeLog($message);
                        }
                    }
                    $tableStatus->store();
                }
                $tableStatus->completed = true;
                $tableStatus->store();
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // contacts
        if ($post[up_contacts]) {
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(7);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(24);
            $completed2 = $tableStatus->completed;
            if ($completed1 && $completed2 ) $completed = true;
           
            if (!$completed) {
            
                JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_contact'.DS.'tables');
                $table = $this->getTable('Contact', 'ContactTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__contact_details" .
                    " WHERE alias LIKE '%-v15-%'" .
                    " AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();
                $message = "<h4>".JText::_('COM_SPUPGRADE_HEADER_CONTACTS')."</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percTen) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }

                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $pos = strpos($alias, '-v15-');
                            if ($pos > 0) $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                            $this->writeLog($message);
                        }
                    }
                    $tableStatus->store();
                }
                $tableStatus->completed = true;
                $tableStatus->store();
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // weblinks
        if ($post[up_weblinks]) {
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(9);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(25);
            $completed2 = $tableStatus->completed;
            if ($completed1 && $completed2 ) $completed = true;
           
            if (!$completed) {
            
                JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_weblinks'.DS.'tables');
                $table = $this->getTable('Weblink', 'WeblinksTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__weblinks" .
                    " WHERE alias LIKE '%-v15-%'" .
                    " AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();
                $message = "<h4>".JText::_('COM_SPUPGRADE_HEADER_WEBLINKS')."</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percTen) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }

                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $pos = strpos($alias, '-v15-');
                            if ($pos > 0) $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                            $this->writeLog($message);
                        }
                    }
                    $tableStatus->store();
                }
                $tableStatus->completed = true;
                $tableStatus->store();
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // newsfeeds
        if ($post[up_newsfeeds]) {	
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(11);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(26);
            $completed2 = $tableStatus->completed;
            if ($completed1 && $completed2 ) $completed = true;
           
            if (!$completed) {
            
                JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_newsfeeds'.DS.'tables');
                $table = $this->getTable('Newsfeed', 'NewsfeedsTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__newsfeeds" .
                    " WHERE alias LIKE '%-v15-%'" .
                    " AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();
                $message = "<h4>".JText::_('COM_SPUPGRADE_HEADER_NEWSFEEDS')."</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percTen) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }
                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $pos = strpos($alias, '-v15-');
                            if ($pos > 0) $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                            $this->writeLog($message);
                        }
                    }
                    $tableStatus->store();
                }
                $tableStatus->completed = true;
                $tableStatus->store();
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // banners
        if ( $post[up_banners] ) {	
            //Check function status
            $completed = false;
            $tableStatus->reset();
            $tableStatus->load(14);
            $completed1 = $tableStatus->completed;
            $tableStatus->reset();
            $tableStatus->load(27);
            $completed2 = $tableStatus->completed;
            if ($completed1 && $completed2 ) $completed = true;
           
            if (!$completed) {
            
                JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_banners'.DS.'tables');
                $table = $this->getTable('Banner', 'BannersTable');
                $db16->setQuery(
                    "SELECT id" .
                    " FROM #__banners" .
                    " WHERE alias LIKE '%-v15-%'" .
                    " AND id > ".$tableStatus->item_id .
                    " ORDER BY id ASC"
                    );
                $db16->query();
                $result = $db16->loadObjectList();
                $message = "<h4>".JText::_('COM_SPUPGRADE_HEADER_BANNERS')."</h4>";
                $this->writeLog($message);
                $percTotal = count($result);
                if ( $percTotal < 100 ) $percKlasma = 0.1;
                if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
                if ( $percTotal > 2000 ) $percKlasma = 0.01;
                $percTen = $percKlasma * $percTotal;
                $percCounter = 0;
                foreach ($result as $key=>$value) {
                    $tableStatus->item_id = $value->id;
                    $percCounter += 1;
                    if (@($percCounter % $percTen) == 0) {
                        $perc = round(( 100 * $percCounter ) / $percTotal);
                        $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                    }

                    $table->reset();
                    $table->load($value->id);
                    $alias = $table->alias;
                    $pos = strpos($table->alias, '-v15-');
                    if ($pos > 0 ) {
                        $table->alias = substr($table->alias, 0, $pos);
                        if (!$table->store()) {
                            $pos = strpos($table->alias, '-v15-');
                            if ($pos > 0) $message=('<p>'.JText::sprintf('COM_SPUPGRADE_MSG_CHANGEDALIAS', $table->title, $table->id, $alias).'</p>');
                            $this->writeLog($message);
                        }
                    }
                    $tableStatus->store();
                }
                $tableStatus->completed = true;
                $tableStatus->store();
                if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
            }
        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();

        $message= '<p>'.JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED').'</p>';
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * Users upgrade
    *********************************************/	 
    function upgradeUsers($db15, $db16, $post) {
      
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(2);
        if ($tableStatus->completed) return true;
        
        $message= ('<hr/><h2>'.JText::_('COM_SPUPGRADE_HEADER_USERS').'</h2>');
        $this->writeLog($message);

        $jAp = & JFactory::getApplication();
        $user = JFactory::getUser(0);

        /////////////////////////////////////
        // Deactivate email notifications
        /////////////////////////////////////

        // First backup
        $file = JPATH_LIBRARIES.'/joomla/mail/mail.php';
        $backupfile = JPATH_LIBRARIES.'/joomla/mail/mail.php.bak';

        if (!copy($file, $backupfile)) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $backupfile), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $backupfile).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        $backuphandle = @fopen($backupfile, "r+");
        $handle = @fopen($file, "w");
        if ($handle) {
            while (($buffer = fgets($backuphandle, 4096)) !== false) {

                IF (fwrite($handle,$buffer) === FALSE)
                {
                    $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file), 'error');
                    $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file).'</font></b></p>';
                    $this->writeLog($message);
                    return false;
                }

                if (strcmp(substr($buffer,1 ,22), 'public function Send()') == 0) {

                    $buffer = fgets($backuphandle, 4096);
                    fwrite($handle,$buffer);
                    $textline = " \n return true;\n";
                    IF (fwrite($handle,$textline) === FALSE)
                    {
                        $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file), 'error');
                        $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file).'</font></b></p>';
                        $this->writeLog($message);
                        return false;
                    }
                }
            }
            if (!feof($backuphandle)) {
                $message = '<p><b><font color="red">'.JText::_('COM_SPUPGRADE_MSG_ERROR_UNEXPECTEDFGETS').'</font></b></p>';
                $this->writeLog($message);
            }
            fflush($handle);
            fclose($handle);
            fclose($backuphandle);
        }

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'users WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'users WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);  
            return false;
        }

        //$versionCounter = 0; //version check
        while ($row = mysql_fetch_assoc($result)) {
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //if ($versionCounter > 100) continue;
            //$versionCounter += 1;


            //$row = $this->mUTF8Encode($row);

            // Special treatment for admin
            if ($row['id'] == 62) {
                $row['username'] = $row['username'].'v15';
                $row['email'] = $row['email'].'v15';
                $message = '<p>'.JText::_('COM_SPUPGRADE_MSG_OLD_ADMIN').'<br/>';
                $this->writeLog($message);
                $message= 'username: '.$row['username'].'<br/>';
                $this->writeLog($message);
                $message= 'email: '.$row['email'].'</p>';
                $this->writeLog($message);
            }

            // Copy record
            $db16->setQuery(
                "INSERT INTO ".$db16->nameQuote('#__users').
                " (id, name, username, email, password, usertype, block, sendEmail, registerDate, lastvisitDate, activation, params)" .
                " VALUES (".$db16->quote($row['id']).", ".$db16->quote($row['name']).", ".$db16->quote($row['username']).", ".$db16->quote($row['email']).", ".$db16->quote($row['password']).", ".$db16->quote($row['usertype']).", ".$db16->quote($row['block']).", ".$db16->quote($row['sendEmail']).", ".$db16->quote($row['registerDate']).", ".$db16->quote($row['lastvisitDate']).", '', '')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';                
                $this->writeLog($message);
                continue;
            }

            // Fix some values
            //$row['id'] = 0;
            $password = $row['password'];
            $row['password'] = "";
            $row['password2'] = "";
            $row['params'] = '{"admin_style":"","admin_language":"","language":"","editor":"","helpsite":"","timezone":""}';

            if (!$user->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $user->id, $user->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // Find user group
            if ($row['usertype'] == "Super Administrator") $row['usertype'] = "Super Users";
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__usergroups' .
                ' WHERE title LIKE '.$db16->quote($row['usertype'])
                );
            $db16->query();
            $gid = $db16->loadResult();
            $user->groups = array(0 => $gid);

            if (!$user->save()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $user->id, $user->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //password
            $db16->setQuery(
                "UPDATE #__users".
                " SET password = ".$db16->quote($password).
                " WHERE id = ".$db16->quote($user->id)
                );
            $db16->query();
            
            $tableStatus->item_id = $row['id'];
            $tableStatus->store();
        }
        mysql_free_result($result);

        /////////////////////////////////////
        // Activate email notifications
        /////////////////////////////////////
        if (!copy($backupfile, $file)) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $file ), 'error');
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $file ).'</p>';
            $this->writeLog($message);
            return false;
        }
        if (!unlink($backupfile)) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTODELETE', $backupfile ), 'error');
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTODELETE', $backupfile ).'</p>';
            $this->writeLog($message);
            return false;
        }

        // alter auto_increment
        $db16->setQuery(
            "ALTER TABLE #__users AUTO_INCREMENT = 0"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ALTER', $db16->getErrorMsg() ).'</p>';
            $this->writeLog($message);
        }

        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
        
        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /******************************************
    * Sections upgrade
    *******************************************/	 
    function upgradeSections($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(3);
        if ($tableStatus->completed) return true;        

        $jAp = & JFactory::getApplication();
        $message= ('<hr><h2></h2>');
        $message= ('<hr/><h2>'.JText::_('COM_SPUPGRADE_HEADER_CONTENT').'</h2>');
        $this->writeLog($message);
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_SECTIONS').'</h3>');
        $this->writeLog($message);
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableContent = $this->getTable('Content', 'JTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'sections WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'sections WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);        

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            
            $tableStatus->item_id = $row['id'];
            
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            $tableCategory->reset();

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $fakeid = '1000000000'.$row['id'];
            $row['alias'] .= '-v15-'.$fakeid;       

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Find total number of categories
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' ORDER BY id DESC'
                );
            $db16->query();
            $catnum = $db16->loadRow();
            $row['id'] = $catnum[0] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO ".$db16->nameQuote('#__categories').
                " (".$db16->nameQuote('id').")".
                " VALUES (".$db16->quote($row['id']).")"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';                
                $this->writeLog($message);
                continue;
            } 

            // Bind
            if (!$tableCategory->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->lft			= null;
            $tableCategory->rgt			= null;
            $tableCategory->level		= null;
            $tableCategory->path = null;
            $tableCategory->extension = "com_content";
            $tableCategory->language = '*';

            if ($row['image'] != "") {
                $tableCategory->params = '{"category_layout":"","image":"images\/stories\/'.
                    $row['image'].
                    '"}';
            }    

            // store			
            if (!$tableCategory->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //find parent
            $tableCategory->parent_id = 1;
            $tableCategory->store();
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);

        // alter auto_increment
        $db16->setQuery(
            "ALTER TABLE #__categories AUTO_INCREMENT = 0"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ALTER', $db16->getErrorMsg() ).'</p>';
            $this->writeLog($message);
        }
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Categories upgrade
    *******************************************/	 
    function upgradeCategories($db15, $db16, $post) {	
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(4);
        if ($tableStatus->completed) return true;
        
        $jAp = & JFactory::getApplication();
        $message= ('<h3>Categories</h3>');
        $this->writeLog($message);

        $tableCategory = $this->getTable('Category', 'JTable');
        $tableContent = $this->getTable('Content', 'JTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'categories WHERE section NOT LIKE "com_%" AND id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE section NOT LIKE "com_%" AND id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            
            $tableStatus->item_id = $row['id'];
            
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);   

            $tableCategory->reset();

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Find total number of categories
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' ORDER BY id DESC'
                );
            $db16->query();
            $newcatnum2 = $db16->loadRow();

            if ($row['id'] < 2  ) $row['id'] =  $newcatnum2[0] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__categories" .
                " (id)".
                " VALUES (".$db16->quote($row['id']).")"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            } 

            // Bind
            if (!$tableCategory->bind($row)) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__categories" .
                    " WHERE id = ".$db16->quote($row['id'])
                    );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg() ).'</p>';
                    $this->writeLog($message);
                }
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->lft			= null;
            $tableCategory->rgt			= null;
            $tableCategory->level		= null;
            $tableCategory->path = null;
            $tableCategory->extension = "com_content";
            $tableCategory->language = '*';

            if ($row['image'] != "") {
                $tableCategory->params = '{"category_layout":"","image":"images\/stories\/'.
                    $row['image'].
                    '"}';
            }    

            if (!$tableCategory->store()) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__categories" .
                    " WHERE id = ".$db16->quote($row['id'])
                    );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg() ).'</p>';
                    $this->writeLog($message);
                }
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //find parent
            $sql    = 'SELECT * FROM '.$post[db_prefix].'sections WHERE id = '.$row['section'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            //alias
            if ($row2['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                $temp = preg_replace('/\s/', '-', $row2['title']);
                $row2['alias'] = $temp;
            }
            $fakeid = '1000000000'.$row2['id'];
            $row2['alias'] .= '-v15-'.$fakeid;

            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' WHERE alias LIKE '.$db16->quote($row2['alias'])
                );
            $db16->query();
            $tableCategory->parent_id = $db16->loadResult();

            if (!$tableCategory->store()) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__categories" .
                    " WHERE id = ".$db16->quote($row['id'])
                    );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg() ).'</p>';
                    $this->writeLog($message);
                }
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);

        // alter auto_increment
        $db16->setQuery(
            "ALTER TABLE #__categories AUTO_INCREMENT = 0"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ALTER', $db16->getErrorMsg() ).'</p>';
            $this->writeLog($message);
        }
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Content upgrade
    *******************************************/	 
    function upgradeContent($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(5);
        if ($tableStatus->completed) return true;        

        $jAp = & JFactory::getApplication();
        $message= ('<h3>Articles</h3>');
        $this->writeLog($message);
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableContent = $this->getTable('Content', 'JTable');
        $user = JFactory::getUser(0);	  

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'content WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;    

        $sql    = 'SELECT * FROM '.$post[db_prefix].'content WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        //$versionCounter = 0; //version check
        while ($row = mysql_fetch_assoc($result)) {
            
            $tableStatus->item_id = $row['id'];
            
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }      

            //if ($versionCounter > 100) continue;
            //$versionCounter += 1;

            //$row = $this->mUTF8Encode($row);

            //problem with archived
            if ($row['state'] == -1 ) $row['state'] = 2;

            //find parent
            $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE id = '.$row['catid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            if ($row2['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                $temp = preg_replace('/\s/', '-', $row2['title']);
                $row2['alias'] = $temp;
            }
            $row2['alias'] .= '-v15-'.$row2['id'];
            if ($row['catid'] == 0) $row2['alias'] = 'uncategorised';

            $extension = 'com_content';
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' WHERE extension LIKE '.$db16->quote($extension).' AND alias LIKE '.$db16->quote($row2['alias'])
                );
            $db16->query();

            $row['catid']=$db16->loadResult();

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //frontpage to featured
            $sql    = 'SELECT * FROM '.$post[db_prefix].'content_frontpage WHERE content_id = '.$row['id'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);      
            if ($row2['content_id'] == $row['id']) {
                $row['featured'] = 1;
                $ordering = $row2['ordering'];
            }

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //assign variables to tableContent
            $tableContent->reset();

            // Copy record
            $db16->setQuery(
                "INSERT INTO ".$db16->nameQuote('#__content').
                " (".$db16->nameQuote('id').")".
                " VALUES (".$db16->quote($row['id']).")"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // Bind
            if (!$tableContent->bind($row)) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__content" .
                    " WHERE id = ".$row['id']
                );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg() ).'</p>';
                    $this->writeLog($message);
                }
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableContent->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableContent->asset_id	= null;
            //$tableContent->attribs = '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","alternative_readmore":"","article_layout":""}';
            //$tableContent->metadata = '{"robots":"","author":"","rights":"","xreference":""}';
            $tableContent->language = '*';

            // store			
            if (!$tableContent->store()) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__content" .
                    " WHERE id = ".$row['id']
                );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg() ).'</p>';
                    $this->writeLog($message);
                }

                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableContent->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //featured
            if ($row['featured'] == 1 ) {
                $db16->setQuery(
                    "INSERT INTO #__content_frontpage (content_id, ordering) VALUES (".$tableContent->id." , ".$ordering.")"
                    );
                $db16->query();
            }		  

            //rating
            $sql    = 'SELECT * FROM '.$post[db_prefix].'content_rating WHERE content_id = '.$row['id'];
            $result2 = mysql_query($sql, $db15);
            $rating = mysql_fetch_assoc($result2);      
            if ($rating['content_id'] == $row['id']) {
                $db16->setQuery(
                    "INSERT INTO #__content_rating (content_id, rating_sum, rating_count, lastip) VALUES (".$tableContent->id." , ".$rating['rating_sum']." , ".$rating['rating_count']." , '".$rating['lastip']."')"
                    );
                $db16->query();		    
            }
            
            $tableStatus->store();
        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);

        // alter auto_increment
        $db16->setQuery(
            "ALTER TABLE #__content AUTO_INCREMENT = 0"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ALTER', $db16->getErrorMsg() ).'</p>';
            $this->writeLog($message);
        }
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * ContactCategories upgrade
    *********************************************/	 
    function upgradeContactCategories($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(6);
        if ($tableStatus->completed) return true;        
        
        $jAp = & JFactory::getApplication();
        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_CONTACTS').'</h2>');
        $this->writeLog($message);
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_CATEGORIES').'</h3>');
        $this->writeLog($message);
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableContent = $this->getTable('Content', 'JTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'categories WHERE section LIKE "com_contact_details" AND id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE section LIKE "com_contact_details" AND id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            $tableCategory->reset();

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Find total number of categories
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' ORDER BY id DESC'
                );
            $db16->query();
            $newcatnum2 = $db16->loadRow();

            if ($row['id'] < 2  ) $row['id'] =  $newcatnum2[0] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__categories" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            } 

            // Bind
            if (!$tableCategory->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->lft			= null;
            $tableCategory->rgt			= null;
            $tableCategory->level		= null;
            $tableCategory->path = null;
            $tableCategory->extension = "com_contact";
            $tableCategory->language = '*';

            if ($row['image'] != "") {
                $tableCategory->params = '{"category_layout":"","image":"images\/stories\/'.
                    $row['image'].
                    '"}';
            }    

            // store			
            if (!$tableCategory->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //find parent
            $tableCategory->parent_id = 1;
            $tableCategory->store();
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Contacts upgrade
    *******************************************/	 
    function upgradeContacts($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(7);
        if ($tableStatus->completed) return true;                

        $jAp = & JFactory::getApplication();
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_CONTACTS').'</h3>');
        $this->writeLog($message);
        JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_contact'.DS.'tables');
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableContact = $this->getTable('Contact', 'ContactTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'contact_details WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'contact_details WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //problem with archived
            if ($row['state'] == -1 ) $row['state'] = 2;

            //find parent
            $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE id = '.$row['catid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            if ($row['catid'] == 0) $row2['alias'] = 'uncategorised';

            if ($row2['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                $temp = preg_replace('/\s/', '-', $row2['title']);
                $row2['alias'] = $temp;
            }
            $row2['alias'] .= '-v15-'.$row2['id'];

            $extension = 'com_contact';
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' WHERE extension LIKE '.$db16->quote($extension).' AND alias LIKE '.$db16->quote($row2['alias'])
                );
            $db16->query();

            $row['catid']=$db16->loadResult();

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //find linked user 
            if ($row['user_id'] == "") $row['user_id'] = 0;

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['name']);
                $temp = preg_replace('/\s/', '-', $row['name']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //assign variables to tableContent
            $tableContact->reset();
            $tableContact->language = '*';

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__contact_details" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // Bind
            if (!$tableContact->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableContact->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableContact->asset_id	= null;

            // check			
            if (!$tableContact->check()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CHECK', $row['id'], $tableContact->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // store	
            if (!$tableContact->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableContact->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * WeblinksCategories upgrade
    *********************************************/	 
    function upgradeWeblinksCategories($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(8);
        if ($tableStatus->completed) return true;        
        
        $jAp = & JFactory::getApplication();
        $tableCategory = $this->getTable('Category', 'JTable');
        $user = JFactory::getUser(0);
        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_WEBLINKS').'</h2>');
        $this->writeLog($message);
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_CATEGORIES').'</h3>');
        $this->writeLog($message);	  	  

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'categories WHERE section LIKE "com_weblinks" AND id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE section LIKE "com_weblinks" AND id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            $tableCategory->reset();

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Find total number of categories
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' ORDER BY id DESC'
                );
            $db16->query();
            $newcatnum2 = $db16->loadRow();

            if ($row['id'] < 2  ) $row['id'] =  $newcatnum2[0] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__categories" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            } 

            // Bind
            if (!$tableCategory->bind($row)) {                                                                                
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->lft			= null;
            $tableCategory->rgt			= null;
            $tableCategory->level		= null;
            $tableCategory->path = null;
            $tableCategory->extension = "com_weblinks";
            $tableCategory->language = '*';

            if ($row['image'] != "") {
                $tableCategory->params = '{"category_layout":"","image":"images\/stories\/'.
                    $row['image'].
                    '"}';
            }    

            // store			
            if (!$tableCategory->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //find parent
            $tableCategory->parent_id = 1;
            $tableCategory->store();
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Weblinks upgrade
    *******************************************/	 
    function upgradeWeblinks($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(9);
        if ($tableStatus->completed) return true;        
        
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_WEBLINKS').'</h3>');
        $this->writeLog($message);
        $jAp = & JFactory::getApplication();
        JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_weblinks'.DS.'tables');
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableWeblink = $this->getTable('Weblink', 'WeblinksTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'weblinks WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'weblinks WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }

            //$row = $this->mUTF8Encode($row);

            //problem with archived
            $row['state'] = $row['published'];
            if ($row['state'] == -1 ) $row['state'] = 2;

            //find parent
            $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE id = '.$row['catid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            if ($row['catid'] == 0) $row2['alias'] = 'uncategorised';

            if ($row2['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                $temp = preg_replace('/\s/', '-', $row2['title']);
                $row2['alias'] = $temp;
            }
            $row2['alias'] .= '-v15-'.$row2['id'];

            $extension = 'com_weblinks';
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' WHERE extension LIKE '.$db16->quote($extension).' AND alias LIKE '.$db16->quote($row2['alias'])
                );
            $db16->query();

            $row['catid']=$db16->loadResult();

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            if ($row['checked_out'] == "") $row['checked_out'] = 0;

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //assign variables to tableContent
            $tableWeblink->reset();
            $tableWeblink->language = '*';

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__weblinks" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // Bind
            if (!$tableWeblink->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableWeblink->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableWeblink->asset_id	= null;

            // check			
            if (!$tableWeblink->check()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CHECK', $row['id'], $tableWeblink->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // store	
            if (!$tableWeblink->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableWeblink->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * NewsfeedsCategories upgrade
    *********************************************/	 
    function upgradeNewsfeedsCategories($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(10);
        if ($tableStatus->completed) return true;        
        
        $jAp = & JFactory::getApplication();
        $tableCategory = $this->getTable('Category', 'JTable');
        $user = JFactory::getUser(0);
        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_NEWSFEEDS').'</h2>');
        $this->writeLog($message);
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_CATEGORIES').'</h3>');
        $this->writeLog($message);    	  	  

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'categories WHERE section LIKE "com_newsfeeds" AND id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE section LIKE "com_newsfeeds" AND id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            $tableCategory->reset();

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Find total number of categories
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' ORDER BY id DESC'
                );
            $db16->query();
            $newcatnum2 = $db16->loadRow();

            if ($row['id'] < 2  ) $row['id'] =  $newcatnum2[0] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__categories" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            } 

            // Bind
            if (!$tableCategory->bind($row)) {                                                                                
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->lft			= null;
            $tableCategory->rgt			= null;
            $tableCategory->level		= null;
            $tableCategory->path = null;
            $tableCategory->extension = "com_newsfeeds";
            $tableCategory->language = '*';

            if ($row['image'] != "") {
                $tableCategory->params = '{"category_layout":"","image":"images\/stories\/'.
                    $row['image'].
                    '"}';
            }    

            // store			
            if (!$tableCategory->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //find parent
            $tableCategory->parent_id = 1;
            $tableCategory->store();
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Newsfeeds upgrade
    *******************************************/	 
    function upgradeNewsfeeds($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(11);
        if ($tableStatus->completed) return true;                

        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_NEWSFEEDS').'</h3>');
        $this->writeLog($message);
        $jAp = & JFactory::getApplication();
        JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_newsfeeds'.DS.'tables');
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableNewsfeed = $this->getTable('Newsfeed', 'NewsfeedsTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'newsfeeds WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'newsfeeds WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            //problem with archived
            $row['state'] = $row['published'];
            if ($row['state'] == -1 ) $row['state'] = 2;

            //find parent
            $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE id = '.$row['catid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            if ($row['catid'] == 0) $row2['alias'] = 'uncategorised';

            if ($row2['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                $temp = preg_replace('/\s/', '-', $row2['title']);
                $row2['alias'] = $temp;
            }
            $row2['alias'] .= '-v15-'.$row2['id'];

            $extension = 'com_newsfeeds';
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' WHERE extension LIKE '.$db16->quote($extension).' AND alias LIKE '.$db16->quote($row2['alias'])
                );
            $db16->query();

            $row['catid']=$db16->loadResult();

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //find linked user 
            if ($row['checked_out'] == "") $row['checked_out'] = 0;

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['name']);
                $temp = preg_replace('/\s/', '-', $row['name']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__newsfeeds" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //assign variables to tableContent
            $tableNewsfeed->reset();
            $tableNewsfeed->language = '*';

            // Bind
            if (!$tableNewsfeed->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableNewsfeed->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableNewsfeed->asset_id	= null;

            // check			
            if (!$tableNewsfeed->check()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CHECK', $row['id'], $tableNewsFeed->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // store	
            if (!$tableNewsfeed->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableNewsfeed->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * BannersCategories upgrade
    *********************************************/	 
    function upgradeBannersCategories($db15, $db16, $post) {
        
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(12);
        if ($tableStatus->completed) return true;        
        
        $jAp = & JFactory::getApplication();
        $tableCategory = $this->getTable('Category', 'JTable');
        $user = JFactory::getUser(0);
        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_BANNERS').'</h2>');
        $this->writeLog($message);
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_CATEGORIES').'</h3>');
        $this->writeLog($message);    	  	  

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'categories WHERE section LIKE "com_banner" AND id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE section LIKE "com_banner" AND id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            $tableCategory->reset();

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Find total number of categories
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' ORDER BY id DESC'
                );
            $db16->query();
            $newcatnum2 = $db16->loadRow();

            if ($row['id'] < 2  ) $row['id'] =  $newcatnum2[0] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__categories" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            } 

            // Bind
            if (!$tableCategory->bind($row)) {                                                                                
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableCategory->asset_id	= null;
            $tableCategory->parent_id = 1;
            $tableCategory->lft			= null;
            $tableCategory->rgt			= null;
            $tableCategory->level		= null;
            $tableCategory->path = null;
            $tableCategory->extension = "com_banners";
            $tableCategory->language = '*';

            if ($row['image'] != "") {
                $tableCategory->params = '{"category_layout":"","image":"images\/stories\/'.
                    $row['image'].
                    '"}';
            }    

            // store			
            if (!$tableCategory->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableCategory->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //find parent
            $tableCategory->parent_id = 1;
            $tableCategory->store();
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * BannerClients upgrade
    *******************************************/	 
    function upgradeBannerClients($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(13);
        if ($tableStatus->completed) return true;        
        
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_CLIENTS').'</h3>');
        $this->writeLog($message);
        $jAp = & JFactory::getApplication();
        JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_banners'.DS.'tables');
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableBannerClients = $this->getTable('Client', 'BannersTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'bannerclient WHERE cid > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'bannerclient WHERE cid > '.$tableStatus->item_id.' ORDER BY cid ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['cid'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            //problem with archived
            $row['state'] = 1;
            $row['id'] = $row['cid'];

            //find linked user 
            if ($row['checked_out'] == "") $row['checked_out'] = 0;

            //assign variables to tableContent
            $tableBannerClients->reset();

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__banner_clients" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // Bind
            if (!$tableBannerClients->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableBannerClients->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // store	
            if (!$tableBannerClients->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableBannerClients->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Banners upgrade
    *******************************************/	 
    function upgradeBanners($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(14);
        if ($tableStatus->completed) return true;        
        
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_BANNERS').'</h3>');
        $this->writeLog($message);
        $jAp = & JFactory::getApplication();
        JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_banners'.DS.'tables');
        $tableCategory = $this->getTable('Category', 'JTable');
        $tableBanner = $this->getTable('Banner', 'BannersTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'banner WHERE bid > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'banner WHERE bid > '.$tableStatus->item_id.' ORDER BY bid ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['bid'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            //id
            $row['id'] = $row['bid'];

            //problem with archived
            $row['state'] = $row['showBanner'];
            if ($row['state'] == -1 ) $row['state'] = 2;

            //find client
            $sql    = 'SELECT name FROM '.$post[db_prefix].'bannerclient WHERE cid = '.$row['cid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            $db16->setQuery(
                'SELECT id' .
                ' FROM #__banner_clients' .
                ' WHERE name LIKE '.$db16->quote($row2['name'])
                );
            $db16->query();

            $row['cid']=$db16->loadResult();

            //find parent
            $sql    = 'SELECT * FROM '.$post[db_prefix].'categories WHERE id = '.$row['catid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);

            if ($row['catid'] == 0) $row2['alias'] = 'uncategorised';

            if ($row2['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                $temp = preg_replace('/\s/', '-', $row2['title']);
                $row2['alias'] = $temp;
            }
            $row2['alias'] .= '-v15-'.$row2['id'];

            $extension = 'com_banners';
            $db16->setQuery(
                'SELECT id' .
                ' FROM #__categories' .
                ' WHERE extension LIKE '.$db16->quote($extension).' AND alias LIKE '.$db16->quote($row2['alias'])
                );
            $db16->query();

            $row['catid']=$db16->loadResult();

            //find linked user 
            if ($row['checked_out'] == "") $row['checked_out'] = 0;

            //params
            $image = '"image":"",';
            if ($row['imageurl'] != "") $image = '{"imageurl":"images\/banners\/'.$row['imageurl'].'",';
            $params = explode("width=", $row['params']);
            $params = explode("height=", $params[1]);
            $row['params'] = $image.'"width":"'.$params[0].'","height":"'.$params[1].'","alt":""}';

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['name']);
                $temp = preg_replace('/\s/', '-', $row['name']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];

            //assign variables to tableContent
            $tableBanner->reset();
            $tableBanner->language = '*';

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__banners" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // Bind
            if (!$tableBanner->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableBanner->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // check			
            if (!$tableBanner->check()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CHECK', $row['id'], $tableBanner->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // store	
            if (!$tableBanner->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableBanner->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * Images upgrade
    *********************************************/	 
    function upgradeImages($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(15);
        if ($tableStatus->completed) return true;                

        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_IMAGES').'</h2>');
        $this->writeLog($message);

        $jAp = & JFactory::getApplication();
        $user = JFactory::getUser(0);

        $path15 = $post['db_path'].'/images';
        $path16 = JPATH_SITE.'/images';

        //backup 16 images folder
        if (file_exists($path16)) @rename($path16, JPATH_SITE.'/images_bak');

        //copy directory
        if (file_exists($path15)) {
            $this->recurse_copy($path15, $path16);
        } else {
            $message= '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_COPYIMAGES', $path15).'</p>';
            $this->writeLog($message);
            rename(JPATH_SITE.'/images_bak', $path16);  
        }

        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
        
        $message = '<p>'.JText::_('COM_SPUPGRADE_MSG_IMAGES').'</p>';
        $this->writeLog($message);
        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /********************************************
    * Template upgrade
    *********************************************/	 
    function upgradeTemplate($db15, $db16, $post) {

        $templateName = strtolower($post['db_template']);
        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_TEMPLATE').' '.$post['db_template'].'</h2>');
        $this->writeLog($message);

        $jAp = & JFactory::getApplication();
        $user = JFactory::getUser(0);

        $path15 = $post['db_path'].'/templates/'.$templateName;
        $path16 = JPATH_SITE.'/tmp/'.$templateName;

        //remove 16 images folder
        if (file_exists($path16)) $this->recursiveDelete($path16);

        //copy directory
        if (file_exists($path15)) {
            $this->recurse_copy($path15, $path16);
        } else {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_TEMPLATE', $path15, $post['db_template']).'</p>';
            $this->writeLog($message);
            return false;  
        }

        //get all file names
        $arrayFileNames = $this->getFileNames($path16);
        foreach ($arrayFileNames as $fileName) {

            //Modify php files
            $strLenght = strlen($fileName);
            if (substr($fileName, $fileName - 4, 4) == '.php') {
                $file = $fileName;
                $backupfile = $fileName.'.bak';

                if (!copy($file, $backupfile)) {
                    $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $backupfile), 'error');
                    $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $backupfile).'</font></b></p>';
                    $this->writeLog($message);
                    return false;
                }

                $backuphandle = @fopen($backupfile, "r+");
                $handle = @fopen($file, "w");
                if ($handle) {

                    while (($buffer = fgets($backuphandle, 4096)) !== false) {

                        $buffer = str_replace("defined( '_JEXEC' ) or die( 'Restricted access' );", "defined('_JEXEC') or die;", $buffer);
                        $buffer = str_replace("\$mainframe->", "JFactory::getApplication()->", $buffer);

                        IF (fwrite($handle,$buffer) === FALSE)
                        {
                            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file), 'error');
                            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file).'</font></b></p>';
                            $this->writeLog($message);
                            return false;
                        }        
                    }
                    if (!feof($backuphandle)) {
                        $message = '<p><b><font color="red">'.JText::_('COM_SPUPGRADE_MSG_ERROR_UNEXPECTEDFGETS').'</font></b></p>';
                        $this->writeLog($message);
                    }
                    fflush($handle);
                    fclose($handle);
                    fclose($backuphandle);
                    unlink($backupfile = $fileName.'.bak');
                }
            }

            //Modify templateDetails.xml file
            $strLenght = strlen($fileName);
            //if (substr($fileName, $fileName - 19, 19) == 'templateDetails.xml') {
            if (substr($fileName, $fileName - 4, 4) == '.xml') {
                $file = $fileName;
                $backupfile = $fileName.'.bak';

                if (!copy($file, $backupfile)) {
                    $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $backupfile), 'error');
                    $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_FAILEDTOCOPY', $backupfile).'</font></b></p>';
                    $this->writeLog($message);
                    return false;
                }

                $backuphandle = @fopen($backupfile, "r+");
                $handle = @fopen($file, "w");
                if ($handle) {

                    while (($buffer = fgets($backuphandle, 4096)) !== false) {

                        //copy language files
                        if (strpos($buffer, '<language tag="')) {
                            $tagPos = strpos($buffer, '"'); 
                            $tagLang = substr($buffer, $tagPos + 1, 5);
                            $posLang = strpos($buffer, '>');
                            $lenLang = strpos($buffer, '</') - $posLang - 1;
                            $fileLang = substr($buffer, $posLang +1, $lenLang);
                            @copy($post['db_path'].'/language/'.$tagLang.'/'.$fileLang, $path16.'/'.$fileLang);
                        }       

                        // replace strings
                        $buffer = str_replace('&', '&amp;', $buffer);

                        $buffer = str_replace('<install version="1.5"', '<install version="1.6"', $buffer);

                        $tmpString = '<config><fields name="params"><fieldset name="advanced">';
                        $buffer = str_replace('<params>', $tmpString, $buffer);

                        $tmpString = '</fieldset></fields></config>';
                        $buffer = str_replace('</params>', $tmpString, $buffer);

                        $buffer = str_replace('<param', '<field', $buffer);
                        $buffer = str_replace('/param>', '/field>', $buffer);

                        IF (fwrite($handle,$buffer) === FALSE)
                        {
                            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file), 'error');
                            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CANNOTWRITEFILE', $file).'</font></b></p>';
                            $this->writeLog($message);
                            return false;
                        }          

                    }
                    if (!feof($backuphandle)) {
                        $message = '<p><b><font color="red">'.JText::_('COM_SPUPGRADE_MSG_ERROR_UNEXPECTEDFGETS').'</font></b></p>';
                        $this->writeLog($message);
                    }
                    fflush($handle);
                    fclose($handle);
                    fclose($backuphandle);
                    unlink($backupfile = $fileName.'.bak');
                }
            }  
        }

        $message = JText::sprintf('COM_SPUPGRADE_MSG_TEMPLATE', $path16);
        $this->writeLog($message);
        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /******************************************
    * Menus upgrade
    *******************************************/	 
    function upgradeMenus($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(17);
        if ($tableStatus->completed) return true;        
        
        $jAp = & JFactory::getApplication();
        $message= ('<hr><h2>'.JText::_('COM_SPUPGRADE_HEADER_MENUS').'</h2>');
        $this->writeLog($message);
        $message=('<h3>'.JText::_('COM_SPUPGRADE_HEADER_MENUTYPES').'</h3>');
        $this->writeLog($message);
        $tableMenuType = $this->getTable('MenuType', 'JTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'menu_types WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        $sql    = 'SELECT * FROM '.$post[db_prefix].'menu_types WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }    

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }

            //$row = $this->mUTF8Encode($row);

            $tableMenuType->reset();

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__menu_types" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                //$message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                //$this->writeLog($message);
                //continue;
            }

            // Bind
            if (!$tableMenuType->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableMenuType->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            // store			
            if (!$tableMenuType->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableMenuType->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }  
            
            $tableStatus->store();

        }

        // Clear the component's cache
        $cache = JFactory::getCache('com_categories');
        $cache->clean();
        mysql_free_result($result);
        
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        return true;
    }

    /******************************************
    * Menu Items upgrade
    *******************************************/	 
    function upgradeMenuItems($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(28);
        if ($tableStatus->completed) return true;        
        $tableStatus->reset();
        $tableStatus->load(18);
        
        $message=('<h3>'.JText::_('COM_SPUPGRADE_HEADER_MENUITEMS').'</h3>');
        $this->writeLog($message);

        $jAp = & JFactory::getApplication();
        $tableMenuItem = $this->getTable('Menu', 'JTable');
        $user = JFactory::getUser(0);

        //Find total number of menus
        //First create in db      
        if ($tableStatus->item_id == 0) {
            $sql    = 'SELECT id FROM '.$post[db_prefix].'menu ORDER BY id DESC';
            $result = mysql_query($sql, $db15);
            $row = mysql_fetch_assoc($result);
            $totalMenus = $row['id'] + 1;
            if ( $totalMenus < 200 ) $totalMenus = 200;
            mysql_free_result($result);

            //Move old items
            $db16->setQuery(
                "SELECT * FROM #__menu WHERE id > 1"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_SELECT', '#__menu', $db16->getErrorMsg()).'</p>';
                $this->writeLog($message);
                continue;
            }
            $result = $db16->loadObjectList();

            foreach ($result as $key=>$value) {

                $id = $value->id + $totalMenus;
                $parent_id = 1;
                $alias = $value->alias;
                if ( $value->parent_id > 1 ) $parent_id = $value->parent_id + $totalMenus;
                if ( $value->alias == "home") { //handle home
                    $alias = $value->alias.'_v16';
                    $home = 0;
                    $db16->setQuery(
                        "UPDATE #__menu" .
                        " SET alias = ".$db16->quote($alias)." , home = ".$db16->quote($home).
                        " WHERE id = ".$value->id
                        );
                    if(!$db16->query()) {
                        $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_UPDATE', $value->id, $db16->getErrorMsg()).'</p>';
                        $this->writeLog($message);
                        continue;
                    }
                    $message=('<p>'.JText::_('COM_SPUPGRADE_MSG_HOME').'</p>');
                    $this->writeLog($message);
                }

                if ($value->id < $totalMenus ) {
                    $db16->setQuery(
                        "UPDATE #__menu" .
                        " SET id = '".$id."' , parent_id = '".$parent_id."'".
                        " WHERE id = ".$value->id
                        );
                    if(!$db16->query()) {
                        $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_MOVE', $value->id, $db16->getErrorMsg()).'</p>';
                        $this->writeLog($message);
                        continue;
                    }
                }    	
            }
        }

        //First create in db 
        if ($tableStatus->item_id == 0) $tableStatus->item_id = 1;
        $sql    = 'SELECT * FROM '.$post[db_prefix].'menu WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];

            //$row = $this->mUTF8Encode($row);

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;        
            }
            $row['alias'] .= '-v15-'.$row['id'];

            // Copy record
            $db16->setQuery(
                "INSERT INTO ".$db16->nameQuote('#__menu').
                " (".$db16->nameQuote('id').",".$db16->nameQuote('alias').")".
                " VALUES (".$db16->quote($row['id']).",".$db16->quote($row['alias']).")"
                );

            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            $tableStatus->store();
        }
        mysql_free_result($result);
        //Set function status
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }
        
        $tableStatus->reset();
        $tableStatus->load(28);
        if($tableStatus->item_id == 0) $tableStatus->item_id = 1;

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'menu WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;

        // Build the relations
        $sql    = 'SELECT * FROM '.$post[db_prefix].'menu WHERE id > '.$tableStatus->item_id.' ORDER BY id ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }


        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }

            //$row = $this->mUTF8Encode($row); 

            //alias
            if ($row['alias'] == '') {
                $temp = preg_replace('#\(\d+\)$#', '-', $row['title']);
                $temp = preg_replace('/\s/', '-', $row['title']);
                $row['alias'] = $temp;
            }
            $row['alias'] .= '-v15-'.$row['id'];     

            //language
            $row['parent_id'] = $row['parent'];
            if ( $row['parent'] == 0 )  $row['parent_id'] = 1;
            $row['title'] = $row['name'];
            $row['language'] = '*';
            $row['template_style_id '] = 0;

            //handle image
            $params = str_replace(array("\r", "\r\n", "\n"), 'mseparator', $row['params']);
            $params = explode('mseparator', $params);

            foreach ( $params as $key=>$value) {
                $params[$key] = str_replace('image=-1', 'image=', $value);
                if ( ($params[$key] != 'image=') && ($params[$key] != 'menu_image=') ) {
                    $params[$key] = str_replace('image=', 'image=images/stories/', $value);
                }
            }

            $row['params'] = implode('mseparator', $params);
            $row['params'] = str_replace("mseparator", "\n", $row['params']);
            
            //find extenion id
            $sql    = "SELECT `option` FROM ".$post[db_prefix].'components WHERE id = '.$row['componentid'];
            $result2 = mysql_query($sql, $db15);
            $row2 = mysql_fetch_assoc($result2);
            if ($row2['option'] == 'com_user') $row2['option'] = 'com_users';

            $db16->setQuery(
                'SELECT extension_id' .
                ' FROM #__extensions' .
                ' WHERE name LIKE '.$db16->quote($row2['option'])
                );
            $db16->query();
            $extension = $row2['option'];

            $rowResult = $db16->loadRow();
            $row['component_id'] = $rowResult[0];

            //level
            $row['level'] = $row['sublevel'] + 1;

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //Handle various components
            if ( $row['type'] == 'url') $extension = 'url';
            if ( $row['type'] == 'separator') $extension = 'separator';
            if ( $row['type'] == 'menulink') $extension = 'alias';            
            //if ( $row['type'] == 'wrapper') $extension = 'com_wrapper'; //for old versions

            $mBool = TRUE;
            switch ($extension) {

case 'com_content':

    //sections
    if ( strpos($row['link'], 'view=section') > 0 ) {
        $link = explode('&', $row['link']);
        foreach ($link as $key=>$value) {          
            $pos = strpos($value, 'id=');
            if ( $pos === 0) {
                $id = substr($value, $tmp + 3);
                $strId = $value;
            }
        }

        //find parent
        $sql    = 'SELECT alias FROM '.$post[db_prefix].'sections WHERE id = '.$id;
        $result2 = mysql_query($sql, $db15);
        $row2 = mysql_fetch_assoc($result2);

        //alias
        if ($row2['alias'] == '') {
            $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
            $temp = preg_replace('/\s/', '-', $row2['title']);
            $row2['alias'] = $temp;
        }
        $fakeid = '1000000000'.$id;
        $row2['alias'] .= '-v15-'.$fakeid;

        $db16->setQuery(
            'SELECT id' .
            ' FROM #__categories' .
            ' WHERE alias LIKE '.$db16->quote($row2['alias'])
            );
        $db16->query();
        $id = $db16->loadResult();

        $row['link'] = str_replace('view=section', 'view=category', $row['link'] );
        $row['link'] = str_replace($Itemid, '', $row['link'] );
        $row['link'] = str_replace($strId, 'id='.$id, $row['link'] );

    }

    //new article
    if ( strpos($row['link'], 'layout=form') > 0 ) {
        $row['link'] = str_replace('view=article', 'view=form', $row['link'] );  
        $row['link'] = str_replace('layout=form', 'layout=edit', $row['link'] );
    }

    //new featured
    if ( strpos($row['link'], 'view=frontpage') > 0 ) {
        $row['link'] = str_replace('view=frontpage', 'view=featured', $row['link'] );  
    }

    break;

case 'com_contact':
    $row['link'] = str_replace('&catid=', '&id=', $row['link'] );

    break;        

case 'com_newsfeeds':
    $row['link'] = str_replace('&catid=', '&id=', $row['link'] );

    break;        

case 'com_search':

    break;

case 'com_users':
    $row['link'] = str_replace('com_user', 'com_users', $row['link'] );
    $row['link'] = str_replace('view=register', 'view=registration', $row['link'] );
    $row['link'] = str_replace('view=user&layout=form', 'view=profile&layout=edit', $row['link'] );
    if ( strpos($row['link'], 'view=user') > 0 ) $mBool = false;

    break;

case 'com_weblinks':
    $row['link'] = str_replace('&catid=', '&id=', $row['link'] );
    $row['link'] = str_replace('view=weblink&layout=form', 'view=form&layout=edit', $row['link'] );

    break;

case 'com_wrapper':
    $row['component_id'] = 2;
    $row['type'] = 'component';

    break;

case 'url':

    break;

case 'separator':

    break;

case 'alias':
    $row['type'] = 'alias'; 
    $row['params'] = str_replace('menu_item', 'aliasoptions', $row['params']);
    
    break;

case 'com_spupgrade':

    break;        

default:
        if (!$post[up_menusall]) $mBool = FALSE; //exit if not amongs the handled extensions

    break;
            }

            if (!$mBool) { //exit if not amongs the handled extensions
                $db16->setQuery(
                    "DELETE FROM #__menu" .
                    " WHERE id = ".$row['id']
                );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg()).'</p>';
                    $this->writeLog($message);
                }
                continue; 
            }

            //assign variables to tableContent
            $tableMenuItem->reset();
            $tableMenuItem->load($row['id']);

            // Bind
            if (!$tableMenuItem->bind($row)) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__menu" .
                    " WHERE id = ".$row['id']
                );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg()).'</p>';
                    $this->writeLog($message);
                }
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableMenuItem->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableMenuItem->lft = null;
            $tableMenuItem->rgt = null;

            // store			
            if (!$tableMenuItem->store()) {
                // delete record
                $db16->setQuery(
                    "DELETE FROM #__menu" .
                    " WHERE id = ".$row['id']
                );
                if(!$db16->query()) {
                    $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_DELETE', $row['id'], $db16->getErrorMsg()).'</p>';
                    $this->writeLog($message);
                }

                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableMenuItem->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();
        }

        // Clear the component's cache
        mysql_free_result($result);

        // alter auto_increment
        $db16->setQuery(
            "ALTER TABLE #__menu AUTO_INCREMENT = 0"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ALTER', $db16->getErrorMsg() ).'</p>';
            $this->writeLog($message);
        }

        //correct id = 1
        // Copy record
        $db16->setQuery(
            "UPDATE ".$db16->nameQuote('#__menu').
            " SET ".$db16->nameQuote('menutype')."=".$db16->quote('').
            " WHERE ".$db16->nameQuote('id')."=".$db16->quote('1')
            );

        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
            $this->writeLog($message);
            continue;
        }
        
        $this->defineDefaultMenuItem($db15, $db16, $post);
        
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /******************************************
    * Modules upgrade
    *******************************************/	 
    function upgradeModules($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(19);
        if ($tableStatus->completed) return true;        
        
        $message= ('<hr/><h2>'.JText::_('COM_SPUPGRADE_HEADER_MODULES').'</h2>');
        $this->writeLog($message);
        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_MODULES').'</h3>');
        $this->writeLog($message);

        //Move modules old items
        if ($tableStatus->item_id == 0) {
            //Find total number of modules
            $sql    = 'SELECT id FROM '.$post[db_prefix].'modules ORDER BY id DESC';
            $result = mysql_query($sql, $db15);
            $row = mysql_fetch_assoc($result);
            $totalModules = $row['id'] + 1;
        
            $db16->setQuery(
                "SELECT * FROM #__modules"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_SELECT', 'modules', $db16->getErrorMsg()).'</p>';
                $this->writeLog($message);
                continue;
            }
            $result = $db16->loadObjectList();

            foreach ($result as $key=>$value) {

                $id = $value->id + $totalModules;
                if ($value->id < $totalModules ) {
                    $db16->setQuery(
                        "UPDATE #__modules" .
                        " SET id = '".$id."'".
                        " WHERE id = ".$value->id
                        );
                    if(!$db16->query()) {
                        $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_MOVE', $value->id, $db16->getErrorMsg()).'</p>';
                        $this->writeLog($message);
                        continue;
                    }
                }    	
            }
        }

        $jAp = & JFactory::getApplication();
        $tableModule = $this->getTable('Module', 'JTable');
        $user = JFactory::getUser(0);

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'modules WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;


        $sql    = 'SELECT * FROM '.$post[db_prefix].'modules WHERE id > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }


            //$row = $this->mUTF8Encode($row);

            //access difference
            if ($row['access'] > 2) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_ACCESSLEVEL', $row['id'] ).'</p>';
                $this->writeLog($message);
                continue;
            }
            if ($row['access'] == 2) $row['access'] = 3;
            if ($row['access'] == 1) $row['access'] = 2;
            if ($row['access'] == 0) $row['access'] = 1;

            //find extenion
            $module = "";
            if ( $row['module'] == 'mod_archive' ) $module = 'mod_articles_archive';
            if ( $row['module'] == 'mod_banners' ) $module = 'mod_banners';
            if ( $row['module'] == 'mod_custom' ) $module = 'mod_custom';
            if ( $row['module'] == 'mod_feed' ) $module = 'mod_feed';
            if ( $row['module'] == 'mod_footer' ) $module = 'mod_footer';
            if ( $row['module'] == 'mod_latestnews' ) $module = 'mod_articles_latest';
            if ( $row['module'] == 'mod_login' ) $module = 'mod_login';
            if ( strpos($row['params'], 'menutype') === 0 ) $module = 'mod_menu';
            if ( $row['module'] == 'mod_mostread' ) $module = 'mod_articles_popular';
            if ( $row['module'] == 'mod_newsflash' ) $module = 'mod_articles_news';
            if ( $row['module'] == 'mod_random_image' ) $module = 'mod_random_image';
            if ( $row['module'] == 'mod_related_items' ) $module = 'mod_related_items';
            if ( $row['module'] == 'mod_search' ) $module = 'mod_search';
            if ( $row['module'] == 'mod_stats' ) $module = 'mod_stats';
            if ( $row['module'] == 'mod_status' ) $module = 'mod_status';
            if ( $row['module'] == 'mod_syndicate' ) $module = 'mod_syndicate';
            if ( $row['module'] == 'mod_whosonline' ) $module = 'mod_whosonline';
            if ( $row['module'] == 'mod_wrapper' ) $module = 'mod_wrapper';

            if ( $module == "" ) continue; //Note: remove this load all modules
            $row['module'] = $module;

            //convert params
            $params = str_replace(array("\r", "\r\n", "\n"), 'mseparator', $row['params']);
            $params = explode('mseparator', $params);

            $secid = '';
            foreach ( $params as $key=>$value) {
                if ( strpos($value, 'secid=') === 0 ) {
                    $secid = str_replace('secid=', '', $value);

                    $sql    = 'SELECT alias FROM '.$post[db_prefix].'sections WHERE id = '.$secid;
                    $result2 = mysql_query($sql, $db15);
                    $row2 = @mysql_fetch_assoc($result2);

                    //alias
                    if ($row2['alias'] == '') {
                        $temp = preg_replace('#\(\d+\)$#', '-', $row2['title']);
                        $temp = preg_replace('/\s/', '-', $row2['title']);
                        $row2['alias'] = $temp;
                    }
                    $fakeid = '1000000000'.$secid;
                    $row2['alias'] .= '-v15-'.$fakeid;

                    $db16->setQuery(
                        'SELECT id' .
                        ' FROM #__categories' .
                        ' WHERE alias LIKE '.$db16->quote($row2['alias'])
                        );
                    $db16->query();
                    $secid = ','.$db16->loadResult();
                    if ($secid == '') $secid =''; 

                    $params[$key] = '';
                }       	
                if ( strpos($value, 'catid=') === 0 ) {
                    if ($secid != ",") $params[$key] .= $secid;
                    $params[$key] = str_replace('=', '":["', $params[$key]).'"]';
                    $params[$key] = str_replace(',', '","', $params[$key]);
                }

                $params[$key] = '"'.str_replace('=', '":"', $params[$key]).'"';
            }
            $row['params'] = implode(',', $params);
            $row['params'] = str_replace(',""', "", $row['params']);
            $row['params'] = str_replace('"]"', '"]', $row['params']);
            $row['params'] = '{'.$row['params'].'}'; 

            //level
            $row['level'] = $row['sublevel'] + 1;

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__modules" .
                " (id)".
                " VALUES ('".$row['id']."')"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //assign variables to tableContent
            $tableModule->reset();

            // Bind
            if (!$tableModule->bind($row)) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_BIND', $row['id'], $tableModule->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }

            //no parent
            $tableModule->language = '*';

            // store			
            if (!$tableModule->store()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_STORE', $row['id'], $tableModule->getError() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            
            $tableStatus->store();

        }
        
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        mysql_free_result($result);

        return true;
    }

    /******************************************
    * ModulesMenu upgrade
    *******************************************/	 
    function upgradeModulesMenu($db15, $db16, $post) {
        //Check function status
        $tableStatus = $this->getTable('SPUpgrade', 'JTable');        
        $tableStatus->reset();
        $tableStatus->load(20);
        if ($tableStatus->completed) return true;        

        $message= ('<h3>'.JText::_('COM_SPUPGRADE_HEADER_LINKMODULESMENUS').'</h3>');
        $this->writeLog($message);

        if ($tableStatus->item_id == 0) {
            //Find total number of modules
            $sql    = 'SELECT id FROM '.$post[db_prefix].'modules ORDER BY id DESC';
            $result = mysql_query($sql, $db15);
            $row = mysql_fetch_assoc($result);
            $totalModules = $row['id'] + 1;

            //update modules_menu old items
            $db16->setQuery(
                "SELECT * FROM #__modules_menu"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_SELECT', 'modules_menu', $db16->getErrorMsg()).'</p>';
                $this->writeLog($message);
                continue;
            }
            $result = $db16->loadObjectList();

            foreach ($result as $key=>$value) {
                $moduleid = $value->moduleid + $totalModules;
                if ($value->moduleid < $totalModules ) {
                    $db16->setQuery(
                        "UPDATE #__modules_menu" .
                        " SET moduleid = ".$db16->quote($moduleid).
                        " WHERE moduleid = ".$db16->quote($value->moduleid)
                        );		    
                    if(!$db16->query()) {
                        $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_MOVE', $value->module_id, $db16->getErrorMsg()).'</p>';
                        $this->writeLog($message);
                        continue;
                    }
                }    	
            }
        }

        $jAp = & JFactory::getApplication();
        $user = JFactory::getUser(0);    	  

        //percentage
        $sql    = 'SELECT COUNT(*) FROM '.$post[db_prefix].'modules_menu WHERE moduleid > '.$tableStatus->item_id;
        $result = mysql_query($sql, $db15);
        $percResult = mysql_fetch_assoc($result);
        $percTotal = $percResult['COUNT(*)'];
        if ( $percTotal < 100 ) $percKlasma = 0.1;
        if ( $percTotal > 100 && $percTotal < 2000 ) $percKlasma = 0.05;
        if ( $percTotal > 2000 ) $percKlasma = 0.01;
        $percTen = $percKlasma * $percTotal;
        $percCounter = 0;


        $sql    = 'SELECT * FROM '.$post[db_prefix].'modules_menu WHERE moduleid > '.$tableStatus->item_id.' ORDER BY moduleid ASC';
        $result = mysql_query($sql, $db15);

        if (!$result) {
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()), 'error');
            $message = '<p><b><font color="red">'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_QUERY_J15', mysql_error()).'</font></b></p>';
            $this->writeLog($message);
            return false;
        }

        while ($row = mysql_fetch_assoc($result)) {
            $tableStatus->item_id = $row['id'];
            //percentage
            $percCounter += 1;
            if (@($percCounter % $percTen) == 0) {
                $perc = round(( 100 * $percCounter ) / $percTotal);
                $message = $perc.'% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                $this->writeLog($message);
            }

            //if ($row['menuid'] == 1) $row['menuid'] = 0;

            //check validity
            /*
            $db16->setQuery(
            "SELECT id FROM #__modules WHERE id = ".$db16->quote($row['moduleid'])
            );
            $db16->query();
            $result16 = $db16->loadResult();
            if ($result16 == '') continue;

            $db16->setQuery(
            "SELECT id FROM #__menu WHERE id = ".$db16->quote($row['menuid'])
            );
            $db16->query();
            $result16 = $db16->loadResult();
            if ($result16 == '') continue;
            */

            // Copy record
            $db16->setQuery(
                "INSERT INTO #__modules_menu" .
                " (moduleid, menuid)".
                " VALUES (".$db16->quote($row['moduleid']).", ".$db16->quote($row['menuid']).")"
                //" VALUES (".$db16->quote($row['moduleid']).", ".$db16->quote(0).")"
                );
            if(!$db16->query()) {
                $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_CREATE', $row['id'], $db16->getErrorMsg() ).'</p>';
                $this->writeLog($message);
                continue;
            }
            $tableStatus->store();

        }
        mysql_free_result($result);
        
        $tableStatus->completed = true;
        $tableStatus->store();
        if ($perc < 100) {
                        $message = '100% '.JText::_('COM_SPUPGRADE_MSG_PROCESSED').'<br/>';  
                        $this->writeLog($message);
                }

        $message=("<p><b>".JText::_('COM_SPUPGRADE_MSG_PROCESS_COMPLETED')."</b></p>");
        $this->writeLog($message);
        return true;
    }

    /**
    * Returns a reference to the a Table object, always creating it.
    */
    public function getTable($type = 'User', $prefix = 'JTable', $config = array())
    {
        $table = JTable::getInstance($type, $prefix, $config);

        return $table;
    }

    /**
    * Convert to utf8
    */
    public function mUTF8Encode( $row = array())
    {
        foreach($row as $key => $value) {
            //if (mb_detect_encoding($value, "UTF-8") == "UTF-8" ) $value = utf8_encode($value);
            if (mb_detect_encoding($value, "UTF-8") == "UTF-8" ) {
                $value = $this->cp1252_to_utf8($value);
            }
            $new_row[$key] = $value;
        }

        return $new_row;
    }

    function cp1252_to_utf8($str) {
        global $cp1252_map;
        $cp1252_map = array(
            "\xc2\x80" => "\xe2\x82\xac", /* EURO SIGN */
            "\xc2\x82" => "\xe2\x80\x9a", /* SINGLE LOW-9 QUOTATION MARK */
            "\xc2\x83" => "\xc6\x92",     /* LATIN SMALL LETTER F WITH HOOK */
            "\xc2\x84" => "\xe2\x80\x9e", /* DOUBLE LOW-9 QUOTATION MARK */
            "\xc2\x85" => "\xe2\x80\xa6", /* HORIZONTAL ELLIPSIS */
            "\xc2\x86" => "\xe2\x80\xa0", /* DAGGER */
            "\xc2\x87" => "\xe2\x80\xa1", /* DOUBLE DAGGER */
            "\xc2\x88" => "\xcb\x86",     /* MODIFIER LETTER CIRCUMFLEX ACCENT */
            "\xc2\x89" => "\xe2\x80\xb0", /* PER MILLE SIGN */
            "\xc2\x8a" => "\xc5\xa0",     /* LATIN CAPITAL LETTER S WITH CARON */
            "\xc2\x8b" => "\xe2\x80\xb9", /* SINGLE LEFT-POINTING ANGLE QUOTATION */
            "\xc2\x8c" => "\xc5\x92",     /* LATIN CAPITAL LIGATURE OE */
            "\xc2\x8e" => "\xc5\xbd",     /* LATIN CAPITAL LETTER Z WITH CARON */
            "\xc2\x91" => "\xe2\x80\x98", /* LEFT SINGLE QUOTATION MARK */
            "\xc2\x92" => "\xe2\x80\x99", /* RIGHT SINGLE QUOTATION MARK */
            "\xc2\x93" => "\xe2\x80\x9c", /* LEFT DOUBLE QUOTATION MARK */
            "\xc2\x94" => "\xe2\x80\x9d", /* RIGHT DOUBLE QUOTATION MARK */
            "\xc2\x95" => "\xe2\x80\xa2", /* BULLET */
            "\xc2\x96" => "\xe2\x80\x93", /* EN DASH */
            "\xc2\x97" => "\xe2\x80\x94", /* EM DASH */

            "\xc2\x98" => "\xcb\x9c",     /* SMALL TILDE */
            "\xc2\x99" => "\xe2\x84\xa2", /* TRADE MARK SIGN */
            "\xc2\x9a" => "\xc5\xa1",     /* LATIN SMALL LETTER S WITH CARON */
            "\xc2\x9b" => "\xe2\x80\xba", /* SINGLE RIGHT-POINTING ANGLE QUOTATION*/
            "\xc2\x9c" => "\xc5\x93",     /* LATIN SMALL LIGATURE OE */
            "\xc2\x9e" => "\xc5\xbe",     /* LATIN SMALL LETTER Z WITH CARON */
            "\xc2\x9f" => "\xc5\xb8"      /* LATIN CAPITAL LETTER Y WITH DIAERESIS*/
            );
        return  strtr(utf8_encode($str), $cp1252_map);
    }

    //////////////////////////////////////////////////////////
    // Directory copy
    //////////////////////////////////////////////////////////    
    function recurse_copy($src,$dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $this->recurse_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }

    /**
    * Delete a file or recursively delete a directory
    *
    * @param string $str Path to file or directory
    */
    function recursiveDelete($str){
        if(is_file($str)){
            return @unlink($str);
        }
        elseif(is_dir($str)){
            $scan = glob(rtrim($str,'/').'/*');
            foreach($scan as $index=>$path){
                $this->recursiveDelete($path);
            }
            return @rmdir($str);
        }
    }

    //////////////////////////////////////////////////////////
    // Get file names
    //////////////////////////////////////////////////////////    
    function getFileNames($dirName) {
        $dir = opendir($dirName);
        $i = 0;
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($dirName . '/' . $file) ) {
                    $arr = $this->getFileNames($dirName . '/' . $file);
                    foreach ($arr as $value) {
                        $arrayNames[$i] = $value; 
                        $i++;
                    }
                }
                else {
                    $arrayNames[$i] = $dirName . '/' . $file; 
                    $i++;
                }
            }
        }
        closedir($dir);
        return $arrayNames;
    }

    function writeLog($message, $mode = 'a')
    {
        $fileName = JPATH_COMPONENT_ADMINISTRATOR.'/log.htm';
        $handle = fopen($fileName, $mode);
        if ($handle) {
            fwrite($handle,$message);
            fflush($handle);
            fclose($handle);
        }
        return true;
    }
    
     function checkDbType($fileConfiguration)
    {
        $jAp = & JFactory::getApplication();
        $pathValidation = TRUE;
        if (!file_exists($fileConfiguration)) {
            $pathValidation = FALSE;
            $jAp->enqueueMessage(JText::sprintf('COM_SPUPGRADE_MSG_INVALIDPATH3', $fileConfiguration), 'error');
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_INVALIDPATH3', $fileConfiguration).'</p>';
            $this->writeLog($message);         
            return false;
        }

        if ($pathValidation) {
            $mBool = FALSE;
            $handleConfiguration = @fopen($fileConfiguration, "r");
            while (($buffer = fgets($handleConfiguration, 4096)) !== false) {
                if (strpos($buffer, "mysqli") > 0 ) $mBool = TRUE;
            }
            fclose($handleConfiguration);

            if (!$mBool) {
                $jAp->enqueueMessage(JText::_('COM_SPUPGRADE_MSG_MYSQLI'), 'error');
                $message = '<p><b><font color="red">'.JText::_('COM_SPUPGRADE_MSG_MYSQLI').'</font></b></p>';
                $this->writeLog($message);
                return false;
            }            
        }
        
        return true;
    }
    
    /******************************************
    * Define Default Menu Item
    *******************************************/	 
    function defineDefaultMenuItem($db15, $db16, $post) {
        $jAp = & JFactory::getApplication();       
        //Seach if there is defined home
        $db16->setQuery(
            "SELECT * FROM #__menu WHERE home = '1'"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_SELECT', '#__menu', $db16->getErrorMsg()).'</p>';
            $this->writeLog($message);
            continue;
        }
        $result = $db16->loadObjectList();

        if (count($result) > 0 ) return true;
        
        $db16->setQuery(
            "UPDATE #__menu" .
            " SET home = 1".
            " WHERE alias = 'home_v16'"
            );
        if(!$db16->query()) {
            $message = '<p>'.JText::sprintf('COM_SPUPGRADE_MSG_ERROR_HOME', $db16->getErrorMsg()).'</p>';
            $this->writeLog($message);
            return false;
        }

        return true;

    }

}
