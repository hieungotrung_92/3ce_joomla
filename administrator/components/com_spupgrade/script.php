<?php
/**
 * @package		SP Upgrade
 * @subpackage	Components
 * @copyright	SP CYEND - All rights reserved.
 * @author		SP CYEND
 * @link		http://www.cyend.com
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html 
*/
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modeladmin');   
/**
 * Script file of SPUPGRADE component
 */
class com_SPUPGRADEInstallerScript
{
	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent) 
	{
            // $parent is the class calling this method
            $parent->getParent()->setRedirectURL('index.php?option=com_spupgrade');
	}
	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
            // $parent is the class calling this method
            echo '<p>' . JText::_('COM_SPUPGRADE_UNINSTALL_TEXT') . '</p>';
	}
	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent) 
	{
            // $parent is the class calling this method
            //echo '<p>' . JText::_('COM_SPUPGRADE_UPDATE_TEXT') . '</p>'; 
            $parent->getParent()->setRedirectURL('index.php?option=com_spupgrade'); 
                                       
	}
	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		//echo '<p>' . JText::_('COM_SPUPGRADE pre flight' . $type . '_TEXT') . '</p>';
	}
	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		//echo '<p>' . JText::_('COM_SPUPGRADE post flight' . $type . '_TEXT') . '</p>';
	}
	
}