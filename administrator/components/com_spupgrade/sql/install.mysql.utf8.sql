DROP TABLE IF EXISTS `#__spupgrade`;
CREATE TABLE IF NOT EXISTS `#__spupgrade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Last processed item',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT 'Process name',
  `completed` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if process is completed',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `#__spupgrade`
--

INSERT INTO `#__spupgrade` (`id`, `item_id`, `title`, `completed`) VALUES
(1, 0, 'initCategories', 0),
(2, 0, 'upgradeUsers', 0),
(3, 0, 'upgradeSections', 0),
(4, 0, 'upgradeCategories', 0),
(5, 0, 'upgradeContent', 0),
(6, 0, 'upgradeContactCategories', 0),
(7, 0, 'upgradeContacts', 0),
(8, 0, 'upgradeWeblinksCategories', 0),
(9, 0, 'upgradeWeblinks', 0),
(10, 0, 'upgradeNewsfeedsCategories', 0),
(11, 0, 'upgradeNewsfeeds', 0),
(12, 0, 'upgradeBannersCategories', 0),
(13, 0, 'upgradeBannerClients', 0),
(14, 0, 'upgradeBanner', 0),
(15, 0, 'upgradeImages', 0),
(16, 0, 'upgradeTemplate', 0),
(17, 0, 'upgradeMenus', 0),
(18, 0, 'upgradeMenuItems', 0),
(19, 0, 'upgradeModules', 0),
(20, 0, 'upgradeModulesMenu', 0),
(21, 0, 'fixAliasCategories', 0),
(22, 0, 'fixAliasMenuItems', 0),
(23, 0, 'fixAliasContent', 0),
(24, 0, 'fixAliasContacts', 0),
(25, 0, 'fixAliasWeblinks', 0),
(26, 0, 'fixAliasNewsfeeds', 0),
(27, 0, 'fixAliasBanners', 0),
(28, 0, 'upgradeMenuItems2', 0);