<?php
/**
* @package		SP Upgrade
* @subpackage	Components
* @copyright	SP CYEND - All rights reserved.
* @author		SP CYEND
* @link		http://www.cyend.com
* @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*/

// No direct access
defined('_JEXEC') or die;

/**
 * SPUpgrade table
 */
class JTableSPUpgrade extends JTable
{
	/**
	 * @param	database	A database connector object
	 * @since	1.0
	 */
	function __construct(&$db)
	{
		parent::__construct('#__spupgrade', 'id', $db);
	}

}
