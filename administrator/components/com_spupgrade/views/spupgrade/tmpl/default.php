<?php
/**
 * @package		SP Upgrade
 * @subpackage	Components
 * @copyright	SP CYEND - All rights reserved.
 * @author		SP CYEND
 * @link		http://www.cyend.com
 * @license		License GNU General Public License version 2 or later
*/
// No direct access.
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>

<script language="javascript">
function myValidate(f) {
   if (document.formvalidator.isValid(f)) {
      f.check.value='<?php echo JUtility::getToken(); ?>'; //send token
      var myDomain = location.protocol + '//' +
                    location.hostname +
                    location.pathname.substring(0, location.pathname.lastIndexOf('/')) +
                    '/components/com_spupgrade/log.htm';                    
      window.open(myDomain,'SP Upgrade','width=640,height=480, scrollbars=1');
      return true; 
   }
   else {
      var msg = 'Some values are cannot be empty.  Please retry.';
 
      alert(msg);
   }
   return false;
}
</script>

<div class="m" align=center>
<?php echo JText::_('COM_SPUPGRADE_INTRONOTE'); ?>
</div>

<div class="m" align=center>
<form id="WV-form" method="post" class="form-validate" onSubmit="return myValidate(this);">
<input type="hidden" name="check" value="post"/>
<table border="0" width="50%" align="center" cellspacing="5" cellpadding="5" summary="Database v1.5 Information">
    <caption><h3><?php echo JText::_('COM_SPUPGRADE_SUBTITLE1'); ?></h3></caption>
    <tr>
        <td colspan="2">
            <label id="jform_db_host-lbl" for="jform_db_host" class=" required"><?php echo JText::_('COM_SPUPGRADE_DB_HOST_LABEL'); ?></label><br />
            <input type="text" name="jform[db_host]" id="jform_db_host" value="localhost" class="inputbox required"/>
        </td>
        <td>
            <em>
                <?php echo JText::_('COM_SPUPGRADE_DB_HOST_COMMENTS'); ?>
            </em>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label id="jform_db_user-lbl" for="jform_db_user" class=" required"><?php echo JText::_('COM_SPUPGRADE_DB_USER_LABEL'); ?></label><br />
            <input type="text" name="jform[db_user]" id="jform_db_user" value="" class="inputbox required"/>									
        </td>
        <td>
            <em>
            <?php echo JText::_('COM_SPUPGRADE_DB_USER_COMMENTS'); ?>
            </em>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label id="jform_db_pass-lbl" for="jform_db_pass" class=""><?php echo JText::_('COM_SPUPGRADE_DB_PASS_LABEL'); ?></label><br />
            <input type="password" name="jform[db_pass]" id="jform_db_pass" value="" class="inputbox"/>									
        </td>
        <td>
            <em>
            <?php echo JText::_('COM_SPUPGRADE_DB_PASS_COMMENTS'); ?>
            </em>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label id="jform_db_name-lbl" for="jform_db_name" class=" required"><?php echo JText::_('COM_SPUPGRADE_DB_NAME_LABEL'); ?></label><br />
            <input type="text" name="jform[db_name]" id="jform_db_name" value="" class="inputbox required"/>									
        </td>
        <td>
            <em>
            <?php echo JText::_('COM_SPUPGRADE_DB_NAME_COMMENTS'); ?>
            </em>

        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label id="jform_db_prefix-lbl" for="jform_db_prefix" class=" required"><?php echo JText::_('COM_SPUPGRADE_DB_PREFIX_LABEL'); ?></label><br />
            <input type="text" name="jform[db_prefix]" id="jform_db_prefix" value="jos_" class="inputbox required"/>									
        </td>
        <td>

            <em>
            <?php echo JText::_('COM_SPUPGRADE_DB_PREFIX_COMMENTS'); ?>
            </em>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <label id="jform_db_path-lbl" for="jform_db_path" class=" required"><?php echo JText::_('COM_SPUPGRADE_DB_PATH_LABEL'); ?></label><br />
            <input type="text" name="jform[db_path]" id="jform_db_path" value="" />									
        </td>
        <td>

            <em>
            <?php echo JText::_("COM_SPUPGRADE_DB_PATH_COMMENTS");?>
            <br/><b><?php echo JPATH_SITE; ?></b>
            </em>
        </td>
    </tr>
</table>
</div>
<div class="m" align=center>
<table border="0" width="50%" align="center" cellspacing="5" cellpadding="5" summary="Choose what to upgrade">
  <caption>
      <h3><?php echo JText::_('COM_SPUPGRADE_SUBTITLE2'); ?></h3>
  </caption>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_USERS_LABEL'); ?>            
        </td>
        <td>
              <input type="radio" name="jform[up_users]" id="jform_up_users" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_users]" id="jform_up_users" value="0" /> <?php echo JText::_('JNO'); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_CATEGORIES_LABEL'); ?>
        </td>
        <td>
              <input type="radio" name="jform[up_content]" id="jform_up_content" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_content]" id="jform_up_content" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_CONTACTS_LABEL'); ?>
        </td>
        <td>
              <input type="radio" name="jform[up_contacts]" id="jform_up_contacts" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_contacts]" id="jform_up_contacts" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_WEBLINKS_LABEL'); ?>
        </td>
        <td>
                  <input type="radio" name="jform[up_weblinks]" id="jform_up_weblinks" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_weblinks]" id="jform_up_weblinks" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_NEWSFEEDS_LABEL'); ?>
        </td>
        <td>
                  <input type="radio" name="jform[up_newsfeeds]" id="jform_up_newsfeeds" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_newsfeeds]" id="jform_up_newsfeeds" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_BANNERS_LABEL'); ?>
        </td>
        <td>
                  <input type="radio" name="jform[up_banners]" id="jform_up_banners" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_banners]" id="jform_up_banners" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>										
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_IMAGES_LABEL'); ?>
        </td>
        <td>
                  <input type="radio" name="jform[up_images]" id="jform_up_images" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_images]" id="jform_up_images" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td>
         <?php echo JText::_('COM_SPUPGRADE_TEMPLATE_LABEL'); ?>
        </td>
        <td>
             <em><?php echo JText::_('COM_SPUPGRADE_TEMPLATE_LABEL2'); ?> </em><input type="text" name="jform[db_template]" id="jform_db_template" value="" />
        </td>
        <td>
                  <input type="radio" name="jform[up_template]" id="jform_up_template" value="1" /> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_template]" id="jform_up_template" value="0" checked/> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td>
         <?php echo JText::_('COM_SPUPGRADE_MENUS_LABEL'); ?>
        </td>
        <td>
                <label class="hasTip" title="<?php echo JText::_('COM_SPUPGRADE_MENUS_ALL_TIP'); ?>"><?php echo JText::_('COM_SPUPGRADE_MENUS_ALL'); ?></label>
                  <input type="radio" name="jform[up_menusall]" id="jform_up_menusall" value="1" /> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_menusall]" id="jform_up_menusall" value="0" checked/> <?php echo JText::_('JNO'); ?>                     
        </td>
        <td>
                  <input type="radio" name="jform[up_menus]" id="jform_up_menus" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_menus]" id="jform_up_menus" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
    <tr>
        <td colspan="2">
         <?php echo JText::_('COM_SPUPGRADE_MODULES_LABEL'); ?>
        </td>
        <td>
                  <input type="radio" name="jform[up_modules]" id="jform_up_modules" value="1" checked/> <?php echo JText::_('JYES'); ?> <input type="radio" name="jform[up_modules]" id="jform_up_modules" value="0" /> <?php echo JText::_('JNO'); ?>                     
        </td>
    </tr>
</table>					
</div>
<div class="m" align="center"><span class="hasTip3" title="<?php echo JText::_('COM_SPUPGRADE_BTN_SUBMIT_TITLE');?>">
  <button class="button validate" type="submit" value="Submit"><?php echo JText::_('COM_SPUPGRADE_BTN_SUBMIT_LABEL');?></button>
</span>
<em><?php echo JText::_('COM_SPUPGRADE_BTN_SUBMIT_COMMENTS');?></em>							
							
  <input type="hidden" name="option" value="com_spupgrade" />
	<input type="hidden" name="task" value="save" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>        
</div>    