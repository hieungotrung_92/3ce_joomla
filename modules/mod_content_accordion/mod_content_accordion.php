<?php

/*------------------------------------------------------------------------
 # Yt Content Accordion  - Version 1.0
 # Copyright (C) 2009-2011 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://www.ytcvn.com
 -------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' ); 
require_once (dirname(__FILE__).DS.'helper.php');

jimport("joomla.filesystem.folder");
jimport("joomla.filesystem.file");

/*-- Process---*/
$intro 							= $params->get("intro", 0);
$note 							= $params->get("note", 0);
$start							= $params->get("start", 1);
$featured                       = $params->get('featured', 2);
$target 						= $params->get("target", '');
$jquery 						= $params->get("jquery", 0);
$total                          = $params->get("total",1);
$play 							= $params->get("play", 'true');
$effect 						= $params->get("effect", 'fade');
$slideshow_speed 				= $params->get("slideshow_speed", 800);
$timer_speed 					= $params->get("timer_speed", 4000);
$opacity_main                   = $params->get("opacity_main", 0.7);
$start_clock_on_mouseOut 		= $params->get("start_clock_on_mouseOut", 'true');
$start_clock_on_mouseOutAfter 	= $params->get("start_clock_on_mouseOutAfter", 3000);
$caption_animation_speed 		= $params->get("caption_animation_speed", 800);
$main_background 				= $params->get("main_background", '#000000');
$main_color 				    = $params->get("main_color", '#FFFFFF');
$title_color 					= $params->get("title_color", '#FFFFFF');
$title_active_color 			= $params->get("title_active_color", '#FFFFFF');
$normal_description_color       = $params->get("normal_description_color", '#FFFFFF');
$normal_desc_active_color       = $params->get("normal_description_active_color", '#FFFFFF');
$normal_background              = $params->get("normal_background", '#2c0da0');
$normal_active_background       = $params->get("normal_active_background", '#2c0da0');
$prenext_show 					= $params->get("prenext_show", 1);
$caption_show 					= $params->get("caption_show", 'true');
$thumb_height 					= $params->get('thumb_height', "940");
$thumb_width 					= $params->get('thumb_width', "450");		
$show_readmore 					= $params->get('show_readmore', "0");
$show_image                     = $params->get('show_image', "1");
$link_caption					= $params->get('link_caption', 1);
$link_image						= $params->get('link_image', 1);
$auto_play						= $params->get('auto_play', 1);
$show_img_on_right				= $params->get('show_img_on_right',1);
$button_theme					= $params->get('button_theme','number');
$desc_box_width					= $params->get('desc_box_width','440');
$width_module                   = $params->get('width_module','800');
$num_item_per_page              = $params->get('num_item_per_page','4');
$show_readmore_slideii          = $params->get('show_readmore_slideii','1');
$readmore                       = $params->get('readmore');
$show_buttons_slideii           = $params->get('show_buttons_slideii','1');
$show_title_slideshowii         = $params->get('show_title_slideshowii','1');
$accmouseenter                  = $params->get('accmouseenter','');
$displayitem                    = $params->get('displayitem','0');
$show_price                     = $params->get('show_price', 0);
$color_price 					= $params->get('price_color', "#FFFFFF");
 
$start--;
$readmore_img = ''.JText::_('read more ').'';

$browser = new Browser();
if($browser->Name=='msie' && floor($browser->Version)==6){$widthIe = 3;}
if (!defined ('CONENT_ACCORDION')) {
	define ('CONENT_ACCORDION', 1);
	
    //JHTML::script('mootools-core.js', JURI::base() . '/modules/'.$module->module.'/assets/js/');
	JHTML::script('mootools-more.js',JURI::base() . '/modules/'.$module->module.'/assets/js/');
	JHTML::script('yt_accordion.js',JURI::base() . '/modules/'.$module->module.'/assets/js/');
	/* Add css*/	
	$browser = new Browser();
	$mainframe = JFactory::getApplication();
	if($browser->Name=='msie' && floor($browser->Version)==6)
	{
		//JHTML::stylesheet('ie6.css', JURI::base() . '/modules/'.$module->module.'/assets/');		
	}
	else if($browser->Name=='msie' && floor($browser->Version)==7)	
	{
		if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."ie7.css")){
			JHTML::stylesheet("ie7.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
		}else{
			JHTML::stylesheet('ie7.css', JURI::base() . '/modules/'.$module->module.'/assets/');	
		}
	}
	
		
	if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."style.css")){
		JHTML::stylesheet("style.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
	}else{
		JHTML::stylesheet('style.css', JURI::base() . '/modules/'.$module->module.'/assets/');	
	}
		
	/* add JS files*/
    
	
    		
}

$items = modYtContentAccordion::process($params, $module);
$count_items = count($items);
if($count_items > 0 ) {
	if ($count_items > 1) {
			foreach($items as $key=>$item)
			{
				if($key==0)
				{
					$pre = $count_items-1;
					$nex = $key+1;
				} 
				elseif(($key+1)==$count_items)
				{
					$pre = $key-1;
					$nex = 0;
				}
				else
				{
					$pre = $key-1;
					$nex = $key+1;
				}		
			}

		} else {
		$items[0]['pre'] = "";
		$items[0]['nex'] = "";
		}
} 	
$path = JModuleHelper::getLayoutPath( 'mod_content_accordion');

if (file_exists($path)) {
	require($path);
}

?>