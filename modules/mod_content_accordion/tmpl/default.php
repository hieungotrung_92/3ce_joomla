<?php
/*------------------------------------------------------------------------
# K2 Accordion - Version 1.0
# Copyright (C) 2009-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
?>
<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>
<script type="text/javascript">
//<![CDATA[
	window.addEvent('load', function() {
    	$$(".yt-accordion").each( function( item ){
    		var myAccordion = new Accordion( item, '.yt-toggler', '.yt-element', {
    			opacity: false,
    			alwaysHide:true,
    			display:<?php echo $displayitem;?>,
    			onActive: function(toggler, element){
    				toggler.addClass('open');
    			},
    			onBackground: function(toggler, element){
    				toggler.removeClass('open');
    			}
    		});
            <?php if($accmouseenter == 'mouseenter'):?>
            $$('.yt-toggler').addEvent('<?php echo $accmouseenter;?>', function() { this.fireEvent('click'); });
            <?php endif;?>							   
    	} );								 
    });
//]]>
</script>
<div class="bd-accordion" style="width:<?php echo $width_module;?>px;">
    <div class="yt-accordion">	
    <?php foreach( $items as $item ) : ?>
    	<h3 class="yt-toggler"><span><?php echo $item['sub_title'];?></span></h3>
    	<div class="yt-element">
            <div class="yt-accordion-image">
                <?php if($show_image == 1):?>
                <div class="" title="<?php echo $item['title'];?>">
    				<?php if($link_image == 1):?><a href="<?php echo $item['link'];?>" target="<?php echo $target;?>"><?php endif;?>
                        <?php if($item['thumb'] != ''):?><img src="<?php echo $item['thumb']?>" title="<?php echo $item['title'];?>" alt=""/><?php endif;?>
                        <?php if($item['thumb'] == ''):?>&nbsp;<?php endif;?>
                    <?php if($link_image == 1):?></a><?php endif;?>
    			</div>
                <?php endif;?>    
            </div>
        	<div class="yt-accordion-content">
            	<p><?php echo $item['sub_content'];?></p>
                <?php if($show_readmore == 1):?><span class="yt-accordion-readmore" title="<?php echo $item['title'];?>"><a href="<?php echo $item['link'];?>" target="<?php echo $target;?>"><?php echo $readmore;?></a></span><?php endif;?>
            </div>
        </div>
    <?php endforeach; ?>            
    </div>
</div>