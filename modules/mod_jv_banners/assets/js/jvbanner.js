var jvbanners=Fx.Elements.extend({
	options:{
		height:true,
		width:true,
		duration: 200,
		opacity:true,
		wait:false,
		t1: null,
		timeShow: null,
		isOpen: 1,
		id:  null
	},
	initialize:function(){
		var options,togglers,elements,container;
		$each(arguments,function(argument,i){
			switch($type(argument)){
			case"object":options=argument;break;
			case"element":container=$(argument);break;
			default:var temp=$$(argument);
			if(!togglers){togglers=temp;}else{elements=temp;}}
		});
		this.togglers=togglers||[];
		this.elements=elements||[];
		this.container=$(container);
		this.setOptions(options);
		this.effects={};
		var obj={};			
		var width = parseInt($('imagefig'+this.options.id).getStyle('width'));
		var height = parseInt($('imagefig'+this.options.id).getStyle('height'));		
		
		if(this.options.anima==0){
			widthfix = width - 2;
			height = height + 20;
			
			$$('.showbanner'+this.options.id).setStyles({width: width});
			$('bannericon'+this.options.id).setStyles({height: height});
		}else{
			widthfix = width - 80
		}
		$('marquee'+this.options.id).setStyles({width: widthfix});
		
		if(this.options.opacity){
			this.effects.opacity="Opacity";
		}		
		if(this.options.anima==1){
			if(this.options.height){
				this.effects.height=this.options.fixedHeight?"Height":"scrollHeight";
			}
		}else{
			if(this.options.width){
				this.effects.width=this.options.fixedWidth?"Width":"scrollWidth";						
			}
		}
		var box = $('jvbanner'+this.options.id);
		var fx = new Fx.Styles(box, {
			duration: 1500,
			wait: false,
			transition: Fx.Transitions.Quad.easeOut
		});
		if(this.options.timeShow>0){
			
			box.setStyles({
				opacity: .0
			});
			
			var timshow = setTimeout(function(){
				window.addEvent('domready', function() {					
				    fx.start({
				    	opacity: [0, 1]
				    });
				});
			},this.options.timeShow);
		}
		$('close'+this.options.id).addEvent('click', function() {
			fx.start({
				opacity: [1, 0]
			});	
		});	
		/*var timeClose = setTimeout(function() {				
			fx.start({
				opacity: .0
			});
		},this.options.timeClose);*/
		
		for(var i=0,l=this.togglers.length;i<l;i++){
			this.Action(this.togglers[i],this.elements[i]);
		}
	},
	Action:function(toggler,element,pos){
		toggler=$(toggler);
		element=$(element);
		this.count = 0;
		var idx=this.togglers.indexOf(toggler);
		toggler.addEvent("click", function() {
			//this.options.isOpen = (this.options.isOpen) ? 0 : 1;
			this.display(idx);
		}.bind(this));			
		element.Opacity=1;	
		this.options.t1=this.display.periodical(this.options.timeHide, this);
		element.setStyle("overflow","hidden");
		return this;
	},
	display:function(index){
		this.options.isOpen = (this.options.isOpen) ? 0 : 1;
		this.isToggle=[];
		var obj={};				
		this.elements.each(
			function(el,i){
				obj[i]={};
				if(this.options.anima==1)					
					var hide=i!=0||this.options.openActive&&el.offsetHeight>0;	
				else					
					var hide=i!=0||this.options.openActive&&el.offsetWidth>0;					
					
				for(var fx in this.effects){					
					obj[i][fx]=hide?0:el[this.effects[fx]];						
				}
			},this);	
		
		if (this.options.isOpen) {
			this.options.t1=this.display.periodical(this.options.timeHide, this);
		} else {
			if (this.options.t1) $clear(this.options.t1);
		}
		return this.start(obj);		
	 }
});