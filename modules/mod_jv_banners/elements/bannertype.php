<?php
/**
 * @package JV Banner module for Joomla! 1.5
 * @author http://www.joomlavision.com
 * @copyright (C) 2010- JoomlaVision.Com
 * @license PHP files are GNU/GPL
**/
defined('_JEXEC') or die( 'Restricted access' );

class JElementbannertype extends JElement
{

	function fetchElement($name, $value, &$node, $control_name)
	{	
	$options = array ();
		
		foreach ($node->children() as $option)
		{
			$val	= $option->attributes('value');
			$text	= $option->data();
			$options[] = JHTML::_('select.option', $val, JText::_($text));
		}
		$class = ( $node->attributes('class') ? 'class="'.$node->attributes('class').'"' : 'class="inputbox"' );
		$class .= " onchange=\"javascript: switchGroup(this)\"";
		$str = JHTML::_('select.genericlist',  $options, ''.$control_name.'['.$name.']', $class, 'value', 'text', $value, $control_name.$name);		
		$str .= "<script type=\"text/javascript\" language=\"javascript\">				
					function getElementsByGroup (group) {
					  if (!group) return;
					  var els=[];					 
					  $$(document.adminForm.elements).each(function(el){ 
						if (el.id.test(group+'_')) els.push(el);
					  });
						
					  return els;
					}
					
					function switchGroup (selectcontrol) {
						selectedel = selectcontrol.options[selectcontrol.selectedIndex];
						groupsel = selectedel.value.split('_')[0];	
						$$(selectcontrol.options).each(function (el) {
					  	var group = el.value.split('_')[0];
  						var groups = getElementsByGroup (group);
  						if (!groups) return;
						var disabled = (groupsel==group)?'':'disabled';
  						groups.each(function(g){
  							g.disabled = disabled;
  						});
					  });
					}
					
					function getTR (el) {
					  el = $(el);
					  var p;
					  while ((p = el.getParent()) && p.tagName != 'TR') {el = p;}
					  
					  return p;
					}
					
					function disableall(){
					  var selectct = $('params".$name."');
					  switchGroup(selectct);					 
					}

					function jvInit () {
					   disableall();
					   document.adminForm.onsubmit = enableall;
					}

					function enableall(){
					  var selectct = $('params".$name."');
					  $$(selectct.options).each(function (el) {
					  	var group = el.value.split('_')[0];
  						var groups = getElementsByGroup (group);
  						if (!groups) return;
  						groups.each(function(g){
  							g.disabled = '';
  						});
					  });
					}

					window.addEvent('load', jvInit);
				</script>";
		
			return $str;
	}
}
