<?php
/**
 * @package JV Banner module for Joomla! 1.5
 * @author http://www.joomlavision.com
 * @copyright (C) 2010- JoomlaVision.Com
 * @license PHP files are GNU/GPL
**/
defined('_JEXEC') or die( 'Restricted access' );

class JElementJvEffect extends JElement
{
	function fetchElement($name, $value, &$node, $control_name)
	{	
		$options = array ();	
		$cid = JRequest::getVar('cid','');
	    if($cid !='') $cid = $cid[0];
	    if($cid == ''){
	      $cid = JRequest::getVar('id');
	    }
		$db = JFactory::getDBO();
	    $sql = "SELECT params FROM #__modules WHERE id=$cid";
        $db->setQuery($sql);        
        $paramsConfigObj = $db->loadObjectList();
        $aryParams = explode("\n",$paramsConfigObj[0]->params); 
        $effect='';
	    foreach($aryParams as $item){
        $posSelection = strpos(trim($item),'effectst=');   
	        if($posSelection === false){
	        } else {
	          $effect = substr(trim($item),$posSelection+(int)strlen('effectst='));
	        }    
       
        }
			
		foreach ($node->children() as $option)
		{
			$val	= $option->attributes('value');
			$text	= $option->data();
			
			$options[] = JHTML::_('select.option', $val, JText::_($text));
		}
		
		$class = ( $node->attributes('class') ? 'class="'.$node->attributes('class').'"' : 'class="inputbox"' );
		$class .= "onchange=\"selectpositions( 'paramsjv_effectst', selectEffect, document.adminForm.paramsjv_Positions.options[document.adminForm.paramsjv_Positions.selectedIndex].value, 0, 0);\"";
		$str = "<script type=\"text/javascript\" language=\"javascript\">				
					function selectpositions( listname, source, key, orig_key, orig_val ) {
						var list = eval( 'document.adminForm.' + listname );
				
						for (i in list.options.length) {
							list.options[i] = null;
						}
						i = 0;
						for (x in source) {
							if (source[x][0] == key) {
								opt = new Option();
								opt.value = source[x][1];
								opt.text = source[x][2];
				
								if ((orig_key == key && orig_val == opt.value) || i == 0) {
									opt.selected = true;
								}
								list.options[i++] = opt;
							}
						}
						list.length = i;
					}	
					var selectEffect = new Array; 
					selectEffect[0] = new Array( '-1','-1','Select effect' );						
					selectEffect[1] = new Array( '1','1','Drop Down' );
					selectEffect[2] = new Array( '1','2','Left to right' );
					selectEffect[3] = new Array( '2','3','Drop Up' );
					selectEffect[4] = new Array( '2','4','Left to right' );
					selectEffect[5] = new Array( '3','5','Drop Down' );
					selectEffect[6] = new Array( '3','6','Right to left' );	
					selectEffect[7] = new Array( '4','7','Drop Up' );
					selectEffect[8] = new Array( '4','8','Right to left' );					
				</script>";
		if($effect){
		$str.="
				<script type=\"text/javascript\" language=\"javascript\">
					window.addEvent('domready', function(){
								position = $('paramsjv_Positions').value;
								if(position ==1){
									newoption = new Option('Drop Down',1);
									$('paramsjv_effectst').add(newoption,null);
									if(".$effect." == 1) {
										newoption.selected = true;	
									}
									newoption = new Option('Left to right',2);
									if(".$effect." == 2){
										newoption.selected = true;
									}
									$('paramsjv_effectst').add(newoption,null);							
								}
								if(position ==2){
									newoption = new Option('Drop Up',3);
									$('paramsjv_effectst').add(newoption,null);
									if(".$effect." == 3) {
										newoption.selected = true;	
									}
									newoption = new Option('Left to right',4);
									if(".$effect." == 4){
										newoption.selected = true;
									}
									$('paramsjv_effectst').add(newoption,null);							
								}
								if(position ==3){
									newoption = new Option('Drop Down',5);
									$('paramsjv_effectst').add(newoption,null);
									if(".$effect." == 5) {
										newoption.selected = true;	
									}
									newoption = new Option('Right to Left',6);
									if(".$effect." == 6){
										newoption.selected = true;
									}
									$('paramsjv_effectst').add(newoption,null);							
								}
								if(position ==4){
									newoption = new Option('Drop Up',7);
									$('paramsjv_effectst').add(newoption,null);
									if(".$effect." == 7) {
										newoption.selected = true;	
									}
									newoption = new Option('Right to Left',8);
									if(".$effect." == 8){
										newoption.selected = true;
									}
									$('paramsjv_effectst').add(newoption,null);							
								}
							});
						</script>
					";
		}else{
			$str.="
				<script type=\"text/javascript\" language=\"javascript\">
					window.addEvent('domready', function(){
								position = $('paramsjv_Positions').value;								
								if(position ==4){
									newoption = new Option('Drop Up',7);
									$('paramsjv_effectst').add(newoption,null);
									
									newoption.selected = true;
									newoption = new Option('Right to Left',8);
									$('paramsjv_effectst').add(newoption,null);							
								}
							});
						</script>
					";
		}
		
		$str .= JHTML::_('select.genericlist',  $options, ''.$control_name.'['.$name.']', $class, 'value', 'text', $value, $control_name.$name);		
		return $str;
	}
}
