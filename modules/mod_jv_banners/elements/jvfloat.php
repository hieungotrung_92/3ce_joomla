<?php
/**
 * @package JV Banner module for Joomla! 1.5
 * @author http://www.joomlavision.com
 * @copyright (C) 2010- JoomlaVision.Com
 * @license PHP files are GNU/GPL
**/
defined('_JEXEC') or die( 'Restricted access' );

class JElementJvfloat extends JElement
{
function fetchElement($name, $value, &$node, $control_name)
	{	
	$options = array ();
		
		foreach ($node->children() as $option)
		{
			$val	= $option->attributes('value');
			$text	= $option->data();
			$options[] = JHTML::_('select.option', $val, JText::_($text));
		}
		$class = ( $node->attributes('class') ? 'class="'.$node->attributes('class').'"' : 'class="inputbox"' );
		$class .= " onchange=\"javascript: jvfloat(this)\"";
		$str = JHTML::_('select.genericlist',  $options, ''.$control_name.'['.$name.']', $class, 'value', 'text', $value, $control_name.$name);		
		$str .= "<script type=\"text/javascript\" language=\"javascript\">				
					function getElementsfloat (group) {
					  if (!group) return;
					  var els=[];					 
					  $$(document.adminForm.elements).each(function(el){ 
						if (el.id.test(group+'_')) els.push(el);
					  });
						
					  return els;
					}
					
					function jvfloat (selectcontrol) {
						selectedel = selectcontrol.options[selectcontrol.selectedIndex];
						groupsel = selectedel.value.split('_')[0];	
						$$(selectcontrol.options).each(function (el) {
					  	var group = el.value.split('_')[0];
  						var groups = getElementsfloat (group);
  						if (!groups) return;
						var disabled = (groupsel==group)?'':'disabled';
  						groups.each(function(g){
  							g.disabled = disabled;
  						});
					  });
					}
					
					function getTRfloat (el) {
					  el = $(el);
					  var p;
					  while ((p = el.getParent()) && p.tagName != 'TR') {el = p;}
					  
					  return p;
					}
					
					function disableallfloat(){
					  var selectct = $('params".$name."');
					  switchGroup(selectct);					 
					}

					function jvInitfloat () {
					   disableallfloat();
					   document.adminForm.onsubmit = enableall;
					}

					function enableall(){
					  var selectct = $('params".$name."');
					  $$(selectct.options).each(function (el) {
					  	var group = el.value.split('_')[0];
  						var groups = getElementsfloat (group);
  						if (!groups) return;
  						groups.each(function(g){
  							g.disabled = '';
  						});
					  });
					}

					window.addEvent('load', jvInitfloat);
				</script>";
		
			return $str;
	}
}
