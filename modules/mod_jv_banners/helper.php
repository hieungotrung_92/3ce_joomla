<?php
/**
 * @package JV Banner module for Joomla! 1.5
 * @author http://www.ZooTemplate.com
 * @copyright (C) 2010- ZooTemplate.Com
 * @license PHP files are GNU/GPL
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

require_once(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_banners'.DS.'helpers'.DS.'banner.php');

class modjv_BannersHelper
{
	var $_style;
	var $_timeHide;
	var $_Width;
	var $_Catid;
	var $_Count;
	var $_Ordering;
	var $_Cid;
	var $_Target;
	var $_Widthmq;
	var $_timeShow;
	var $_effect;
	var $_html_code;
	var $_use_html;

	function __construct($params) {
		
		$this->_timeHide = $params->get('jv_time_hide', $this->_timeHide);
		
		$this->_style = $params->get ( 'jv_Positions', $this->_style );	
		
		$this->_effect = $params->get ( 'jv_effectst', $this->_effect );
			
		$this->_Catid = $params->get ( 'catid', $this->_Catid );	
		
		$this->_Count = $params->get ( 'count', $this->_Count );

		$this->_Ordering = $params->get ( 'ordering', $this->_Ordering );
		
		$this->_Cid = $params->get ( 'cid', $this->_Cid );
		
		$this->_Target = $params->get ( 'target', $this->_Target );
		
		$this->_timeShow = $params->get ( 'jv_time_show', $this->_timeShow );
		
		$this->_html_code = $params->get('html_code', $this->_html_code);
		
		$this->_use_html = $params->get('use_html', $this->_use_html);
		
	}
	
	function JvBannerSetting() {
		global $mainframe;
		$hs_base = JURI::base () . 'modules/mod_jv_banners/assets/js/';
		$headtag = array ();		
		$headtag [] = "<script type='text/javascript' src='" . $hs_base . "jvbanner.js'></script>";					
		$headtag [] = JHTML::_ ( 'behavior.mootools' );		
		$mainframe->addCustomHeadTag ( implode ( "\n", $headtag ) );
	}
	
	function getStyleBanner($params , $id_mod, $md_title) {
		$this->JvBannerSetting ();
		
		switch ($this->_effect)
		{
		    case ($this->_effect=7):
		        $row->text = $this->showbannerBottomRight ($id_mod, $params, $md_title);
				return $row->text;
		        break;
		    case ($this->_effect=3):
		        $row->text = $this->showbannerBottomLeft ($id_mod, $params, $md_title);
				return $row->text;
		        break;
		    case ($this->_effect=1):
		        $row->text = $this->showbannerTopLeft ($id_mod, $params, $md_title);
				return $row->text;
		        break;
		    case ($this->_effect=5):
		        $row->text = $this->showbannerTopRight ($id_mod, $params, $md_title);
				return $row->text;
		        break;	
		    case ($this->_effect=6):
		        $row->text = $this->showbannerTopRightToLeft ($id_mod, $params, $md_title);
				return $row->text;
		        break;	
		    case ($this->_effect=8):
		        $row->text = $this->showbannerBottomRightToLeft ($id_mod, $params, $md_title);
				return $row->text;
		        break;	
		    case ($this->_effect=2):
		        $row->text = $this->showbannerTopLeftToRight ($id_mod, $params, $md_title);
				return $row->text;
		        break;	
		    case ($this->_effect=4):
		        $row->text = $this->showbannerBottomLeftToRight ($id_mod, $params, $md_title);
				return $row->text;
		        break;	   
		    default :
		        $row->text = $this->showbannerBottomRight ($id_mod, $params, $md_title);
				return $row->text;
		        break;
		} 
		
	}
	
	function showbannerBottomRight($id_mod, $params, $md_title){
		$list = $this->getList($params);	
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'" id="viewjvbanner_'.$id_mod.'">
					<div class="item_banner_'.$this->_effect.'">						
						<div class="bannerheader">
							<div class="jv_marguee_'.$this->_effect.'">';
							if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>
						</div>			
					</div>	
					<div class="showbanner showbanner'.$id_mod.'" id="showbanner'.$id_mod.'">';						
						foreach($list as $item) :
						$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
						endforeach;
				$divs.='						
					</div>
				</div>
			<!--[if lte IE 7]>
			<script language="javascript" type="text/javascript">
				window.addEvent(\'domready\', function() {
					$(\'viewjvbanner_'.$id_mod.'\').setStyles({width:$(\'showbanner'.$id_mod.'\').getStyle(\'width\')});
				});
			</script>
			<![endif]-->
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';
	
	}
	
	function showbannerBottomRightToLeft($id_mod, $params, $md_title){
		$list = $this->getList($params);
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'">
					<div class="item_banner_'.$this->_effect.'">	
						<div class="bannerheader_'.$this->_effect.'" id="bannericon'.$id_mod.'">
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>							
						</div>					
						';
				$divs.='<div class="showbanner'.$id_mod.'" id="showbanner_'.$this->_effect.'">
							<div class="jv_marguee_'.$this->_effect.'">';
								if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
						    <div class="detail'.$this->_effect.'">';						
								foreach($list as $item) :
								$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
								endforeach;						
					 $divs.='
					 		</div>
				 		</div>				 			
				 	';	
				 	
			$divs.='</div>				
				</div>
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';	
	}
	
	function showbannerBottomLeft($id_mod, $params, $md_title){
		$list = $this->getList($params);
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'" id="viewjvbanner_'.$id_mod.'">
					<div class="item_banner_'.$this->_effect.'">						
						<div class="bannerheader">
							<div class="jv_marguee_'.$this->_effect.'">';
								if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>
						</div>			
					</div>	
					<div class="showbanner showbanner'.$id_mod.'" id="showbanner'.$id_mod.'">
					';						
						foreach($list as $item) :
						$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
						endforeach;
				$divs.='						
					</div>
				</div>
			<!--[if lte IE 7]>
			<script language="javascript" type="text/javascript">
				window.addEvent(\'domready\', function() {
					$(\'viewjvbanner_'.$id_mod.'\').setStyles({width:$(\'showbanner'.$id_mod.'\').getStyle(\'width\')});
				});
			</script>
			<![endif]-->
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';
	
	}
	
	function showbannerBottomLeftToRight($id_mod, $params, $md_title){
		$list = $this->getList($params);
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'">
					<div class="item_banner_'.$this->_effect.'">						
						';
				$divs.='<div class="showbanner'.$id_mod.'" id="showbanner_'.$this->_effect.'">
							<div class="jv_marguee_'.$this->_effect.'">';
								if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
						    <div class="detail'.$this->_effect.'">
					';						
								foreach($list as $item) :
								$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
								endforeach;						
					 $divs.='
					 		</div>
				 		</div>
				 		<div class="bannerheader_'.$this->_effect.'" id="bannericon'.$id_mod.'">
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>							
						</div>	
				 	';	
				 	
			$divs.='</div>				
				</div>
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';	
	}	
	
	function showbannerTopRightToLeft($id_mod, $params, $md_title){
		$list = $this->getList($params);
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'">
					<div class="item_banner_'.$this->_effect.'">	
						<div class="bannerheader_'.$this->_effect.'" id="bannericon'.$id_mod.'">
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>							
						</div>					
						';
				$divs.='<div class="showbanner'.$id_mod.'" id="showbanner_'.$this->_effect.'">							
						    <div class="detail'.$this->_effect.'">
					';						
								foreach($list as $item) :
								$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
								endforeach;						
					 $divs.='
					 		</div>
					 		<div class="jv_marguee_'.$this->_effect.'">';
								if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
				 		</div>				 			
				 	';	
				 	
			$divs.='</div>				
				</div>
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';	
	
	}
	function showbannerTopLeftToRight($id_mod, $params, $md_title){
		$list = $this->getList($params);
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'">
					<div class="item_banner_'.$this->_effect.'">						
						';
				$divs.='<div class="showbanner'.$id_mod.'" id="showbanner_'.$this->_effect.'">							
						    <div class="detail'.$this->_effect.'">
					';						
								foreach($list as $item) :
								$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
								endforeach;						
					 $divs.='
					 		</div>
					 		<div class="jv_marguee_'.$this->_effect.'">';
								 if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
				 		</div>
				 		<div class="bannerheader_'.$this->_effect.'" id="bannericon'.$id_mod.'">
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>							
						</div>	
				 	';					 	
			$divs.='</div>				
				</div>
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';	
	
	}	
	function showbannerTopRight($id_mod, $params, $md_title){
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'" id="viewjvbanner_'.$id_mod.'">';
			$divs.='<div class="showbanner showbanner'.$id_mod.'" id="showbanner'.$id_mod.'">
					';
						$list = $this->getList($params);						
						foreach($list as $item) :
						$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
						endforeach;
			$divs.='</div>';
			$divs.='<div class="item_banner_'.$this->_effect.'">						
						<div class="bannerheader">
							<div class="jv_marguee_'.$this->_effect.'">';
								if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
							<div class="ul_element">
								<ul class="element">
									<li class="close" id="close'.$id_mod.'"></li>
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'"></li>
								</ul>
							</div>
						</div>			
					</div>';				
		$divs.='</div>
		<!--[if lte IE 7]>
		<script language="javascript" type="text/javascript">
			window.addEvent(\'domready\', function() {
				$(\'viewjvbanner_'.$id_mod.'\').setStyles({width:$(\'showbanner'.$id_mod.'\').getStyle(\'width\')});
			});
		</script>
		<![endif]-->
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';
	
	}
	
	function showbannerTopLeft($id_mod, $params, $md_title){
		$divs='';
		$divs.='
				<div class="viewjvbanner_'.$this->_effect.'" id="viewjvbanner_'.$id_mod.'">';
			$divs.='<div class="showbanner showbanner'.$id_mod.'" id="showbanner'.$id_mod.'">
					';
						$list = $this->getList($params);												
						foreach($list as $item) :
						$divs.=	modjv_BannersHelper::renderBanner($params, $item, $id_mod);
						endforeach;
			$divs.='</div>';
			$divs.='<div id="item_banner_'.$id_mod.'" class="item_banner_'.$this->_effect.'">						
						<div class="bannerheader">
							<div class="ul_element">
								<ul class="element">
									<li class="opent'.$id_mod.' toggler" id="open_'.$this->_effect.'">'.JText::_("Toggle").'</li>
									<li class="close" id="close'.$id_mod.'">'.JText::_("Close").'</li>
								</ul>
							</div>
							<div class="jv_marguee_'.$this->_effect.'">';
								if($this->_use_html){
					  $divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$md_title.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }else{
					$divs .='<script type="text/javascript">
									 /* <![CDATA[ */
									document.write(\'<marquee id="marquee'.$id_mod.'" scrolldelay="10" onmouseover="this.stop();" direction="left" onmouseout="this.start();" scrollamount="2">\');
									document.write(\''.$list['0']->name.'\');
									document.write(\'<\/marquee>\');
									/* ]]> */
								</script>';
							 }
					$divs .='</div>
						</div>			
					</div>';				
		$divs.='</div>
		<!--[if lte IE 7]>
		<script language="javascript" type="text/javascript">
			window.addEvent(\'domready\', function() {
				$(\'viewjvbanner_'.$id_mod.'\').setStyles({width:$(\'showbanner'.$id_mod.'\').getStyle(\'width\')});
			});
		</script>
		<![endif]-->
		';
		if ($divs != '') {
			return $this->JVbanner ( $divs, $id_mod );
		}
		return '';
	
	}
	function JVbanner($divs, $id_mod) {
		global $mainframe;
		
		if (! defined ( "JVBanners_HEADTAG_" . strtoupper ( $this->_effect ) )) {
			$headtag [] = '<link rel="stylesheet" href="' . JURI::base () . 'modules/mod_jv_banners/assets/css/jvbanner' . $this->_effect . '.css" type="text/css" media="screen"/>';
			$mainframe->addCustomHeadTag ( implode ( "\n", $headtag ) );
			define ( "JVBanners_HEADTAG_" . strtoupper ( $this->_effect ), true );
		}
		
		$html = '';
		$html .= '		
		<div class=\'jvbanner_'.$this->_effect.'\' id="jvbanner'.$id_mod.'">			
			'.$divs.'
		</div>		
		';	
		$html .= "
		<script type=\"text/javascript\">
			window.addEvent('domready', function() {
				var showbanner".$id_mod." = new jvbanners('li.opent".$id_mod."', 'li.close".$id_mod."', 'div.showbanner".$id_mod."', {
					display: false,
					opacity: true,
					openActive: true,";
			if($this->_effect==2 || $this->_effect==4 || $this->_effect==6 || $this->_effect==8){
			$html.="anima: 0,";
			}else{
			$html.="anima: 1,";	
			}
			$html.="timeHide: ".$this->_timeHide.",
					timeShow: ".$this->_timeShow.",	
					id:	".$id_mod."
				},$('jvbanner".$id_mod."'));	
		});
	    </script>";	
		return $html;
	}
	
	function getList(&$params)
	{
		$model		= modjv_BannersHelper::getModel();

		// Model Variables
		$vars['cid']		= (int) $this->_Cid;
		$vars['catid']		= (int) $this->_Catid;
		$vars['limit']		= (int) $this->_Count;
		$vars['ordering']	= $this->_Ordering;

		$banners = $model->getList( $vars );
		$model->impress( $banners );

		return $banners;
	}

	function getModel()
	{
		if (!class_exists( 'BannersModelBanner' ))
		{
			// Build the path to the model based upon a supplied base path
			$path = JPATH_SITE.DS.'components'.DS.'com_banners'.DS.'models'.DS.'banner.php';
			$false = false;

			// If the model file exists include it and try to instantiate the object
			if (file_exists( $path )) {
				require_once( $path );
				if (!class_exists( 'BannersModelBanner' )) {
					JError::raiseWarning( 0, 'Model class BannersModelBanner not found in file.' );
					return $false;
				}
			} else {
				JError::raiseWarning( 0, 'Model BannersModelBanner not supported. File not found.' );
				return $false;
			}
		}

		$model = new BannersModelBanner();
		return $model;
	}

	function renderBanner($params, &$item, $id_mod)
	{
		$link = JRoute::_( 'index.php?option=com_banners&task=click&bid='. $item->bid );
		$baseurl = JURI::base();
		$user_html = $params->get( 'use_html' );
		$target = $params->get( 'target' );
		$html_code = $params->get('html_code');
		
		if ($user_html)
		{
			if($params->get('bannertype')=='float'){
			$html = '<div id="imagefig'.$id_mod.'">'.$html_code.'</div>';			
			}else{
			$html = '<div style="overflow: hidden; display: inline-block" id="imagefig'.$id_mod.'">'.$html_code.'</div>';	
			}
		}
		else if (BannerHelper::isImage( $item->imageurl ))
		{
			
			if($params->get('bannertype')=='float'){
				$div = '<div id="imagefig'.$id_mod.'">';
				$image 	= '<img class="showimg" src="'.$baseurl.'images/banners/'.$item->imageurl.'" alt="'.JText::_('Banner').'" />';
			}else{
				$div = '<div style="overflow: hidden; display: inline-block" id="imagefig'.$id_mod.'">';
				$image 	= '<img class="showimg" src="'.$baseurl.'images/banners/'.$item->imageurl.'" alt="'.JText::_('Banner').'" />';
			}
			if ($item->clickurl)
			{
				switch ($target)
				{
					// cases are slightly different
					case 1:
						// open in a new window
						 $a = '<a href="'. $link .'" target="_blank">';
						break;

					case 2:
						// open in a popup window
						$a = "<a href=\"javascript:void window.open('". $link ."', '', 'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=780,height=550'); return false\">";
						break;

					default:	// formerly case 2
						// open in parent window
						$a = '<a href="'. $link .'">';
						break;
					}

				$html = $div.$a . $image . '</a></div>';
			}
			else
			{
				$html = $div.$image.'</div>';
			}
		}
		else if (BannerHelper::isFlash( $item->imageurl ))
		{
			//echo $item->params;
			$banner_params = new JParameter( $item->params );
			$width = $banner_params->get( 'width');
			$height = $banner_params->get( 'height');

			$imageurl = $baseurl."images/banners/".$item->imageurl;
			$html =	"<object classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000\" codebase=\"http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\" border=\"0\" width=\"$width\" height=\"$height\">
						<param name=\"movie\" value=\"$imageurl\"><embed src=\"$imageurl\" loop=\"false\" pluginspage=\"http://www.macromedia.com/go/get/flashplayer\" type=\"application/x-shockwave-flash\" width=\"$width\" height=\"$height\"></embed>
					</object>";
		}

		return $html;
	}
	
}
