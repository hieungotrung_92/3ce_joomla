<?php
/**
 * @package JV Banner module for Joomla! 1.5
 * @author http://www.ZooTemplate.com
 * @copyright (C) 2010- ZooTemplate.Com
 * @license PHP files are GNU/GPL
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Include the syndicate functions only once
require_once (dirname(__FILE__).DS.'helper.php');

$id_mod = $module->id;
$md_title = $module->title;
$jvBanner = new modjv_BannersHelper($params);
$bannertype = $params->get('bannertype');
if($bannertype=='float'){
	$list = $jvBanner->getList($params, $id_mod);
	JModuleHelper::getLayoutPath('mod_jv_banners','jvfloat');
	require(JModuleHelper::getLayoutPath('mod_jv_banners','jvfloat'));
}else{
	$banner = $jvBanner->getStyleBanner($params, $id_mod, $md_title);
	require(JModuleHelper::getLayoutPath('mod_jv_banners'));
}