<?php 
/**
 * @package JV Banner module for Joomla! 1.5
 * @author http://www.ZooTemplate.com
 * @copyright (C) 2010- ZooTemplate.Com
 * @license PHP files are GNU/GPL
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
$document = &JFactory::getDocument();
$document->addStyleSheet("modules/mod_jv_banners/assets/css/jvfloat.css");
$document->addScript("modules/mod_jv_banners/assets/js/jvfloat.js");
$agent = $_SERVER['HTTP_USER_AGENT'];
$checkie='/msie\s(7\.[0-9]).*(win)/i';
if (preg_match($checkie,$agent)) {$browser = 'IE7';}
//var_dump($params);
if(preg_match('|Safari/([0-9\.]+)|',$agent,$matched)) {
        $browser_version=$matched[1];
        $browser = 'Safari';
}
?>
<?php if($params->get('float_banner')=='right'){?>
<div id="divhieghtright" class="arounddivright">
	<div id="jvFloatRight" style="margin-top: <?php echo $params->get('float_fromtop');?>px;"> 	
		<?php foreach($list as $item) {?>
		<div class="banneritem<?php echo $params->get( 'moduleclass_sfx' ) ?>">
			<?php
				echo modjv_BannersHelper::renderBanner($params, &$item, $id_mod);
			?>
			<div class="clr"></div>
		</div>
		<?php } ?>
	</div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
	var positionFooter; 
	var positionRight = 110;
	var widthAds = <?php echo $params->get('float_right_padding');?>;
	var heightAds = 1;
	window.addEvent('domready', function(){	
		var heightright = parseInt($(document.body).getStyle('height')) + 25 + 'px';			
		$('divhieghtright').setStyle("height", heightright);
		var widthright = parseInt($('jvFloatRight').getStyle('width'));
		<?php if($browser =='IE7'){?>					
			fixwidht = widthright - 34 +'px';
			$('divhieghtright').setStyles({
				'width': fixwidht,
				'padding-right': <?php echo $params->get('float_right_padding');?>+'px'
			});
		<?php }else if($browser=='Safari'){ }else{?>
			$('divhieghtright').setStyles({
				'width': widthright,
				'padding-right': <?php echo $params->get('float_right_padding');?>+'px'
			});
		<?php }?>
	});
	var innerheight = window.innerHeight;
	
	$('jvFloatRight').style.display = "block";
	var yy = 0;
	var toptop=<?php echo $params->get('float_fromtop');?>;
	var beforeTop = <?php echo $params->get('float_fromtop');?>;
	function ShowBannerRight()
	{               
		 yy = f_clientWidth() - widthAds + 68
		 toptop = (f_scrollTop() - beforeTop)/6; 
		 beforeTop = toptop + beforeTop;	
		 var pageHeight = parseInt($(document.body).getStyle('height'))+10;
		 var imgHeightright = parseInt($('jvFloatRight').getStyle('height'));
	     fixtopright = pageHeight - beforeTop;
	     if(fixtopright<imgHeightright){
	    	$('jvFloatRight').addClass('stopRight');
		 }else{
		    if(beforeTop < 5){
		        $('jvFloatRight').style.top = f_scrollTop() + 'px';
		    }else{
		        $('jvFloatRight').style.top = beforeTop + 'px';
			    if(fixtopright>imgHeightright){
			    	$('jvFloatRight').removeClass('stopRight');
				}
		    }
	     }		
		 beforeTop = parseInt($('jvFloatRight').style.top.substring(0,$('jvFloatRight').style.top.length - 2));
		
		 $('jvFloatRight').style.left = yy  + 'px';
		 
		 $('jvFloatRight').style.display = "block";		 
		
		 setTimeout('ShowBannerRight()', 50);    
		     
	}
	window.addEvent('domready', function(){	
		ShowBannerRight();
	});
/* ]]> */
</script>
<?php }else{?>
<div id="divhieght" class="arounddiv">
<div id="jvFloatLeft" style="margin-top: <?php echo $params->get('float_fromtop');?>px;"> 	
	<?php foreach($list as $item) {?>
	<div class="banneritem<?php echo $params->get( 'moduleclass_sfx' ) ?>">
		<?php
			echo modjv_BannersHelper::renderBanner($params, &$item, $id_mod);
		?>
		<div class="clr"></div>
	</div>
	<?php } ?>
</div>
</div>
<script type="text/javascript">
/* <![CDATA[ */
	window.addEvent('domready', function(){	
		var heightleft = parseInt($(document.body).getStyle('height')) + 25 + 'px';
		$('divhieght').setStyle("height",heightleft);
		var widthleft = parseInt($('jvFloatLeft').getStyle('width'));
		<?php if($browser =='IE7'){?>					
			fixwidhtleft = widthleft - 34 +'px';
			$('divhieght').setStyles({
				'width': fixwidhtleft,
				'padding-left': <?php echo $params->get('float_left_padding');?>+'px'
			});
		<?php }else if($browser=='Safari'){ }else{?>
			$('divhieght').setStyles({
				'width': widthleft,
				'padding-left': <?php echo $params->get('float_left_padding');?>+'px'
			});
		<?php }?>
	}); 
	var topleft=<?php echo $params->get('float_fromtop');?>;
	var beforeTopleft = <?php echo $params->get('float_fromtop');?>;
	
	function ShowBannerLeft()
	{               	    
	    topleft = (f_scrollTop() - beforeTopleft)/6;            
	    beforeTopleft = topleft + beforeTopleft;
	    var pageHeight = parseInt($(document.body).getStyle('height'))+10;
	    var imgHeight = parseInt($('jvFloatLeft').getStyle('height'));
	    fixtopleft = pageHeight - beforeTopleft;
	    //console.log(pageHeight)
	    if(fixtopleft<imgHeight){
	    	$('jvFloatLeft').addClass('stopLeft');
		}else{
		    if(beforeTopleft < 5){
		        $('jvFloatLeft').style.top = f_scrollTop() + 'px';
		    }else{
		        $('jvFloatLeft').style.top = beforeTopleft + 'px';

			    if(fixtopleft>imgHeight){
			    	$('jvFloatLeft').removeClass('stopLeft');
				}
		    }
	    }
	    beforeTopleft = parseInt($('jvFloatLeft').style.top.substring(0,$('jvFloatLeft').style.top.length - 2));
	    	    
	    $('jvFloatLeft').style.display = "block";
	                             
	    setTimeout('ShowBannerLeft()', 50);  
	}	
	window.addEvent('domready', function(){	
		ShowBannerLeft();
	});
/* ]]> */
</script>
<?php }?>