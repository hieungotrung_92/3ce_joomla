<?php
/*------------------------------------------------------------------------
 # Yt Content Simple Tabs  - Version 1.1
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://www.ytcvn.com
 -------------------------------------------------------------------------*/
?>
<?php
require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

if (! class_exists("YtContentSimpleTabs") ) {
class YtContentSimpleTabs {
	var $items = array();
	var $featured = 2;	// 0 - without frontpage, 1 - only frontpage - 2 both	
	var $category = 0;
	var $limit_articles = 6;
	var $thumb_width = 100;
	var $thumb_height = 100;
	var $web_url = '';	
	var $thumbnail_mode = 1;
	var $max_title = 25;	
	var $resize_folder = '';
	var $url_to_resize = '';
	var $url_to_module = '';	
    var $count_character = 1; 
	var $customUrl ='';
	var $limit_title_category = 25;
	var $limit_title_article = 25;
	var $cutStr_style_article = 1;
	var $limit_description_article = 200;
	var $character_desc_article = 1;	
	var $sort_order_field = 'created';
	function Content() {
		
	}
		
	function getList() {
			global $mainframe;			
			$items = array();
			$arrCustomUrl = YtContentSimpleTabs::getArrURL($this->customUrl);   
			$db = & JFactory::getDBO ();
			$user = & JFactory::getUser ();
			$aid = $user->get ( 'aid', 0 );
			$aid = 1;
			$contentConfig = &JComponentHelper::getParams ( 'com_content' );
			$noauth = ! $contentConfig->get ( 'shownoauth' );			
			jimport ( 'joomla.utilities.date' );
			$date = new JDate ( );
			$now = $date->toMySQL ();			
			$nullDate = $db->getNullDate ();          
 
 
				$arraycatid = ($this->category);    
				//var_dump($arraycatid); die;
				if (!is_null ($arraycatid)){
					if (!is_array($arraycatid)) {
					 $arraycatid = array($arraycatid);
					}
				
                foreach ($arraycatid as $key=> $value) {			
				 $cats = YtContentSimpleTabs::getSubCategories($value, true);
                 $sql = 'SELECT cc.id as catid, cc.description as catdesc, cc.title as cattitle, cc.access as cat_access, ' . ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug FROM #__categories as cc WHERE cc.id = '."{$value}".'';             
				 $query = 'SELECT a.*,u.name as creater,cc.id as catid, cc.description as catdesc, cc.title as cattitle, cc.access as cat_access, ' . ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,' . ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug FROM #__content AS a' . ' INNER JOIN #__categories AS cc ON cc.id = a.catid' . ' left JOIN #__users AS u ON a.created_by = u.id';          
				 $query .= ' WHERE a.state = 1 '. ($noauth ? ' AND a.access <= ' . ( int ) $aid . ' AND cc.access <= ' . ( int ) $aid : '') . ' AND (a.publish_up = ' . $db->Quote ( $nullDate ) . ' OR a.publish_up <= ' . $db->Quote ( $now ) . ' ) ' . ' AND (a.publish_down = ' . $db->Quote ( $nullDate ) . ' OR a.publish_down >= ' . $db->Quote ( $now ) . ' )' . ' AND cc.published = 1';
                    if (sizeof($cats) > 0) {
                        $query .= " AND a.catid in (". implode(",", $cats) .")";  
                    } else {
                        $query .= " AND a.catid = {$value}";  
                    }
					if ($this->featured == 0) {
						$query .= ' AND a.featured = 0';
					} else if ($this->featured == 1) {
						$query .= ' AND a.featured = 1';
					}
				//	var_dump($this->sort_order_field); die;
						switch ($this->sort_order_field)
                        {
                            case 'created': $ordering = 'a.created DESC';
                                          break;     
							case 'modified': $ordering = 'a.modified DESC';
                                          break;  										  
                            case 'ordering': $ordering = 'a.ordering DESC';
                                          break; 
                            case 'title': $ordering = 'a.title';
                                          break;                                  
                            case 'random': $ordering = 'RAND()';
                                         break;                          
                            default:       $ordering = 'a.cdate';                              
                                         break;
                            }        
                           
						 $query .=  ' ORDER BY ' . $ordering;   
				    
						 $query .=  $this->limit_articles ? ' LIMIT ' . $this->limit_articles : '';					   
						 $db->setQuery($sql);
                         $a = $db->loadObjectlist();
                         $db->setQuery($query);
                         $b = $db->loadObjectlist();
                         $items[$value][0] = $a;
                         $items[$value][1] = $b;
                         }
                         }
            //var_dump($items); die;
			
			global $mainframe;
			JPluginHelper::importPlugin ( 'content' );
			$dispatcher = & JDispatcher::getInstance ();
			$params =& JComponentHelper::getParams('com_content');
            if(!is_null($items)){						
               			
				 foreach ($items as $key=>$value){    
                 $category = array();    
                 $firstval = current ($value[0]);  
             
                 
                if($firstval->cat_access <= $aid ) {
                      
                        $firstval->category_link = JRoute::_(ContentHelperRoute::getCategoryRoute($firstval->catslug));
                     
                        }
                        //var_dump($firstval->category_link); die;     
                        
                $firstval->category_sub_title = $this->cutStr($firstval->cattitle, $this->limit_title_category);

                $category['category_link']= $firstval->category_link;
                $category['category_sub_title']= $firstval->category_sub_title;
                
                $items[$key][0] = $category;        
                           
                $article = array();
                
            
                foreach ($value[1] as $item) {
                 $temp = array();            
                    $item->introtext = trim($item->introtext);
                    $item->text = $item->introtext;
                    $item->text .= $item->fulltext;                    
                    $item->article_image = $this->getImage($item->text);
                    $item->article_image = YtContentSimpleTabs::thumb_mode($item->article_image, $this->thumb_width, $this->thumb_height, $this->thumbnail_mode);        
                      if($item->access <= $aid ) {
                       if(array_key_exists($item->id, $arrCustomUrl))
                         {
                         $item->article_link = $arrCustomUrl[$item->id];
                         }
                        else 
                        {
                        $item->article_link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catslug));
                        }
                        }                        
                    $item->article_desc = $this->removeImage($item->text);
                    
                        if (!isset($item->article_sub_title)){
                        $item->article_sub_title = $this->cutStr($item->title, $this->limit_title_article);
                        }
                        else
                        {
                        $item->article_sub_title = $item->title; 
                        }
                    //    var_dump($this->limit_description_article); die;
                   
                     if (!isset($item->article_content)){
					  if($this->character_desc_article ==1){
							if($this->cutStr_style_article == 1){							
							$item->article_content = KeepingHtml::mb_trim($item->article_desc, $pos = 0, $this->limit_description_article, $hiddenClasses = '', $encoding = 'utf-8');
							}
							else
							{
							$item->article_content = $this->cutStr($item->article_desc, $this->limit_description_article);
							}
						}	
						else
						{
							if($this->cutStr_style_article == 1){							
							$item->article_content = $item->article_desc;
							}
							else
							{
							$item->article_content =  strip_tags($item->article_desc);
							}
						}						
						}
                  
                        if($item->cat_access <= $aid ) {
                       if(array_key_exists($item->catid, $arrCustomUrl))
                         {
                         $item->category_link = $arrCustomUrl[$item->catid];
                         }
                        else 
                        {
                        $item->category_link = JRoute::_(ContentHelperRoute::getCategoryRoute($item->catslug));
                        }
                        }
                      
                    $temp['article_id'] = $item->id;                   
                    $temp['article_sub_title'] = $item->article_sub_title;             
                    $temp['creater'] = $item->creater;
                    $temp['article_image'] = $item->article_image;                        
                    $temp['article_content'] = $item->article_content;  
                    $temp['article_link'] = $item->article_link;
                    $temp['article_link'] = $item->article_link;
                    $temp['category_link'] = $item->category_link;
                    $article[] = $temp;
                        
                }
                
                $items[$key][1] = $article;
                
                }
            
              }
            //  var_dump($items); die;
              return $items;      
            }
            
        function getSubCategories($pId, $clear = false) {
        static $cats = array();
        
        if ($clear == true) {
             $cats = array();
        }
        
        $cats[] = $pId;
        $db = &JFactory::getDBO(); 
        $sql = 'SELECT c.id FROM #__categories c  WHERE c.parent_id = "'. $pId.'"';        
        $db->setQuery($sql);
        $items  = $db->loadObjectList();
        if (sizeof($items) > 0) {
            foreach($items as $item) {
                YtContentSimpleTabs::getSubCategories($item->id);
            }            
        }
        
        return $cats ;
        
         }

		function getImage($str){			
    		$regex = "/\<img.+src\s*=\s*\"([^\"]*)\"[^\>]*\>/";
    		$matches = array();
			preg_match ( $regex, $str , $matches );    
			$images = (count($matches)) ? $matches : array ();
			$image = count($images) > 1 ? $images[1] : '';
						
			return $image;
			}
			
		function removeImage($str) {
		$regex1 = "/\<img[^\>]*>/";
		$str = preg_replace ( $regex1, '', $str );
		$regex1 = "/<div class=\"mosimage\".*<\/div>/";
		$str = preg_replace ( $regex1, '', $str );
		$str = trim ( $str );
		
		return $str;
			}
		function thumb_mode ($image, $image_width, $image_height, $image_mode) {	
				if (!isset($thumb) && ($image != '')) {
						$thumb = $this->processImage($image, $image_width, $image_height, $image_mode);    
					}
					else {
						$thumb = '';
					}   
				return $thumb;
				}
		function getArrURL($customUrl) {     
        $arrUrl = array();
        $tmp = explode("\n", trim($customUrl));            
        foreach( $tmp as $strTmp){
            $pos = strpos($strTmp, ":");
            if($pos >=0){
                $tmpKey = substr($strTmp, 0, $pos);
                $key = trim($tmpKey);
                $tmpLink = substr($strTmp, $pos+1, strlen($strTmp)-$pos);
                $haveHttp =  strpos(trim($tmpLink), "http://");
                //var_dump($haveHttp);die;                   
                if($haveHttp<0 || ($haveHttp !== false) ){
                    $link = trim($tmpLink);
                }else{
                    $link = "http://" . trim($tmpLink);
                }
                $arrUrl[$key] = $link;
            }  
        }            
        return $arrUrl;
		}
		
		
	function processImage($img, $width, $height, $thumbmode) {
		$imagSource = JPATH_SITE.DS. str_replace( '/', DS, $img );	
		
		if(file_exists($imagSource) && is_file($imagSource)) {                             
                        switch ($thumbmode)
                            {
                            case '0': return $img;
                                          break;               
                            case '1': return $this->resizeImage($img, $width, $height);
                                          break;                
                            case '2': return $this->cropImage($img, $width, $height);
                                          break; 
                                     
                            default:  return $this->resizeImage($img, $width, $height);                             
                                         break;
                            }      
            
            
        } else{

            return '';
            
              
		   }
		} 

		
	function resizeImage($imagePath, $width, $height) {
		global $module;
				
		$folderPath = $this->resize_folder;
		 
		 if(!JFolder::exists($folderPath)){
		 		JFolder::create($folderPath);	 
		 }
		 
		 $nameImg = str_replace('/','',strrchr($imagePath,"/"));
			
		 $ext = substr($nameImg, strrpos($nameImg, '.'));
		
		 $file_name = substr($nameImg, 0,  strrpos($nameImg, '.'));
		
		 $nameImg = $file_name . "_" . $width . "_" . $height .  $ext;
		 
		 
		 if(!JFile::exists($folderPath.DS.$nameImg)){
			 $image = new SimpleImage();
	  		 $image->load($imagePath);
	  		 $image->resize($width,$height);
	   		 $image->save($folderPath.DS.$nameImg);
		 }else{
		 		 list($info_width, $info_height) = @getimagesize($folderPath.DS.$nameImg);
		 		 if($width!=$info_width||$height!=$info_height){
		 		 	 $image = new SimpleImage();
	  				 $image->load($imagePath);
	  				 $image->resize($width,$height);
	   				 $image->save($folderPath.DS.$nameImg);
		 		 }
		 }
   		 return $this->url_to_resize . $nameImg;
	}
	
	function cropImage($imagePath, $width, $height) {
		global $module;
		
		$folderPath = $this->resize_folder;
		 
		if(!JFolder::exists($folderPath)){
		 		JFolder::create($folderPath);	 
		}
		 
		$nameImg = str_replace('/','',strrchr($imagePath,"/"));		 
		 
		 if(!JFile::exists($folderPath.DS.$nameImg)){
			 $image = new SimpleImage();
	  		 $image->load($imagePath);
	  		 $image->crop($width,$height);
	   		 $image->save($folderPath.DS.$nameImg);
		 }else{
		 		 list($info_width, $info_height) = @getimagesize($folderPath.DS.$nameImg);
		 		 if($width!=$info_width||$height!=$info_height){
		 		 	 $image = new SimpleImage();
	  				 $image->load($imagePath);
	  				 $image->crop($width,$height);
	   				 $image->save($folderPath.DS.$nameImg);
		 		 }
		 }
		 
   		 return $this->url_to_resize . $nameImg;
	}

	/*Cut string*/
	    function mb_cutsubstrws( $str_text, $number) {

        if( (mb_strlen($str_text) > $number) ) {
    
            $whitespaceposition = mb_strpos($str_text," ",$number)-1;
    
            if( $whitespaceposition > 0 ) {
                $chars = count_chars(mb_substr($str_text, 0, ($whitespaceposition+1)), 1);
                //var_dump($chars);die;
                if (isset($chars[ord('<')]) > isset($chars[ord('>')]))
                    $whitespaceposition = mb_strpos($str_text,">",$whitespaceposition)-1;
                $str_text = mb_substr($str_text, 0, ($whitespaceposition+1));
				$str_text .= '...';
            }
    
            // close unclosed html tags
            if( preg_match_all("|<([a-zA-Z]+)|",$str_text,$aBuffer) ) {
    
                if( !empty($aBuffer[1]) ) {
    
                    preg_match_all("|</([a-zA-Z]+)>|",$str_text,$aBuffer2);
    
                    if( count($aBuffer[1]) != count($aBuffer2[1]) ) {
    
                        foreach( $aBuffer[1] as $index => $tag ) {
    
                            if( empty($aBuffer2[1][$index]) || $aBuffer2[1][$index] != $tag)
                                $str_text .= '</'.$tag.'>';
                        }
                    }
                }
            }
        }
        return $str_text;
    }
	
	function cutStr( $str, $number){
	
		//If length of string less than $number
		$str = strip_tags($str);
		if(strlen($str) <= $number){
			return $str;
		}
		$str = substr($str,0,$number);
	
		$pos = strrpos($str,' ');
	
		return substr($str,0,$pos).'...';
	}
    

  
}
}

if (! class_exists("SimpleImage") ) {
class SimpleImage {
   var $image;
   var $image_type;
 
   function load($filename) {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
     	 
		 
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
   			
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }   
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }   
   }
   function getWidth() {
      return imagesx($this->image);
   }
   function getHeight() {
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   function resize($width,$height) {
   	  $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image	, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image; 
   }    
   function getbeginWidth($width){
   $k = $this->getWidth();
   $x1 = ($k - $width)/2;
   return $x1;
   }
   function getbeginHeight($height){
   $k = $this->getHeight();
   $y1 = ($k - $height)/2;
   return $y1;
   }
   function crop($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, $this->getbeginWidth($width), $this->getbeginHeight($height),  $width, $height, $width, $height);
      $this->image = $new_image;   
   }   
}
}
/* Keeping html tag class*/
if (! class_exists ( 'KeepingHtml' )) {
	class KeepingHtml {
		/*
      $hiddenClasses: Class that have property display: none or invisible.
    */
		function mb_trim($strin, $pos = 0, $len = 10000, $hiddenClasses = '', $encoding = 'utf-8') {
			mb_internal_encoding ( $encoding );
			$strout = trim ( $strin );
			
			$pattern = '/(<[^>]*>)/';
			$arr = preg_split ( $pattern, $strout, - 1, PREG_SPLIT_DELIM_CAPTURE );
			$left = $pos;
			$length = $len;
			$strout = '';
			for($i = 0; $i < count ( $arr ); $i ++) {
				$arr [$i] = trim ( $arr [$i] );
				if ($arr [$i] == '')
					continue;
				if ($i % 2 == 0) {
					if ($left > 0) {
						$t = $arr [$i];
						$arr [$i] = mb_substr ( $t, $left );
						$left -= (mb_strlen ( $t ) - mb_strlen ( $arr [$i] ));
					}
					
					if ($left <= 0) {
						if ($length > 0) {
							$t = $arr [$i];
							$arr [$i] = mb_substr ( $t, 0, $length );
							$length -= mb_strlen ( $arr [$i] );
							if ($length <= 0) {
								$arr [$i] .= '...';
							}
						
						} else {
							$arr [$i] = '';
						}
					}
				} else {
					if (KeepingHtml::isHiddenTag ( $arr [$i], $hiddenClasses )) {
						if ($endTag = KeepingHtml::getCloseTag ( $arr, $i )) {
							while ( $i < $endTag )
								$strout .= $arr [$i ++] . "\n";
						}
					}
				}
				$strout .= $arr [$i] . "\n";
			}
			//echo $strout;  
			return KeepingHtml::toString ( $arr, $len );
		}
		
		function trim($strin, $pos = 0, $len = 10000, $hiddenClasses = '') {
			$strout = trim ( $strin );
			
			$pattern = '/(<[^>]*>)/';
			$arr = preg_split ( $pattern, $strout, - 1, PREG_SPLIT_DELIM_CAPTURE );
			$left = $pos;
			$length = $len;
			$strout = '';
			for($i = 0; $i < count ( $arr ); $i ++) {
				$arr [$i] = trim ( $arr [$i] );
				if ($arr [$i] == '')
					continue;
				if ($i % 2 == 0) {
					if ($left > 0) {
						$t = $arr [$i];
						$arr [$i] = substr ( $t, $left );
						$left -= (strlen ( $t ) - strlen ( $arr [$i] ));
					}
					
					if ($left <= 0) {
						if ($length > 0) {
							$t = $arr [$i];
							$arr [$i] = substr ( $t, 0, $length );
							$length -= strlen ( $arr [$i] );
							if ($length <= 0) {
								$arr [$i] .= '...';
							}
						
						} else {
							$arr [$i] = '';
						}
					}
				} else {
					if (KeepingHtml::isHiddenTag ( $arr [$i], $hiddenClasses )) {
						if ($endTag = KeepingHtml::getCloseTag ( $arr, $i )) {
							while ( $i < $endTag )
								$strout .= $arr [$i ++] . "\n";
						}
					}
				}
				$strout .= $arr [$i] . "\n";
			}
			//echo $strout;  
			return KeepingHtml::toString ( $arr, $len );
		}
		
		function isHiddenTag($tag, $hiddenClasses = '') {
			//By pass full tag like img
			if (substr ( $tag, - 2 ) == '/>')
				return false;
			if (in_array ( KeepingHtml::getTag ( $tag ), array ('script', 'style' ) ))
				return true;
			if (preg_match ( '/display\s*:\s*none/', $tag ))
				return true;
			if ($hiddenClasses && preg_match ( '/class\s*=[\s"\']*(' . $hiddenClasses . ')[\s"\']*/', $tag ))
				return true;
		}
		
		function getCloseTag($arr, $openidx) {
			$tag = trim ( $arr [$openidx] );
			if (! $openTag = KeepingHtml::getTag ( $tag ))
				return 0;
			
			$endTag = "<$openTag>";
			$endidx = $openidx + 1;
			$i = 1;
			while ( $endidx < count ( $arr ) ) {
				if (trim ( $arr [$endidx] ) == $endTag)
					$i --;
				if (KeepingHtml::getTag ( $arr [$endidx] ) == $openTag)
					$i ++;
				if ($i == 0)
					return $endidx;
				$endidx ++;
			}
			return 0;
		}
		
		function getTag($tag) {
			if (preg_match ( '/\A<([^\/>]*)\/>\Z/', trim ( $tag ), $matches ))
				return ''; //full tag
			if (preg_match ( '/\A<([^ \/>]*)([^>]*)>\Z/', trim ( $tag ), $matches )) {
				//echo "[".strtolower($matches[1])."]";
				return strtolower ( $matches [1] );
			}
			//if (preg_match ('/<([^ \/>]*)([^\/>]*)>/', trim($tag), $matches)) return strtolower($matches[1]);
			return '';
		}
		
		function toString($arr, $len) {
			$i = 0;
			$stack = new JAStack ( );
			$length = 0;
			while ( $i < count ( $arr ) ) {
				$tag = trim ( $arr [$i ++] );
				if ($tag == '')
					continue;
				if (KeepingHtml::isCloseTag ( $tag )) {
					if ($ltag = $stack->getLast ()) {
						if ('</' . KeepingHtml::getTag ( $ltag ) . '>' == $tag)
							$stack->pop ();
						else
							$stack->push ( $tag );
					}
				} else if (KeepingHtml::isOpenTag ( $tag )) {
					$stack->push ( $tag );
				} else if (KeepingHtml::isFullTag ( $tag )) {
					//echo "[TAG: $tag, $length, $len]\n";
					if ($length < $len)
						$stack->push ( $tag );
				} else {
					$length += strlen ( $tag );
					$stack->push ( $tag );
				}
			}
			
			return $stack->toString ();
		}
		
		function isOpenTag($tag) {
			if (preg_match ( '/\A<([^\/>]+)\/>\Z/', trim ( $tag ), $matches ))
				return false; //full tag
			if (preg_match ( '/\A<([^ \/>]+)([^>]*)>\Z/', trim ( $tag ), $matches ))
				return true;
			return false;
		}
		
		function isFullTag($tag) {
			//echo "[Check full: $tag]\n";
			if (preg_match ( '/\A<([^\/>]*)\/>\Z/', trim ( $tag ), $matches ))
				return true; //full tag
			return false;
		}
		
		function isCloseTag($tag) {
			if (preg_match ( '/<\/(.*)>/', $tag ))
				return true;
			return false;
		}
	}
	
	class JAStack {
		var $_arr = null;
		function JAStack() {
			$this->_arr = array ();
		}
		
		function push($item) {
			$this->_arr [count ( $this->_arr )] = $item;
		}
		function pop() {
			if (! $c = count ( $this->_arr ))
				return null;
			$ret = $this->_arr [$c - 1];
			unset ( $this->_arr [$c - 1] );
			return $ret;
		}
		function getLast() {
			if (! $c = count ( $this->_arr ))
				return null;
			return $this->_arr [$c - 1];
		}
		function toString() {
			$output = '';
			foreach ( $this->_arr as $item ) {
				$output .= $item . "\n";
			}
			return $output;
		}
	}
}


?>
