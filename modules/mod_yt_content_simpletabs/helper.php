<?php 
/*------------------------------------------------------------------------
 # Yt Content Simple Tabs  - Version 1.1
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://www.ytcvn.com
 -------------------------------------------------------------------------*/

 
 
defined('_JEXEC') or die('Restricted access');
if (! class_exists("modContentSimpleTabsHelper") ) { 
require_once (dirname(__FILE__) .DS. 'assets' .DS.'yt_content_simpletabs.php');

class modContentSimpleTabsHelper {
	var $module_name = '';
		function process($params, $module) {
		
		$enable_cache 		=   $params->get('cache',1);
		$cachetime			=   $params->get('cache_time',0);
		$this->module_name =    $module->module;
		
		if($enable_cache==1) {		
			$conf =& JFactory::getConfig();
			$cache = &JFactory::getCache($module->module);
			$cache->setLifeTime( $params->get( 'cache_time', $conf->getValue( 'config.cachetime' ) * 60 ) );
			$cache->setCaching(true);
			//$cache->setCacheValidation(true);
			$items =  $cache->get( array('modContentSimpleTabsHelper', 'getList'), array($params, $module));
		} else {
			$items = modContentSimpleTabsHelper::getList($params, $module);
		}
		
		return $items;		
		
	}
	
	
	function getList ($params, $module) {
		
		/*$content = new YtcContent();
		$content->featured = $params->get('featured', 2);
		$content->showtype = $params->get('showtype', 1);
		$content->category = $params->get('category', 0);
		$content->listIDs = $params->get('itemIds', '');
		$content->limit = $params->get('total', 5);
		$content->sort_order_field = $params->get('sort_order_field', "created");
		$content->type_order = $params->get('sort_order', "ASC");
		$content->thumb_height = $params->get('thumb_height', "150px");
		$content->thumb_width = $params->get('thumb_width', "120px");
		
		$content->small_thumb_height = $params->get('small_thumb_height', "0");
		$content->small_thumb_width = $params->get('small_thumb_width', "0");
		
		$content->web_url = JURI::base();
		$content->max_title		=   $params->get('limittitle',25);
		$content->max_description		=   $params->get('limit_description',25);
		
		$content->resize_folder = JPATH_CACHE.DS. $this->module_name .DS."images";
		$content->url_to_resize = $content->web_url . "cache/". $this->module_name ."/images/";
		$content->cropresizeimage = $params->get('cropresizeimage', 1);   */
        
        
        
        $content = new YtContentSimpleTabs();
		$content->web_url 				= JURI::base();
        $content->featured 				= $params->get('featured', 2);
		$content->category				= $params->get('category',0);    
		$content->sort_order_field      = $params->get("sort_order_field",'created');   
		$content->limit_articles        = $params->get('limit_articles', 6); 
        $content->thumb_height 			= $params->get('thumb_height', 100);
        $content->thumb_width 			= $params->get('thumb_width', 100);                            
        $content->web_url				= JURI::base();      
        $content->resize_folder 		= JPATH_CACHE.DS. $module->module .DS."images";
        $content->url_to_resize 		= $content->web_url . "cache/". $module->module ."/images/";
        $content->thumbnail_mode		= $params->get('thumbnail_mode', 1);
        $content->count_character 		= $params->get('count_character', 1); 
        $content->read_more_link        = $params->get("read_more_text", '10'); 
        $content->show_title            = $params->get("show_title", 1);
        $content->show_image            = $params->get("show_image", 1); 
        $content->show_read_more_link   = $params->get("show_read_more_link",'1');
        $content->link_image            = $params->get('link_image', 1);  
		$content->featured         		= $params->get("featured", 2);
		$content->customUrl             = $params->get("customUrl", '');
        $content->limit_title_category     		= $params->get('limit_title_category', 25); 
		$content->limit_title_article     		= $params->get('limit_title_article', 25); 
		$content->cutStr_style_article       	= $params->get('cutStr_style_article', 1); 
		$content->limit_description_article   	= $params->get('limit_description_article', 200);   
		$content->character_desc_article		= $params->get('character_desc_article', 1);
		
		$items = $content->getList();
				
		return $items;
	}
	

}
			
} 
if(!class_exists('Browser')){
	class Browser
	{
		private $props    = array("Version" => "0.0.0",
									"Name" => "unknown",
									"Agent" => "unknown") ;
	
		public function __Construct()
		{
			$browsers = array("firefox", "msie", "opera", "chrome", "safari",
								"mozilla", "seamonkey",    "konqueror", "netscape",
								"gecko", "navigator", "mosaic", "lynx", "amaya",
								"omniweb", "avant", "camino", "flock", "aol");
	
			$this->Agent = strtolower($_SERVER['HTTP_USER_AGENT']);
			foreach($browsers as $browser)
			{
				if (preg_match("#($browser)[/ ]?([0-9.]*)#", $this->Agent, $match))
				{
					$this->Name = $match[1] ;
					$this->Version = $match[2] ;
					break ;
				}
			}
		}
	
		public function __Get($name)
		{
			if (!array_key_exists($name, $this->props))
			{
				die("No such property or function {$name}");
			}
			return $this->props[$name] ;
		}
	
		public function __Set($name, $val)
		{
			if (!array_key_exists($name, $this->props))
			{
				SimpleError("No such property or function.", "Failed to set $name", $this->props);
				die;
			}
			$this->props[$name] = $val ;
		}
	
	} 
}	
?>

