<?php
/*------------------------------------------------------------------------
 # Yt Content Simple Tabs  - Version 1.1
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://www.ytcvn.com
 -------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

require_once (dirname(__FILE__).DS.'helper.php');

jimport("joomla.filesystem.folder");
jimport("joomla.filesystem.file");

/*-- Process---*/
$jquery 						= $params->get("jquery", 0);
$theme 							= $params->get("theme", 'default');
$thumb_height 					= $params->get('thumb_height', 100);
$thumb_width 					= $params->get('thumb_width', 100);		
$show_description 				= $params->get('show_description', "0");
$link_image						= $params->get('link_image', 1);
$limit_articles                 = $params->get('limit_articles', 6);   
$read_more_link                 = $params->get("read_more_text", 'read more...'); 
$view_all_text					= $params->get("view_all_text", 'view all'); 
$show_title                     = $params->get("show_title", 1);
$show_image                     = $params->get("show_image",1); 
$show_read_more_link            = $params->get("show_read_more_link",'1');
$show_all_articles    			= $params->get("show_all_articles",'1');
$footer_text           			= $params->get("footer_text",'');
$intro_text           			= $params->get("intro_text",'');
$customUrl            			= $params->get("customUrl", '');
$target            				= $params->get("target", '_self');
$num_items            			= $params->get("num_items", 2);
$thumbnail_mode            		= $params->get("thumbnail_mode", 1);
$limit_items					= $params->get("limit_items", 6); 
$category		 				= $params->get('category', 0); 
$cutStr_style_article           = $params->get('cutStr_style_article', 1);
$limit_title_category     		= $params->get('limit_title_category', 25); 
$limit_title_article     		= $params->get('limit_title_article', 25); 
$width_module					= $params->get('width_module', 700); 
$cutStr_style_article           = $params->get('cutStr_style_article', 1);
$limit_description_article      = $params->get('limit_description_article', 200);
$article_description_color      = $params->get('article_description_color', "#000000");  
$article_title_color            = $params->get("article_title_color", '#000000');   
$link_article_title             = $params->get('link_article_title', 1);
$featured                       = $params->get('featured', 2); 
$sort_order_field               = $params->get("sort_order_field",''); 
$category_title_color           = $params->get("category_title_color", '#000000'); 

//var_dump($thumbnail_mode); die;
	
if (!defined ('YTC_CONTENT_SIMPLETABS')) {
	define ('YTC_CONTENT_SIMPLETABS', 1);

	if (!defined ('YTCJQUERY')){
		define('YTCJQUERY', 1);
		JHTML::script('ytc.jquery-1.5.min.js', JURI::base() . '/modules/'.$module->module.'/assets/js/');				
	}
	/* BOF: New */
	$mainframe = JFactory::getApplication();
	// the a co attributte dlink :)
	if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."slotmachine.js") && $theme=='theme1'){
		JHTML::script("slotmachine.js", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
	}else{
		JHTML::script('slotmachine.js', JURI::base() . '/modules/'.$module->module.'/assets/js/');
	}
	/* BOF: Old */
	//JHTML::script('slotmachine.js',JURI::base() . '/modules/'.$module->module.'/assets/js/');

    /* Add css*/    
    $browser = new Browser();
	
	if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."style.css")){
		JHTML::stylesheet("style.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
	}else{
		JHTML::stylesheet('style.css',JURI::base() . 'modules/'.$module->module.'/assets/');
	}
    
       
    if($browser->Name=='msie' && floor($browser->Version)==6)
    {
        //JHTML::stylesheet('ie6.css', JURI::base() . '/modules/'.$module->module.'/assets/');        
    }
    else if($browser->Name=='msie' && floor($browser->Version)==7)    
    {
		if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."ie7.css")){
			JHTML::stylesheet("ie7.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
		}else{
			JHTML::stylesheet('ie7.css', JURI::base() . '/modules/'.$module->module.'/assets/');
		}
        
    } 
} 
	
$contentHelper = new modContentSimpleTabsHelper();
$items = $contentHelper->process($params, $module);
$count_category = count ($items);

$path = JModuleHelper::getLayoutPath( 'mod_yt_content_simpletabs', $theme );
if (file_exists($path)) {
	require($path);
}
?>
