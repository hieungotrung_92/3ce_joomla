 <?php
/*------------------------------------------------------------------------
 # Ytc Content Simple Tabs  - Version 1.1
 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://wwww.ytcvn.com
 -------------------------------------------------------------------------*/
?>
<style>
#ytc_tabs<?php echo $module->id;?> .article_desc * {
    color: <?php echo $article_description_color;?>!important;
   }
#ytc_tabs<?php echo $module->id;?> .article_title{
    color: <?php echo $article_title_color;?>!important;
   }
    
</style>
<script type="text/javascript">

$jYtc(document).ready(function(jQuery) {
	$jYtc("#ytc_tabs<?php echo $module->id;?>").simpleTabs({		
		}		
	);
});	
</script>
<?php defined( '_JEXEC' ) or die( 'Restricted access' ); ?>

    <div id="page-wrap" class="yt-content-simpletabs">
     <div style="text-align: left; width: <?php echo $width_module;?>px; overflow: hidden"><?php echo $intro_text; ?></div>
        <div id="ytc_tabs<?php echo $module->id;?>" class="theme4" style="width: <?php echo $width_module;?>px; ">
           
            <div class="box-wrapper" style="width:<?php echo $width_module;?>px;">   
                 <?php 
                if(!empty($items)){ $j=1;
                    foreach ($items as $key => $value){                     
                     $firstval = current($value[1]);          
                ?>
                    <div id="<?php echo $module->id;?><?php echo $key;?>" class="content-box <?php if($j==1) echo "current";?>" style="max-width: <?php echo $width_module;?>px; width:<?php echo $width_module;?>px;">
                        <?php if($value[1]){?>
                        <?php if (($show_image == 1) && ($firstval['article_image']!='')){?>
                        <div class="col-one col" style="width: <?php echo $thumb_width;?>px; height: <?php echo $thumb_height;?>px">
                        <?php if($link_image ==1) {?>                              
                        <a href="<?php echo $firstval['article_link'];?>" target="<?php echo $target;?>">
                        <img src="<?php echo $firstval['article_image']?>" href="<?php echo $firstval['article_link'];?>" target = "<?php echo $target;?>"/><br>
                        </a>                                 
                        <?php  } 
                        else 
                        {?>
                         <img src="<?php echo $firstval['article_image']?>"/>
                        <?php } ?>                               
                        </div>
                        <?php } ?>   
                        <div class="col-two col" <?php if(($show_image== 0) || ($firstval['article_image']=='')) {?>style="width:45%" <?php } else {?> style="width:<?php echo ((($width_module- 20 - $thumb_width)/2)-20)?>px;" <?php }?>>
                            <?php if($show_title ==1){?> 
                            <div class="article_title" style="color:<?php echo $article_title_color;?>">                        
                            <?php if($link_article_title ==1){?>
							<a style="color:<?php echo $article_title_color;?>;" href="<?php echo $firstval['article_link'];?>" target="<?php echo $target;?>"> <?php echo $firstval['article_sub_title'];?> </a> 
                            <?php } else { echo $firstval['article_sub_title']; }?>		
                                  
                            </div>
                            <?php } ?>
                             <?php if($show_description ==1){?>   
                            <div class="article_desc" style="color:<?php echo $article_description_color;?>">
                            <?php echo $firstval['article_content']; ?>                            
                            </div>
                            <?php }?>
                            <?php if ($show_read_more_link == 1) {?>   
                           <div class="read_more"> <a href="<?php echo $firstval['article_link'];?>" target="<?php echo $target;?>"><?php echo $read_more_link;?></a>
                           </div>
                            <?php }?>
                        </div>
                       <div class="col-three col" <?php if(($show_image == 0) || ($firstval['article_image']=='')){?>style="width:45%" <?php } else {?> style="width:<?php echo ((($width_module- 30- $thumb_width)/2)-20)?>px;" <?php }?>>
                             <ul class="articles"> <?php $i=1; foreach($value[1] as $key1=>$value1){   
                                 if($i!=1){?>
                                <div class="arrow_articles">
                                <div class="arrow"> &nbsp;</div>
                                <li><a style="color:<?php echo $article_title_color;?>;" href="<?php echo $value1['article_link']?>" target="<?php echo $target; ?>"><?php echo $value1['article_sub_title']?></a></li>
                                </div>
                                 <?php };$i++;}?>                             
                            </ul>
                            
                            <?php if ($show_all_articles ==1) { ?>
                            
                            <div class="link_category">                                
                            <div class="arrow1"> &nbsp;</div>   
                               
                                <a href="<?php echo $value[0]['category_link'];;?>" target="<?php echo $target;?>">
                                <?php echo $view_all_text;?>
                                </a>
                                                
                            </div>
                            <?php }?> 
                        </div>
						 <?php } else {echo JText::_('Has no content to show!');}?>
                    </div>  
                <?php $j++;}}?>
            </div> <!-- END Box Wrapper -->
            
             <ul class="tabs">
            <?php    
           //var_dump($items); die;
               if(!empty($items)){
               foreach ($items as $key=>$value) {  
               $firstval = $value[0];                   
                ?>
                <li>
                <a style="color:<?php echo $category_title_color;?>;" dlink="#<?php echo $module->id;?><?php echo $key;?>">
                <?php echo $firstval['category_sub_title']; ?>
                </a>
                </li>
                <?php }}?>
            </ul>
        </div> <!-- END ytc_tabs -->
     <div style="text-align: left; overflow: hidden; width: <?php echo $width_module;?>px; "><?php echo $footer_text; ?></div>
   </div>