<?php
 # Yt Content Category  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://joomla.ytcvn.com
 #-------------------------------------------------------------------------*/
require_once (JPATH_SITE . '/components/com_content/helpers/route.php');

if (! class_exists("YtContentCategory") ) {
class YtContentCategory {
    var $items = array();
    var $categories = array();  
	var $sub_category = array();  	
	var $limit_sub_category = 6;
	var $max_title = 25;
    var $customUrl = array();     
	var $arrCustomUrl = array(); 
	var $sort_order_field = 'published';
	var $thumb_width = '100';
	var $thumb_height = '100';
	var $sub_thumb_width = '50';
	var $sub_thumb_height = '50';
	var $web_url = '';	
    var $imagesource = 0;		
	var $resize_folder = '';
	var $url_to_resize = '';
	var $url_to_module = '';	
	var $thumbnail_mode = 0;
	var $sub_thumbnail_mode = 0;
	var $limit_title = 25;
	var $limit_sub_title = 25;
	var $level_pfirst = 1;
	
    function Content() {
        
    }
          
    function getList($module) {
            global $mainframe;           
            $arrCustomUrl = YtContentCategory::getArrURL($this->customUrl);            		
			$items = array();				
			$db = & JFactory::getDBO ();
			$user = & JFactory::getUser ();
			$aid = $user->get ( 'aid', 0 );
			$aid = 1;
			$contentConfig = &JComponentHelper::getParams ( 'com_content' );
			$noauth = ! $contentConfig->get ( 'shownoauth' );			
			jimport ( 'joomla.utilities.date' );
			$date = new JDate ( );
			$now = $date->toMySQL ();			
			$nullDate = $db->getNullDate ();     
			$art_id = '';
				
				
				$arraycatid = ($this->categories);    
				//var_dump($arraycatid); die;
				$catids = array();    
				if (!is_null ($arraycatid)){
                    if (!is_array($arraycatid)) {
                     $arraycatid = array($arraycatid);
                    }
                    
                foreach ($arraycatid as $key=>$value){    
				$sql = "SELECT c.id FROM #__categories as c";   
				$sql .= " WHERE c.level = {$this->level_pfirst} AND c.id = {$value}";	
					   
				    $db->setQuery($sql);					
					//echo $db->getQuery() . '<br/><br/>';					
                    $a = $db->loadObjectlist();    
                    //var_dump($a); die;
                    if (count($a)>0){
                    $catids[$key] = $a;                  
					}                       
			     }  
                } 
              // var_dump($catids); die;         
				    $contentcatids = array();
					foreach ($catids as $key=>$value) { $firstval = current($value); 
					$contentcatids[$key]['catid'] = $firstval->id; 
					}  
				//var_dump($contentcatids); die;
					
				   $sub_catids = array();
                   if (!is_null ($arraycatid)){                      
                       
                    foreach ($arraycatid as $key=>$value){ 
                    $sql = "SELECT c.id FROM #__categories as c";
                    $sql .= " WHERE c.level = {($this->level_pfirst+1)}"
                         . " AND c.id = {$value}";    
                           
                        $db->setQuery($sql);                    
                        //echo $db->getQuery() . '<br/><br/>';                    
                        $a = $db->loadObjectlist(); 
                      //  var_dump($a); die;
						if (count($a)>0){
                        $sub_catids[$key] = $a;                  
                        }  
                     }  
                    }
					//var_dump($sub_catids); die;

						
                   if(!empty($sub_catids)) {
                   foreach($sub_catids as $key=>$value){ $firstval = current($value);
                    $art_id .= $firstval->id.',';
                    }
                      $art_id = substr($art_id,0,strlen($art_id)-1);
                       }
                         
                       
                   // var_dump($art_id); die;
                 
			        if (!empty ($contentcatids)){	
					foreach ($contentcatids as $key=>$value) {	
				  //$cats = YtK2Category::getSubCategories($value, true);
					  $sql = "SELECT c.* FROM #__categories as c";
					  $sql .= " WHERE c.published = 1"
						   . " AND c.access <= {$aid}"						  			
						   . " AND c.id = {$value['catid']}"; 	
						
					  $query = "SELECT c.* FROM #__categories as c";
					  $query .= " WHERE c.published = 1"
							 . " AND c.access <= {$aid}"											
							 . " AND c.parent_id = {$value['catid']}"; 	
				
                    if( !empty($art_id)) { 
                        $query .= " AND c.id IN (". $art_id .") ";
                    }  
                    if ($this->sort_order_field == 'random'){
							$query .=  ' GROUP BY c.id ORDER BY rand()';
						}else {
							$query .=   ' GROUP BY c.id  ORDER BY ' . $this->sort_order_field;			
						}					 		 
							        
							$query .=  $this->limit_sub_category ? ' LIMIT ' . $this->limit_sub_category : '';
							
					$db->setQuery($sql);					
					//echo $db->getQuery() . '<br/><br/>';					
                    $a = $db->loadObjectlist();
                   // var_dump($a); die;
					$db->setQuery($query);					
					//echo $db->getQuery() . '<br/><br/>';					
                    $b = $db->loadObjectlist();
                    //var_dump($b); die;			
					$category[$key][0] = $a;
					$category[$key][1] = $b;
                   }
                 }
			    //var_dump($category); die;
                if(!empty($category)) {
				 foreach ($category as $key=>$item) { 
				  $temp1 = array();  	
				  foreach ($item[0] as $key1=>$value1) { 
				  $temp = array();
				  				
				    $value1->category_image = $this->getImage($value1->description);
				    if (!file_exists($value1->category_image))
					{
					$value1->category_image = 'modules/'.$module->module.'/images/no_image.gif';
					}
					$value1->category_image = YtContentCategory::thumb_mode($value1->category_image, $this->thumb_width, $this->thumb_height, $this->thumbnail_mode);		
					
					   if($value1->access <= $aid ) {
                      
						$value1->category_link = JRoute::_(ContentHelperRoute::getCategoryRoute($value1->id));
						
						}	
						if (!isset($value1->category_sub_title)) 
						{
						$value1->category_sub_title = $this->cutStr($value1->title, $this->limit_title);
						}	
						$temp['category_id'] = $value1->id;				    
						$temp['category_thumb'] = $value1->category_image;				
						$temp['category_desc'] = $this->removeImage($value1->description);	   			
						$temp['category_link'] = $value1->category_link;
						$temp['category_title'] = $value1->category_sub_title;					
						$temp1[] = $temp;
						} 
					  $item[0] = $temp1;			
				     //var_dump($item[0]); die;
					
					  $temp2 = array();  				 
				  
   				     if(!empty($item[1]))
					 { 
					 foreach ($item[1] as $key1=>$value1) { 		
				     $temp = array();
				     $query = "SELECT i.id FROM #__content as i";
					 $query .= " WHERE i.access <= {$aid}"												
							. " AND i.catid = {$value1->id}"; 
					  $db->setQuery($query);					
						//echo $db->getQuery() . '<br/><br/>';					
					  $a = $db->loadObjectlist();
					  
						$value1->count_articles = count($a);
						$value1->category_image = $this->getImage($value1->description);
						if (!file_exists($value1->category_image))
						{
						$value1->category_image = 'modules/'.$module->module.'/images/no_image.gif';
						}
						$value1->category_image = YtContentCategory::thumb_mode($value1->category_image, $this->sub_thumb_width, $this->sub_thumb_height, $this->sub_thumbnail_mode);		
					
					   if($value1->access <= $aid ) {
                       if(array_key_exists($value1->id, $arrCustomUrl))
						 {
						 $value1->category_link = $arrCustomUrl[$value1->id];
						 }
						else 
						{
						$value1->category_link = JRoute::_(ContentHelperRoute::getCategoryRoute($value1->id));
						}
						}	
						if (!isset($value1->category_sub_title)) 
						{
						$value1->category_sub_title = $this->cutStr($value1->title, $this->limit_sub_title);
						}	
						
						$temp['category_id'] = $value1->id;				    
						$temp['category_thumb'] = $value1->category_image;				
						$temp['category_desc'] = $this->removeImage($value1->description);	   			
						$temp['category_link'] = $value1->category_link;
						$temp['category_title'] = $value1->category_sub_title;		
						$temp['count_articles'] = $value1->count_articles;
						$temp2[] = $temp;
							
						}
						}
					 $item[1] = $temp2;		
					//var_dump($item[1]); die;
					
					$category[$key][0] = $item[0];
					$category[$key][1] = $item[1];
					}
				//	var_dump($category); die;
					
					return $category; 
                      }        
                   
                     }    
                     
		 	function getImage($str){			
    		$regex = "/\<img.+src\s*=\s*\"([^\"]*)\"[^\>]*\>/";
    		$matches = array();
			preg_match ( $regex, $str , $matches );    
			$images = (count($matches)) ? $matches : array ();
			$image = count($images) > 1 ? $images[1] : '';
						
			return $image;
			}
			
			function removeImage($str) {
			$regex1 = "/\<img[^\>]*>/";
			$str = preg_replace ( $regex1, '', $str );
			$regex1 = "/<div class=\"mosimage\".*<\/div>/";
			$str = preg_replace ( $regex1, '', $str );
			$str = trim ( $str );
			
			return $str;
			}
			function thumb_mode ($image, $image_width, $image_height, $image_mode) {	
				if (!isset($thumb) && ($image != '')) {
						$thumb = $this->processImage($image, $image_width, $image_height, $image_mode);    
					}
					else {
						$thumb = '';
					}   
				return $thumb;
				}
	
		
			
        function getArrURL($customUrl) {     
        $arrUrl = array();
        $tmp = explode("\n", trim($customUrl));            
        foreach( $tmp as $strTmp){
            $pos = strpos($strTmp, ":");
            if($pos >=0){
                $tmpKey = substr($strTmp, 0, $pos);
                $key = trim($tmpKey);
                $tmpLink = substr($strTmp, $pos+1, strlen($strTmp)-$pos);
                $haveHttp =  strpos(trim($tmpLink), "http://");
                //var_dump($haveHttp);die;                   
                if($haveHttp<0 || ($haveHttp !== false) ){
                    $link = trim($tmpLink);
                }else{
                    $link = "http://" . trim($tmpLink);
                }
                $arrUrl[$key] = $link;
            }  
				}            
				return $arrUrl;
			}
		
		
		function processImage($img, $width, $height, $thumbmode) {
		$imagSource = JPATH_SITE.DS. str_replace( '/', DS, $img );	
		
		if(file_exists($imagSource) && is_file($imagSource)){
	
    		if ($thumbmode == 0){

    			return $this->resizeImage($img, $width, $height);

    		} else {

    			return $this->cropImage($img, $width, $height);

    		}

			} else{
	
            return '';
		   }
		} 	


		
	function resizeImage($imagePath, $width, $height) {
		global $module;
				
		$folderPath = $this->resize_folder;
		 
		 if(!JFolder::exists($folderPath)){
		 		JFolder::create($folderPath);	 
		 }
		 
		 $nameImg = str_replace('/','',strrchr($imagePath,"/"));
			
		 $ext = substr($nameImg, strrpos($nameImg, '.'));
		
		 $file_name = substr($nameImg, 0,  strrpos($nameImg, '.'));
		
		 $nameImg = $file_name . "_" . $width . "_" . $height .  $ext;
		 
		 
		 if(!JFile::exists($folderPath.DS.$nameImg)){
			 $image = new SimpleImage();
	  		 $image->load($imagePath);
	  		 $image->resize($width,$height);
	   		 $image->save($folderPath.DS.$nameImg);
		 }else{
		 		 list($info_width, $info_height) = @getimagesize($folderPath.DS.$nameImg);
		 		 if($width!=$info_width||$height!=$info_height){
		 		 	 $image = new SimpleImage();
	  				 $image->load($imagePath);
	  				 $image->resize($width,$height);
	   				 $image->save($folderPath.DS.$nameImg);
		 		 }
		 }
   		 return $this->url_to_resize . $nameImg;
	}
	
	function cropImage($imagePath, $width, $height) {
		global $module;
		
		$folderPath = $this->resize_folder;
		 
		if(!JFolder::exists($folderPath)){
		 		JFolder::create($folderPath);	 
		}
		 
		$nameImg = str_replace('/','',strrchr($imagePath,"/"));		 
		 
		 if(!JFile::exists($folderPath.DS.$nameImg)){
			 $image = new SimpleImage();
	  		 $image->load($imagePath);
	  		 $image->crop($width,$height);
	   		 $image->save($folderPath.DS.$nameImg);
		 }else{
		 		 list($info_width, $info_height) = @getimagesize($folderPath.DS.$nameImg);
		 		 if($width!=$info_width||$height!=$info_height){
		 		 	 $image = new SimpleImage();
	  				 $image->load($imagePath);
	  				 $image->crop($width,$height);
	   				 $image->save($folderPath.DS.$nameImg);
		 		 }
		 }
		 
   		 return $this->url_to_resize . $nameImg;
	}
	
	/*Cut string*/
	function cutStr( $str, $number){
		//If length of string less than $number
		$str = strip_tags($str);
		if(strlen($str) <= $number){
			return $str;
		}
		$str = substr($str,0,$number);
	
		$pos = strrpos($str,' ');
	
		return substr($str,0,$pos).'...';
	}
   
}
}

if (! class_exists("SimpleImage") ) {
class SimpleImage {
   var $image;
   var $image_type;
 
   function load($filename) {
      $image_info = getimagesize($filename);
      $this->image_type = $image_info[2];
     	 
		 
      if( $this->image_type == IMAGETYPE_JPEG ) {
         $this->image = imagecreatefromjpeg($filename);
      } elseif( $this->image_type == IMAGETYPE_GIF ) {
         $this->image = imagecreatefromgif($filename);
      } elseif( $this->image_type == IMAGETYPE_PNG ) {
         $this->image = imagecreatefrompng($filename);
      }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
   			
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image,$filename,$compression);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image,$filename);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image,$filename);
      }   
      if( $permissions != null) {
         chmod($filename,$permissions);
      }
   }
   function output($image_type=IMAGETYPE_JPEG) {
      if( $image_type == IMAGETYPE_JPEG ) {
         imagejpeg($this->image);
      } elseif( $image_type == IMAGETYPE_GIF ) {
         imagegif($this->image);         
      } elseif( $image_type == IMAGETYPE_PNG ) {
         imagepng($this->image);
      }   
   }
   function getWidth() {
      return imagesx($this->image);
   }
   function getHeight() {
      return imagesy($this->image);
   }
   function resizeToHeight($height) {
      $ratio = $height / $this->getHeight();
      $width = $this->getWidth() * $ratio;
      $this->resize($width,$height);
   }
   function resizeToWidth($width) {
      $ratio = $width / $this->getWidth();
      $height = $this->getheight() * $ratio;
      $this->resize($width,$height);
   }
   function scale($scale) {
      $width = $this->getWidth() * $scale/100;
      $height = $this->getheight() * $scale/100; 
      $this->resize($width,$height);
   }
   function resize($width,$height) {
   	  $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image	, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
      $this->image = $new_image; 
   }    
   function getbeginWidth($width){
   $k = $this->getWidth();
   $x1 = ($k - $width)/2;
   return $x1;
   }
   function getbeginHeight($height){
   $k = $this->getHeight();
   $y1 = ($k - $height)/2;
   return $y1;
   }
   function crop($width,$height) {
      $new_image = imagecreatetruecolor($width, $height);
      imagecopyresampled($new_image, $this->image, 0, 0, $this->getbeginWidth($width), $this->getbeginHeight($height),  $width, $height, $width, $height);
      $this->image = $new_image;   
   }   
}
}

?>
