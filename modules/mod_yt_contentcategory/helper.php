<?php 
 # Yt Content Category  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://joomla.ytcvn.com
 #-------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');
require_once (JPATH_SITE . '/components/com_content/helpers/route.php');
if (! class_exists("modContentCategory") ) { 
require_once (dirname(__FILE__) .DS. 'assets' .DS.'yt_content_category.php');

class modContentCategory {
	
	function process($params, $module) {
		
		$enable_cache 		=   $params->get('cache',1);
		$cachetime			=   $params->get('cache_time',0);		
		
		if($enable_cache==1) 
        {	$conf =& JFactory::getConfig();
			$cache = &JFactory::getCache($module->module);
			$cache->setLifeTime( $params->get( 'cache_time', $conf->getValue( 'config.cachetime' ) * 60 ) );
			$cache->setCaching(true);
			//$cache->setCacheValidation(true);
			$items =  $cache->get( array('modContentCategory', 'getList'), array($params, $module));
		} else {
			    $items = modContentCategory::getList($params, $module);
		        }
		
		return $items;		
		
	    }
	
	
	function getList ($params, $module) {

		$content = new YtContentCategory();	
		$content->categories 					= $params->get('category', 0);   
        $content->web_url 						= JURI::base();                  
        $content->resize_folder 				= JPATH_CACHE.DS. $module->module .DS."images";
        $content->url_to_resize					= $content->web_url . "cache/". $module->module ."/images/";   
		$content->show_title 					= $params->get("show_title", 1);
		$content->thumb_height 					= $params->get('thumb_height', "100");
		$content->thumb_width 					= $params->get('thumb_width', "100");	
		$content->sub_thumb_height 				= $params->get('sub_thumb_height', "50");
		$content->sub_thumb_width 				= $params->get('sub_thumb_width', "50");	
		$content->max_title						= $params->get('link_title', 1);
		$content->link_image					= $params->get('link_image', 1);		
		$content->limit_sub_category		    = $params->get('limit_sub_category', 6); 
		$content->customUrl		                = $params->get("customUrl", '');     
		$content->sort_order_field         		= $params->get("sort_order_field",'published');
		$content->thumbnail_mode       			= $params->get("thumbnail_mode", 0);
		$content->sub_thumbnail_mode       		= $params->get("sub_thumbnail_mode", 0);
		$content->limit_title					= $params->get('limit_title', 25); 
		$content->limit_sub_title				= $params->get('limit_sub_title', 25);
		$content->sort_order_field				= $params->get('sort_order_field', 'published');
		$content->level_pfirst					= $params->get('level_pfirst', '1'); 
        
		$items 									= $content->getList($module);
				
		return $items;
       // var_dump($items); die;
	}  
} 
}
if(!class_exists('Browser')){
	class Browser
	{
		private $props    = array("Version" => "0.0.0",
									"Name" => "unknown",
									"Agent" => "unknown") ;
	
		public function __Construct()
		{
			$browsers = array("firefox", "msie", "opera", "chrome", "safari",
								"mozilla", "seamonkey",    "konqueror", "netscape",
								"gecko", "navigator", "mosaic", "lynx", "amaya",
								"omniweb", "avant", "camino", "flock", "aol");
	
			$this->Agent = strtolower($_SERVER['HTTP_USER_AGENT']);
			foreach($browsers as $browser)
			{
				if (preg_match("#($browser)[/ ]?([0-9.]*)#", $this->Agent, $match))
				{
					$this->Name = $match[1] ;
					$this->Version = $match[2] ;
					break ;
				}
			}
		}
	
		public function __Get($name)
		{
			if (!array_key_exists($name, $this->props))
			{
				die("No such property or function {$name}");
			}
			return $this->props[$name] ;
		}
	
		public function __Set($name, $val)
		{
			if (!array_key_exists($name, $this->props))
			{
				SimpleError("No such property or function.", "Failed to set $name", $this->props);
				die;
			}
			$this->props[$name] = $val ;
		}
	
	} 
}	
?>

