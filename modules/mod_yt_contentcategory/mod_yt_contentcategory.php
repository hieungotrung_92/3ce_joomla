<?php
 # Yt Content Category  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://joomla.ytcvn.com
 #-------------------------------------------------------------------------*/


defined( '_JEXEC' ) or die( 'Restricted access' );

require_once (dirname(__FILE__).DS.'helper.php');

jimport("joomla.filesystem.folder");
jimport("joomla.filesystem.file");

/*-- Process---*/
$target 						= $params->get("target", '');
$jquery 						= $params->get("jquery", 0);
$theme 							= $params->get("theme", 'default');
$title_color 					= $params->get("title_color", '#000000');
$sub_title_color 				= $params->get("sub_title_color", '#000000');
$show_title 					= $params->get("show_title", 1);
$show_sub_title 				= $params->get("show_sub_title", 1);
$thumb_height 					= $params->get('thumb_height', "100");
$thumb_width 					= $params->get('thumb_width', "100");	
$sub_thumb_height 				= $params->get('sub_thumb_height', "50");
$sub_thumb_width 				= $params->get('sub_thumb_width', "50");	
$link_title						= $params->get('link_title', 1);
$link_sub_title					= $params->get('link_sub_title', 1);
$link_image						= $params->get('link_image', 1);
$link_sub_image					= $params->get('link_sub_image', 1);
$background_color				= $params->get('background_color', "#A52A2A");
$limit_sub_category				= $params->get('limit_sub_category', 6); 
$limit_title					= $params->get('limit_title', 25); 
$limit_sub_title				= $params->get('limit_sub_title', 25); 
$customUrl		            	= $params->get("customUrl", '');
$footer_text           			= $params->get("footer_text",'');
$intro_text           			= $params->get("intro_text",'');
$category_columns           	= $params->get("category_columns", 4);
$sub_category_colums           	= $params->get("sub_category_colums", 4);
$sort_order_field           	= $params->get("sort_order_field",'published');
$thumbnail_mode       			= $params->get("thumbnail_mode", 0);
$sub_thumbnail_mode       		= $params->get("sub_thumbnail_mode", 0);
$content_box_width       		= $params->get("content_box_width", 500);
$show_image       				= $params->get("show_image", 1);
$show_small_image       		= $params->get("show_small_image", 1);
$tab_event       				= $params->get("tab_event", 'click');
$show_total_articles       		= $params->get("show_total_articles", 1);
$width_module			    	= $params->get("width_module", 700);
$width_article_content			= $params->get("width_article_content", 220);
$categories 					= $params->get("category", 0);     


if (!defined ('YT_CONTENT_CATEGORY')){
	define('YT_CONTENT_CATEGORY', 1);
	
	if (!defined ('YTCJQUERY')){
		define('YTCJQUERY', 1);
		JHTML::script('ytc.jquery-1.5.min.js', JURI::base() . '/modules/'.$module->module.'/assets/js/');				
	}	
	//JHTML::script('jquery.accordion.js',JURI::base() . 'modules/'.$module->module.'/assets/js/');		
JHTML::script('jquery.hoveraccordion.js',JURI::base() . 'modules/'.$module->module.'/assets/js/');	
	
	/* Add css*/	
	$browser = new Browser();
	$mainframe = JFactory::getApplication();
	if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."style.css")){
		JHTML::stylesheet("style.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
	}else{
		JHTML::stylesheet('style.css',JURI::base() . 'modules/'.$module->module.'/assets/');
	}
       
    if($browser->Name=='msie' && floor($browser->Version)==7)    
    {
		if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."ie7.css")){
			JHTML::stylesheet("ie7.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
		}else{
			JHTML::stylesheet('ie7.css',JURI::base() . 'modules/'.$module->module.'/assets/');
		}

    }	
}


$contentHelper = new modContentCategory();
$items = $contentHelper->process($params, $module);	

//var_dump($items); die;
$count_category = count($items); 
if($category_columns>$count_category){
    $category_columns = $count_category;
}
$path = JModuleHelper::getLayoutPath( 'mod_yt_contentcategory', $theme );
if (file_exists($path)) {
	require($path);
}
?>
