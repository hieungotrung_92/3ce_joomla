<?php        
/*------------------------------------------------------------------------
 # Yt Content Category  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://joomla.ytcvn.com
 -------------------------------------------------------------------------*/
?>
    <?php if(!empty($items)){?> 
    <div id="page-wrap" class="yt-contentcategory">
     <div style="text-align: left; width:<?php echo $width_module;?>px;"><?php echo $intro_text; ?></div>
        <div id="ytc_tabs<?php echo $module->id;?>" class="theme2" style="overflow: hidden">  
            <div class="box-wrapper" style="width:<?php echo $width_module;?>px;">
                <?php  $j=1; $ij=1;	$count_items = count($items);  
                       foreach ($items as $key=>$item){ $firstval = current($item[0]);							 
					   if($ij==1) {?>										
                       <div class="content-box <?php if($j==1) echo "current";?>" style="max-width:<?php echo $width_module;?>px !important;width:<?php echo $width_module;?>px !important; margin-bottom: 10px; float: left;"> <?php }?>						
						  <div class="sub_content" style="float: left; width:<?php echo ($width_module/$category_columns);?>px!important; overflow: hidden">    <div class="category_element" style="padding-right: 10px">  
						       <div class="category" style=" float: left; padding-right: 10px">
									<?php if ($show_title == 1){ ?>
									<div class="title" style="color:<?php echo $title_color;?>; font-weight: bold; text-align: left; margin-bottom: 5px"> 
										<?php if($link_title ==1) {?>		
										<a style="color:<?php echo $title_color;?>; font-weight: bold" href="<?php echo $firstval['category_link'];?>" target = "<?php echo $target;?>" > <?php echo $firstval['category_title'];?> </a> <br>                                     									
										<?php } else { echo $firstval['category_title']; }?> 									
									</div>
									<?php }	?>
									<?php if (($show_image == 1)&& ($firstval['category_thumb']!='')){ ?>
									<div class="image_category" style="float: left; margin-right: 10px;">
										<?php if($link_image ==1) {?>		
										<a href="<?php echo $firstval['category_link'];?>" target = "<?php echo $target;?>" > 
										<img src="<?php echo $firstval['category_thumb']?>" href="<?php echo $firstval['category_link'];?>" target = "<?php echo $target;?>"/>
										</a>                                      									
										<?php } 
										else 
										{?>
										<img src="<?php echo $firstval['category_thumb']?>"/>
										<?php } ?>								
									</div>
									<?php } ?>										
								  </div>								  
								  <div class="sub-category" style="overflow: hidden">
									<?php if (!empty($item[1])) { foreach ($item[1] as $key1=>$value1) {?>
										<div class="sub_category" style="overflow: hidden; line-height: 20px">													
										<?php if ($show_sub_title == 1){ ?>
										<div style="overflow: hidden">
										<div class="arrow">&nbsp;&nbsp;&nbsp;</div> 
										<div class="sub_title" style="color:<?php echo $sub_title_color;?>;"> 										
											<?php if($link_sub_title ==1) {?>		
											<a style="color:<?php echo $sub_title_color;?>"; href="<?php echo $value1['category_link'];?>" target = "<?php echo $target;?>"> <?php echo $value1['category_title'];?> </a> <br>                                     									
											<?php } else
											{
											echo $value1['category_title']; 
											}?> 									
										</div>
										<?php if ($show_total_articles ==1) {?>
										<div class="num_items" style=""><?php echo '('.$value1['count_articles'].')';?></div>								
										<?php }?>								
										</div>
										<?php }?>
										</div>
									<?php }} else {echo JText::_('No sub-categories to show!'); } ?>	
								 </div>	
							  </div><!--end category_element-->
						    </div> <!-- END sub_content -->
						   	<?php if(($count_items) == $ij){?>
							</div><?php } else {?>
							<?php if($ij%$category_columns ==0) {?> </div><div class="content-box" style="max-width:<?php echo $width_module;?>px !important;width:<?php echo $width_module;?>px !important; margin-bottom: 10px; float: left;"> <?php }?>
							<?php }?>    
							<?php $ij++;?>  
					
				<?php  $j++;}?>
            </div> <!-- END Box Wrapper -->
        </div> <!-- END ytc_tabs -->
       <div style="text-align: left; width:<?php echo $width_module;?>px; overflow: hidden"><?php echo $footer_text; ?></div>
    </div>
 <?php } else { echo JText::_('Has no content to show!');}?>