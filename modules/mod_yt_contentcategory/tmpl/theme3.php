<?php        
 # Yt Content Category  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://joomla.ytcvn.com
 #-------------------------------------------------------------------------*/
?>
     <?php if(!empty($items)){?> 
	 <div id="page-wrap" class="yt-contentcategory">
     <div style="text-align: left; width:<?php echo $width_module;?>px;"><?php echo $intro_text; ?></div>
        <div id="ytc_tabs<?php echo $module->id;?>" class="theme3" style="overflow: hidden">  
            <div class="box-wrapper" style="overflow: hidden">
                <?php  $j=1; $ij=1;	$count_items = count($items);  
                       foreach ($items as $key=>$item){ $firstval = current($item[0]);							 
						  if($ij==1) {?>										
                          <div class="content-box <?php if($j==1) echo "current";?>" style="max-width:<?php echo $width_module;?>px !important; width:<?php echo $width_module;?>px !important;" > <?php }?>						
						  <div class="sub_content" style="float: left; position:relative; width:<?php echo ($width_module/$category_columns);?>px!important; overflow: hidden"> 						
									<?php if (($show_image == 1)&& ($firstval['category_thumb']!='')){ ?>
									<div class="image" style="margin-right: 10px; margin-bottom: 10px">
									<?php if($link_image ==1) {?>		
									<a href="<?php echo $firstval['category_link'];?>" target = "<?php echo $target;?>" > 
									<img src="<?php echo $firstval['category_thumb']?>" href="<?php echo $firstval['category_link'];?>" target = "<?php echo $target;?>"/><br>
									</a>                                      									
									<?php } 
									else 
									{?>
									<img src="<?php echo $firstval['category_thumb']?>"/>
									<?php } ?>								
									</div>
									<?php } ?>	
									<?php if ($show_title == 1){ ?>
									<div class="title" style="color:<?php echo $title_color;?>; overflow: hidden"> 
									<?php if($link_title ==1) {?>		
									<a style="color:<?php echo $title_color;?>; font-weight: bold; text-decoration: underline;" href="<?php echo $firstval['category_link'];?>" target = "<?php echo $target;?>" > <?php echo $firstval['category_title'];?> </a> <br>                                     									
									<?php } else { echo $firstval['category_title']; }?> 									
									</div>
									<?php }	?>							    
                                    												
									
									<div class="sub_category" style="color:<?php echo $sub_title_color;?>">
									<?php if(!empty($item[1])){ if ($show_sub_title == 1){ ?>									
									<?php $i=1; foreach ($item[1] as $key1=>$value1) { $count = count($item[1]);?>									
										<div class="sub_title" style="float: left">
										<?php if($link_sub_title ==1) {?>		
										<a style="color:<?php echo $sub_title_color;?>" href="<?php echo $value1['category_link'];?>" target = "<?php echo $target;?>"> 
										<?php if(($i >=1) && ($i < $count)) 
										{
										echo $value1['category_title'].','.'&nbsp;'; 
										}
										else { 
										echo $value1['category_title']; 
										}?>
										</a>                                     									
										<?php } else { 
										if(($i >=1) && ($i < $count)) 
										{
										echo $value1['category_title'].','.'&nbsp;'; 
										}
										else { 
										echo $value1['category_title']; 
										}}?>
										</div>
									<?php $i++; }}}  else {echo JText::_('No sub-categories to show!'); } ?>			
					 
									</div>
																
						    </div> <!-- END sub_content -->
						   	 <?php if(($count_items) == $ij){?>
							</div><?php } else {?>
							<?php if($ij%$category_columns ==0) {?> </div><div class="content-box" style="width:<?php echo $width_module;?>px; margin-top: 10px; float:left"> <?php }?>
							<?php }?>    
							   <?php $ij++;?>  
					
				<?php  $j++;}?>
            </div> <!-- END Box Wrapper -->
        </div> <!-- END ytc_tabs -->
       <div style="text-align: left; width:<?php echo $width_module;?>px; overflow: hidden"><?php echo $footer_text; ?></div>
    </div>
 <?php } else { echo JText::_('Has no content to show!');}?>