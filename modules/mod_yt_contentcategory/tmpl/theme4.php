<?php       
 # Yt Content Category  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2011-2012 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://joomla.ytcvn.com
 #-------------------------------------------------------------------------*/
?>
<?php
    defined('_JEXEC') or die('Restricted access');?>
<style>
    #yt_accordion<?php echo $module->id;?> a{
        display: block;
    }
</style>

<script>
//    $jYtc(document).ready(function($){
//    	// Setup HoverAccordion for Example 2 with some custom options
//    	$('#yt_accordion<?php echo $module->id;?>').hoverAccordion({
//    		activateitem: '1',
//    		speed: 'fast',
//			action: '<?php echo $tab_event ;?>'
//    	});
//    	$('#yt_accordion<?php echo $module->id;?>').children('li:first').addClass('firstitem');
//    	$('#yt_accordion<?php echo $module->id;?>').children('li:last').addClass('lastitem');
//    });
$jYtc(document).ready(function($){
	$jYtc('#yt_accordion<?php echo $module->id;?>').hoverAccordion({
	    active: 0,
	    selectedClass: 'opened',
	    alwaysOpen: false,
	    autoheight: false,  
	    header: '.head', 
	    animated: "slide",
	    action: '<?php echo $tab_event ;?>'	    
	});
});
</script>
    <?php if(!empty($items)){?> 
    <div id="page-wrap" class="yt-contentcategory">
     <div style="text-align: left; width:<?php echo $width_module;?>px;"><?php echo $intro_text; ?></div>
        <div id="ytc_tabs<?php echo $module->id;?>" class="theme4" style="overflow: hidden">  
            <div class="box-wrapper">
               			    <div class="normal_category" style="float: left; width:<?php echo $width_article_content;?>px;">
							<ul id="yt_accordion<?php echo $module->id;?>" class="normal_content" style="width:<?php echo $width_article_content;?>px;">
							 <?php $j=0; 
								foreach ($items as $key=>$item){ $firstval = current($item[0]);?>	
								<li class="normal_items" style="width:<?php echo $width_article_content;?>px;">								
								   <a href="javascript:void(0)">   
									<div class="icon_left">&nbsp;&nbsp;&nbsp;</div>
									<div class="title" style="color: <?php echo $title_color;?>;">
									<?php echo strtoupper($firstval['category_title']);?></div>
									<div class="icon_right">&nbsp;&nbsp;&nbsp;</div>
									</a>
								    <?php if (!empty($item[1])){ $count = count($item[1]); 
												foreach ($item[1] as $key1=>$value1) {?>
								    <ul id="<?php echo $module->id.$j;?>" class="normal_content_accor" style="<?php if(($show_small_image ==1) &&($value1['category_thumb'] !='')){?> height:<?php echo $sub_thumb_height;?>px; <?php } else {?> height: 18px; <?php } ?>">								
									<div style="width:<?php echo ($width_article_content -10);?>px; float: left">
									<?php if(($show_small_image ==1) &&($value1['category_thumb'] !='')){?>
									<li style="float: left; margin-right: 10px">
									<?php if($link_sub_image == 1){?>
									<a href="<?php echo $value1['category_link']; ?>" target = "<?php echo $target;?>">
									<img src="<?php echo $value1['category_thumb']?>" title="<?php echo $value1['category_title']?>"/>
									</a>
									<?php } 
									else 
									{?>
									<img src="<?php echo $value1['category_thumb']?>"/>
									<?php } ?>		
								</li>
								<?php }?>
								<li class="normal_desc">										
									<?php if ($show_sub_title == 1){ ?>                                  
                                    <div style="color:<?php echo $sub_title_color;?>;">                                         
                                    <?php if($link_sub_title ==1) {?>        
                                    <a style="font-weight: bold; background: none; color:<?php echo $sub_title_color;?>"; href="<?php echo $value1['category_link'];?>" target = "<?php echo $target;?>"> <?php echo $value1['category_title'];?> </a> <br>                                                                         
                                    <?php } else
                                    {
                                    echo $value1['category_title']; 
                                    }?>                                     
                                    </div>
                                   
								</li>
								<?php if ($show_total_articles ==1) {?>
								<li class="num_items" style="color: #737373;">
								<?php echo '('.$value1['count_articles'].')';?>
								</li>  
								<?php }}?>								
								</div>								
							  </ul>	
							  <?php }}?>
							</li>
							<?php } ?>
						  </ul>
					  </div>	
					
            </div> <!-- END Box Wrapper -->
        </div> <!-- END ytc_tabs -->
      <div style="text-align: left; width:<?php echo $width_module;?>px; overflow: hidden"><?php echo $footer_text; ?></div>
    </div>
 <?php } else { echo JText::_('Has no content to show!');}?>