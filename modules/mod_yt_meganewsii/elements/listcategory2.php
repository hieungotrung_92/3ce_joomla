<?php
/*------------------------------------------------------------------------
 # Yt Mega News II - Version 1.0
 # Copyright (C) 2009-2011 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://www.ytcvn.com
 -------------------------------------------------------------------------*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (class_exists('JFormField')){
	class JFormFieldListCategory2 extends JFormField {
		var	$_name = 'Yt Category Listing';
		function getInput(){
			$db =& JFactory::getDBO();
			$category_query = "
				SELECT c.title, c.id, s.title as section_title, s.id as section_id
				FROM #__categories c
				INNER JOIN #__categories s ON s.id=c.parent_id
				WHERE c.extension='com_content' AND c.level=3 AND c.published=1 AND s.published=1;";
			$db->setQuery($category_query);
			$categories = $db->loadObjectList();
			
			$sections = array();
			$html = "";
			if (isset($categories) && count($categories)>0){
				foreach ($categories as $i => $cat){
					if (!isset($sections[$cat->section_id])){
						$sections[$cat->section_id] = array();
					}
					$sections[$cat->section_id][] =& $categories[$i];
				}
				if (!is_array($this->value)){
					$this->value = array($this->value);
				}
				class_exists('YtUtils') or include_once dirname(dirname(__FILE__)) . DS . 'lib' . DS . 'ytutils.php';
				$html = '<select class="inputbox" multiple="multiple" size="15" style="min-width: 200px;" id="' . $this->id .'" name="' . $this->name . '[]">';
				foreach ($sections as $sec){
					if (count($sec)){
						$html .= '<optgroup label="'. YtUtils::shorten($sec[0]->section_title, 50) . '">';
						foreach ($sec as $cat){
							$cid = $cat->id . '_' . $cat->section_id;
							$selected = in_array($cid, $this->value) ? 'selected="selected"' : '';
							$html .= '<option value="' . $cid . '" ' . $selected . '>' . YtUtils::shorten($cat->title, 45) . '</option>';
						}
						$html .= '</optgroup>';
					}
				}
				$html .= '<select>';
			} else {
				$html = "Have no category. Please create categories first.";
			}
			return $html;
		}
	}
}