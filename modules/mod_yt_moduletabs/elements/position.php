<?php

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );
jimport('joomla.html.html');
jimport('joomla.form.formfield');
class JFormFieldPosition extends JFormField
{
	/**
	 * Element name
	 *
	 * @access	protected
	 * @var		string
	 */
	protected	$type = 'Position'; 

	protected function getInput()
	{	$session = JFactory::getSession();
        $options = array();
        $attr = '';
        // Initialize some field attributes.
        $attr .= $this->element['class'] ? ' class="'.(string) $this->element['class'].'"' : '';
        // To avoid user's confusion, readonly="true" should imply disabled="true".
        if ( (string) $this->element['readonly'] == 'true' || (string) $this->element['disabled'] == 'true') { 
            $attr .= ' disabled="disabled"';
        }
        $attr .= $this->element['size'] ? ' size="'.(int) $this->element['size'].'"' : '';
        $attr .= $this->multiple ? ' multiple="multiple"' : '';
        // Initialize JavaScript field attributes.
        $attr .= $this->element['onchange'] ? ' onchange="'.(string) $this->element['onchange'].'"' : '';
        //now get to the business of finding the articles
		$options = $this->getPositions();

        $arrOpt = array();
        $arrOpt[] = JHTML::_('select.option', '', '-------- None select --------');
        foreach ( $options as $item ){//
            $arrOpt[] = JHTML::_('select.option',  $item->position, $item->position );
        } 
		return JHTML::_('select.genericlist',  $arrOpt, $this->name,trim($attr), 'value', 'text', $this->value, $this->id ); 
		
	}
	
	function getPositions()
	{
		$db =& JFactory::getDBO();
		
		$query = 'SELECT DISTINCT position'
		. ' FROM #__modules AS a'
		. ' WHERE a.published = 1'
		. ' ORDER BY a.position'
		;
		$db->setQuery( $query );
		$db->getQuery();
		$options = $db->loadObjectList();
		
		return $options;
	}
	
}
?>