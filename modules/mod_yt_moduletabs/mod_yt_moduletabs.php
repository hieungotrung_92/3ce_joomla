<?php
/*------------------------------------------------------------------------
 # Ytc K2 Scroller  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://addon.ytcvn.com
 -------------------------------------------------------------------------*/

defined( '_JEXEC' ) or die( 'Restricted access' );

/*-- Process---*/
$position 						= $params->get("position", 0); //print_r($position) ; die();
$height 						= $params->get("height", '300');
if (!defined ('MODULETABS')) {
	define ('MODULETABS', 1);
	/* Add css*/	
	$mainframe = JFactory::getApplication();
	if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."style.css")){
		JHTML::stylesheet("style.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
	}else{
		JHTML::stylesheet('style.css',JURI::base() . 'modules/'.$module->module.'/assets/');
	}
	
	/* add JS files*/
	JHTML::script('script.js',JURI::base() . 'modules/'.$module->module.'/assets/');	
}

$document	= &JFactory::getDocument();
$renderer	= $document->loadRenderer('module'); //var_dump($renderer); die();

$contents = '';

$module1s = JModuleHelper::getModules($position);
//print_r($module1s); die();

//$content = array();
foreach ($module1s as $key =>  $mod)  {
	$module1s[$key]->content = $renderer->render($mod, array());
	//$content[] = $renderer->render($mod, array());
}

/* Show html*/
$path = JModuleHelper::getLayoutPath ( 'mod_yt_moduletabs');
if (file_exists ( $path )) {
	require ($path);
}


?>
