<?php
/*------------------------------------------------------------------------
 # Ytc K2 Scroller  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://addon.ytcvn.com
 -------------------------------------------------------------------------*/
?>

<script language="javascript">
window.addEvent('domready', init);
function init() {
	myTabs1 = new mootabs('ytTabs', {height: '<?php echo $height; ?>px', mouseOverClass: 'over'});
}
</script>
<?php
	if (sizeof ($module1s) > 0) {
?>
<div id="ytTabs" class="module-tabs">

	<ul class="mootabs_title">
		<?php 
			foreach ($module1s as $item) {
		?>
		<li title="module<?php echo $item->id?>"><span><?php echo $item->title?></span></li>		
		<?php } ?>
	</ul>
    
	<?php foreach ($module1s as $item) {?>
    <div id="module<?php echo $item->id?>" class="mootabs_panel">
        <div class="yttabs-inner">
            <?php echo $item->content?>
            </div>
        </div>
    <?php } ?>	
	
</div><!--/widget-->
<?php }  ?>
