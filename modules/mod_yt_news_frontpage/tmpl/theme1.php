<?php
/*------------------------------------------------------------------------

 # Yt News Front Page  - Version 1.0

 # ------------------------------------------------------------------------

 # Copyright (C) 2010 - 2011 The YouTech Company. All Rights Reserved.

 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

 # Author: The YouTech Company

 # Websites: http://joomla.ytcvn.com

 -------------------------------------------------------------------------*/
?>
<?php

    defined('_JEXEC') or die('Restricted access');
?>
<!--Intro Text-->
<?php if($intro): ?>
<div style="text-align:left; width:<?php echo $width_module; ?>px">
	<?php  echo $intro;?>
</div>
<br/>
<?php endif;?>
<!--End Intro Text-->

<?php if(count($items)>0):?>   
<div class="yt_frontpage <?php echo $themes; ?>" style="width:<?php echo $width_module; ?>px;background: #FFFFFF !important;">
    <?php
        $count_items = 0;
        if($count_items == 0){
    ?>
        <div class="main_frontpage">
            <?php if($show_main_image == 1){?>   
            <div class="main_images">
                <a href="<?php echo ($link_main_image)?$items[0]['link']:"#"; ?>" title="<?php echo htmlspecialchars($item[0]['title']);?>" target = "<?php echo $target;?>" style="background: none;"><img src="<?php echo $items[0]['thumb']?>" title="<?php echo $items[0]['title']?>"/></a>
            </div>
            <?php } ?> 
            <div class="main_content" style="width: <?php echo $thumb_width; ?>px;font-size:12px;padding-right:2px;">
                <?php if($show_main_title == 1){?>
                    <a href="<?php echo ($link_main_title)?$items[0]['link']:"#"; ?>" title="<?php echo htmlspecialchars($items[0]['title']);?>" target = "<?php echo $target;?>" style="text-align: left; text-decoration: none;background: none !important;">
                        <h3 style="color: <?php echo $color_main_title;?> !important;font-size:16px"> <?php echo $items[0]['sub_title']?></h3>
                    </a>
                <?php } ?>
                <?php if($show_date == 1){?>
                    <p class="yt_date" style="margin: 0 0 3px 0 !important;line-height: normal !important;"><?php echo date("d F Y", strtotime($items[0]['publish'])); ?></p>
                <?php } ?>
                <?php if($show_description == 1){?>
                    <?php echo $items[0]['sub_main_content']?>
                <?php } ?>
                <?php if($show_readmore == 1){?>
                <p class="redmore_main_frontpage"><a href="<?php echo $items[0]['link']; ?>" title="<?php echo htmlspecialchars($items[0]['title']);?>" target = "<?php echo $target;?>" style="background: none !important;text-decoration: none;color:#135CAE !important"><b><?php echo $readmore_text;?></b></a></p>
                <?php } ?>
            </div> 
        </div>
    <?php 
        $count_items = 1;
        }
     ?>
    <div class="nomal_frontpage">
        <div class="nomal_items" style="width:<?php echo $width_content; ?>px;">
            <?php 
                foreach($items as $key=>$item) { 
                    if($key != 0){
            ?>
            <div style="padding-bottom: 10px; float: left;width:<?php echo $width_content; ?>px;">
                <?php if($show_normal_image == 1):?>
                <div class="nomal_images" style="float: right;" title="<?php echo htmlspecialchars($item['title']);?>">
                 <a href="<?php echo ($link_normal_image)?$item['link']:"#"; ?>" target = "<?php echo $target;?>" title="<?php echo htmlspecialchars($item['title']);?>" style="background: none;"><img src="<?php echo $item['small_thumb']?>" title="<?php echo htmlspecialchars($item['title']);?>" border="none"/></a>
                </div>
                <?php endif;?>
                <div class="nomal_content" >
                    <?php if($show_normal_title == 1){?>
                        <a href="<?php echo ($link_normal_title)?$item['link']:"#"; ?>" title="<?php echo htmlspecialchars($item['title']);?>"  target = "<?php echo $target;?>" style="background: none  !important; text-decoration: none;right:0px;">
                            <strong style="color: <?php echo $color_normal_title;?>;font-size:13px;" title="<?php echo htmlspecialchars($item['title']);?>" ><?php echo $item['sub_title']?></strong><br />
                        </a>
                    <?php } ?>
                    <?php if($show_date == 1){?>
                        <span class="yt_date" style="padding-top: 5px !important;"><?php echo date("d F Y", strtotime($item['publish'])); ?></span>
                    <?php } ?>
                    <?php if($show_normal_description == 1){?>
                    <p style="font-size:12px;padding:0px !important"><?php echo $item['sub_normal_content']?></p>
                    <?php } ?>
                </div>
            </div>
            <?php }} ?>
        </div>
        
    </div>
</div>
<?php else: echo JText::_('Has no content to show!');?>
    <?php endif;?>
<?php if($note): ?>
<br/>
<div style="text-align:left; width:<?php echo $width_module; ?>px">
	<p><?php  echo $note;?></p>
</div>
<?php endif;?>