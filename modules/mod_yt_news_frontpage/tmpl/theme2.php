<?php
/*------------------------------------------------------------------------

 # Yt News FrontPage  - Version 1.0

 # ------------------------------------------------------------------------

 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.

 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

 # Author: The YouTech Company

 # Websites: http://joomla.ytcvn.com

 -------------------------------------------------------------------------*/
?>
<?php

    defined('_JEXEC') or die('Restricted access');
?>
<!--Intro Text-->
<?php if($intro): ?>
<div style="text-align:left; width:<?php echo $width_module; ?>px">
	<?php  echo $intro;?>
</div>
<br/>
<?php endif;?>
<!--End Intro Text-->

<?php if(count($items)>0):?>    
<div class="yt_frontpage <?php echo $themes; ?>" style="width:<?php echo $width_module; ?>px;">
    <div class="normal_yt">
        <?php
            $count_items = 0;   
            if($count_items == 0){
        ?>
        <div class="main_frontpage" style="height: <?php echo $thumb_height ?>px;">   
            <?php if($show_main_image == 1){?>
            <div class="main_images">
                <a href="<?php echo ($link_main_image)?$items[0]['link']:"#"; ?>" target = "<?php echo $target;?>" style="background: none;"><img src="<?php echo $items[0]['thumb']?>" title="<?php echo $items[0]['title']?>"/></a>
            </div>
            <?php } ?> 
        </div>
        <?php 
            $count_items = 1;
            }
         ?>
        <div class="normal_frontpage_theme2" style="width:<?php echo $width_content;?>px; float: left;">
            <ul class="normal_items_theme2" style="text-align: left !important;position: relative !important;">
            <?php 
				
				
                foreach($items as $key=>$item) { 
                    if($key != 0){
            ?>
                <li>
                    <?php if($show_normal_title == 1){?>
                    <div class="normal_title_theme2" style="left: 0px;">
                        <a style="color: <?php echo $color_normal_title;?>;font-size:13px;background: none  !important; text-decoration: none;" href="<?php echo ($link_normal_title)?$item['link']:"#"; ?>" title="<?php echo htmlspecialchars($item['title']);?>" target = "<?php echo $target;?>"><?php echo $item['sub_title']?></a>
                    </div>
                    <?php } ?> 
                    <?php if($show_date == 1){?>
                        <div class="yt_date">
                            <span style="color: #B7B7B7 !important;"><?php echo date("d F Y", strtotime($item['publish'])); ?></span>
                        </div>
                    <?php } ?>
                    <?php if($show_normal_description == 1){?>
                    <div class="normal_desc_theme2" style="font-size: 12px;"><?php  echo $item['sub_normal_content']?></div>
                    <?php } ?>
                </li>
             <?php }} ?>   
            </ul>
        </div>
    </div>

    <div class="main_content_theme2">
        <?php if($show_main_title == 1){?>
            <a href="<?php echo ($link_main_title)?$items[0]['link']:"#"; ?>" target = "<?php echo $target;?>" style="text-align: left; text-decoration: none;background: none;">
                <h3 style="color: <?php echo $color_main_title;?>;font-size: 16px;" title="<?php echo $items[0]['title']?>"><?php echo $items[0]['sub_title']?></h3>
            </a>
        <?php } ?>
            <?php if($show_date == 1){?>
                <p class="yt_date"><?php echo date("d F Y", strtotime($items[0]['publish'])); ?></p>
             <?php } ?>
            <?php if($show_description == 1){?>            
                <span  style="font-size: 12px;"><?php echo $items[0]['sub_main_content']?></span><br />
            <?php } ?>                
            <?php if($show_readmore == 1){?>
            <p class="redmore_main_frontpage"><a href="<?php echo $items[0]['link']; ?>" title="<?php echo $items[0]['title']?>" target = "<?php echo $target;?>" style="font-size: 12px !important;background: none !important;text-decoration: none;color:#135CAE !important"><b><?php echo $readmore_text;?></b></a></p>
            <?php } ?>
    </div>

</div>
<?php else: echo JText::_('Has no content to show!');?>
    <?php endif;?>

<?php if($note): ?>
<br/>
<div style="text-align:left; width:<?php echo $width_module; ?>px">
	<p><?php  echo $note;?></p>
</div>
<?php endif;?>