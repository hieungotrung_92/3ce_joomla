<?php
/*------------------------------------------------------------------------

 # Yt News FrontPage  - Version 1.0

 # ------------------------------------------------------------------------

 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.

 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

 # Author: The YouTech Company

 # Websites: http://joomla.ytcvn.com

 -------------------------------------------------------------------------*/
?>
<?php

    defined('_JEXEC') or die('Restricted access');

?>
<!--Intro Text-->
<?php if($intro): ?>
<div style="text-align:left; width:<?php echo $width_module; ?>px">
	<?php  echo $intro;?>
</div>
<br/>
<?php endif;?>
<!--End Intro Text-->

<?php if(count($items)>0):?>    
<div class="yt_frontpage yt_frontpage_theme3" style="width:<?php echo $width_module; ?>px;padding:8px;">
    <?php
        $count_items = 0;
        foreach($items as $key=>$item) {      
        if($count_items == 0){
    ?>
    <div class="ytc main_frontpage_theme3" style="width:<?php echo $width_module; ?>px; height:<?php echo $thumb_height;?>px;">
        <?php if($show_main_image == 1):?>
        <div class="main_img_theme3" style="float: left; position: relative;">
             <a href="<?php echo ($link_main_image)?$items[0]['link']:"#"; ?>" target = "<?php echo $target;?>" title="<?php echo htmlspecialchars($item[0]['title']);?>" style="background: none;"><img src="<?php echo $items[0]['thumb']?>" title="<?php echo htmlspecialchars($items[0]['title']);?>"/></a>
        </div>
        <?php endif;?>
        <div class="main_content_theme3" style="position: relative; overflow: hidden;padding-left: 10px;">
            <?php if($show_main_title == 1){?>
                <a href="<?php echo ($link_main_title)?$items[0]['link']:"#"; ?>" target = "<?php echo $target;?>" style="text-align: left; text-decoration: none;font-size: 16px !important;background: none;">
                    <h3 style="color: <?php echo $color_main_title;?>;" title="<?php echo htmlspecialchars($items[0]['title']);?>"><?php echo $items[0]['sub_title']?></h3>
                </a>
            <?php } ?> 
             <?php if($show_date == 1){?>
                <span class="yt_date"><?php echo date("d F Y", strtotime($items[0]['publish'])); ?></span>
             <?php } ?>
            <?php if($show_description == 1){?>
                <p style="font-size: 12px !important; display: block !important;margin:0px;padding:0px;padding-top:5px;"><?php echo $items[0]['sub_main_content']?></p>
            <?php } ?>
            <?php if($show_readmore == 1){?>
            <span><a href="<?php echo $items[0]['link']; ?>" title="<?php echo htmlspecialchars($items[0]['title']);?>" target = "<?php echo $target;?>" style="font-size: 12px !important; background: none !important; text-decoration: none;"><b><?php echo $readmore_text;?></b></a></span>
            <?php } ?>
        </div>
    </div>
    
    <div  style="position: relative;float: left !important;">
    <?php 
        $count_items = 1;
        }elseif($count_items == 1){
     ?>
        <div class="nomal_frontpage_theme3"  style="float: left;width:<?php echo $widthpage_theme3;?>px;">
            <div class="nomal_content_theme3">
                <?php if($show_normal_image == 1):?>
               <div style="float: left;">
                    <a href="<?php echo ($link_normal_image)?$item['link']:"#"; ?>" target = "<?php echo $target;?>" style="background: none !important;"><img src="<?php echo $item['small_thumb']?>" title="<?php echo $item['title']?>" border="0px"/> </a>
                </div>
                <?php endif;?>
                <?php if($show_normal_title == 1){?>
                <div class="normal_des" >
                    <a href="<?php echo ($link_normal_title)?$item['link']:"#"; ?>" target = "<?php echo $target;?>" style="text-decoration: none;background: none !important;font-size: 13px !important;">
                        <span style="color: <?php echo $color_normal_title;?>;" title="<?php echo htmlspecialchars($item['title']);?>"><?php echo $item['sub_title']?></span>
                    </a>
                </div>
                <?php } ?> 
                <?php if($show_date == 1){?>
                    <div class="yt_date">
                        <span style="padding-left: 10px;color:#B7B7B7 !important" ><?php echo date("d F Y", strtotime($item['publish'])); ?></span>
                    </div>
                <?php } ?>
                <?php if($show_normal_description == 1){?>
                    <div class="normal_des_theme3" style="font-size:12px !important; position: relative; overflow: hidden; padding: 0px 5px 0px 9px;"><?php echo $item['sub_normal_content']?></div>
                <?php } ?>
            </div>
        </div>
    <?php }} ?>
    </div>
</div>
<?php else: echo JText::_('Has no content to show!');?>
    <?php endif;?>
<?php if($note): ?>
<br/>
<div style="text-align:left; width:<?php echo $width_module; ?>px">
	<p><?php  echo $note;?></p>
</div>
<?php endif;?>