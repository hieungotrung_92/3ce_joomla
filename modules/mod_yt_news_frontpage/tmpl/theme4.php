<?php

/*------------------------------------------------------------------------



 # Yt News FrontPage  - Version 1.0



 # ------------------------------------------------------------------------



 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.



 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL



 # Author: The YouTech Company



 # Websites: http://addon.ytcvn.com



 -------------------------------------------------------------------------*/

?>

<?php



    defined('_JEXEC') or die('Restricted access');

?>

<!--Intro Text-->

<?php if($intro): ?>

<div style="text-align:left; width:<?php echo $width_module; ?>px">

	<?php  echo $intro;?>

</div>

<br/>

<?php endif;?>

<!--End Intro Text-->


<?php if(count($items)>0):?> 

<style>

#yt_accordion<?php echo $module->id;?> a{

    display:block;

    font-weight:normal;

    text-decoration:none;

}

</style>

<script>

    jQuery.noConflict();

    jQuery(document).ready(function(){

    	// Setup HoverAccordion for Example 2 with some custom options

    	jQuery('#yt_accordion<?php echo $module->id;?>').hoverAccordion({

    		activateitem: '1',

    		speed: 'fast'

    	});

    	jQuery('#yt_accordion<?php echo $module->id;?>').children('li:first').addClass('firstitem');

    	jQuery('#yt_accordion<?php echo $module->id;?>').children('li:last').addClass('lastitem');

    });

</script>

   

<div class="yt_frontpage <?php echo $themes; ?>" style="width:<?php echo $width_module; ?>px;">

    <?php

        $count_items = 0;     

        if($count_items == 0){

    ?>

    <div class="main_frontpage" style="float: left;">   

        <?php if($show_main_image==1):?>

        <div class="main_images">

         <a href="<?php echo ($link_main_image)?$items[0]['link']:"#"; ?>" title="<?php echo htmlspecialchars($items[0]['title']);?>"  target = "<?php echo $target;?>" style="border: none;background: none !important;"><img src="<?php echo $items[0]['thumb']?>" title="<?php echo htmlspecialchars($items[0]['title']);?>"/></a>

        </div>

        <?php endif;?> 

        <div class="main_content" style="width: <?php echo $thumb_width;?>px;padding-right:2px;">

            <?php if($show_main_title == 1){?>

                <a href="<?php echo ($link_main_title)?$items[0]['link']:"#"; ?>" target = "<?php echo $target;?>" style="font-size:16px !important;background: none !important; text-decoration: none;">

                    <h3 style="color: <?php echo $color_main_title;?>;" title="<?php echo $items[0]['title']?>"><?php echo $items[0]['sub_title']?></h3>

                </a>

            <?php } ?>

            <?php if($show_date == 1){?>

                <p style="color:#B7B7B7; font-size:11px; text-align: left;margin:0 !important;display: block !important;"><?php echo date("d F Y", strtotime($items[0]['publish'])); ?></p>

            <?php } ?>

            <?php if($show_description == 1){?>

                <span style="font-size:12px !important;">

                    <?php echo $items[0]['sub_main_content']?>

                </span>

            <?php } ?>

            <p><a href="<?php echo $items[0]['link']; ?>" title="<?php echo $items[0]['title']?>" target = "<?php echo $target;?>" style="font-size:12px !important;background: none !important; text-decoration: none;color:#095197 !important;display: block !important;text-align:right;"><b><?php echo $readmore_text;?></b></a></p>

        </div> 

    </div>

    <?php 

        $count_items = 1;

        }

     ?>

    <div class="normal_frontpage_theme4" style="float: left;">

      <ul id="yt_accordion<?php echo $module->id;?>" class="normal_content_theme4" style="width:<?php echo $width_content;?>px;float:right;right: auto;">

        <?php 

            foreach($items as $key=>$item) { 

                if($key != 0){

        ?>

        <li class="normal_items_theme4">

            <?php if($show_normal_title == 1){?>

                <a href="<?php echo ($link_normal_title)?$item['link']:"#"; ?>" target = "<?php echo $target;?>" style="text-shadow:none !important;background: none !important;text-decoration:none;font-size:13px !important;text-transform: none !important;">  

                    <strong style="color: <?php echo $color_normal_title;?> !important;" title="<?php echo htmlspecialchars($item['title']);?>"><?php echo $item['sub_title']?></strong>

                </a>

            <?php } ?> 

              <ul class="normal_content_accor" style="text-align: left !important;position: relative;">

                    <?php if($show_main_image==1):?>

                    <li style="float: right; margin-left: 3px;">

                        <a href="<?php echo ($link_normal_image)?$item['link']:"#"; ?>" target = "<?php echo $target;?>" style="background: none !important; text-transform: none;"><img src="<?php echo $item['small_thumb']?>" title="<?php echo htmlspecialchars($item['title']);?>"/></a>

                    </li>

                    <?php endif;?>

                    <li class="normal_desc_theme4">

                    <?php if($show_date == 1){?>

                            <span><?php echo date("d F Y", strtotime($item['publish'])); ?></span>

                    <?php } ?>

                    <?php if($show_normal_description == 1){?>

                        <p style="margin: 0 !important;font-size:12px !important;padding: 0 !important;display:block !important;"><?php echo htmlspecialchars($item['sub_normal_content']);?></p>

                    <?php } ?> 

                    </li>

              </ul>

        </li>

        <?php }} ?>

      </ul>

  </div>

</div>

<?php else: echo JText::_('Has no content to show!');?>

    <?php endif;?>

<?php if($note): ?>

<br/>

<div style="text-align:left; width:<?php echo $width_module; ?>px">

	<p><?php  echo $note;?></p>

</div>

<?php endif;?>