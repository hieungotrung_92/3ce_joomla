<?php
/*------------------------------------------------------------------------
 # Yt Tittle flash  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://addon.ytcvn.com
 -------------------------------------------------------------------------*/
defined('_JEXEC') or die('Restricted access');

$user 		= &JFactory::getUser();
$db 		= &JFactory::getDBO();
$menu 		= &JSite::getMenu();
$document	= &JFactory::getDocument();

//..Adding Css to Header
$mainframe = JFactory::getApplication();
	
if(is_file(JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$module->module.DS."yt.titleflash.css")){
	JHTML::stylesheet("yt.titleflash.css", 'templates/'.$mainframe->getTemplate().'/html/'.$module->module.'/');
}else{
	JHTML::stylesheet('yt.titleflash.css', JURI::base() . '/modules/'.$module->module.'/assets/');
}	


JHTML::_('script','yt.titleflash.js','modules/mod_yt_titleflash/assets/js/');


//... These params I get from the author of Phoca Gallery, no need in this module. Leave them as default ...........
$moduleclass_sfx = $params->get( 'moduleclass_sfx', 0 );

require_once (JPATH_SITE.DS.'components'.DS.'com_content'.DS.'helpers'.DS.'route.php');

class mod_YT_TitleFlashHelper
{
	function getList(&$params)
	{
		global $mainframe;

		$db			=& JFactory::getDBO();
		$user		=& JFactory::getUser();
		$userId		= (int) $user->get('id');
		
		$contentConfig = &JComponentHelper::getParams ( 'com_content' );
		$noauth = ! $contentConfig->get ( 'shownoauth' );
		
		jimport ( 'joomla.utilities.date' );
		$date = new JDate ( );
		$now = $date->toMySQL ();
		
		$nullDate = $db->getNullDate ();
			
		$intro_lenght = intval($params->get( 'intro_length', 200) );
		$count		= (int) $params->get('count', 10);
		//$catid		= trim( $params->get('catid') );
		//$secid		= trim( $params->get('secid') );
		$show_front	= $params->get('show_front', 1);
		// Ordering
		$ordering		= 'a.id DESC';
		$aid		= $user->get('aid', 1);
		$aid = 1;
		$categories = $params->get('categories',array());
		
			
		
		if ($ordering == 'random') {				
			$orderby =  ' ORDER BY rand()';			
		} 
		else 
		{				
			$orderby = ' ORDER BY ' . $ordering . '';			
		}
		$limit = " LIMIT {$count}";
		
		if (is_array($categories)){
			$ary = " in (" . implode("," , $categories) . ")";   
		} else { 
			$ary = " = ". ( int ) $categories;     
		}
		// query to determine article count
		$query = 'SELECT a.*,u.name as creater,cc.description as catdesc, cc.title as cattitle,' . ' CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(":", a.id, a.alias) ELSE a.id END as slug,' . ' CASE WHEN CHAR_LENGTH(cc.alias) THEN CONCAT_WS(":", cc.id, cc.alias) ELSE cc.id END as catslug FROM #__content AS a' . ' INNER JOIN #__categories AS cc ON cc.id = a.catid' . ' left JOIN #__users AS u ON a.created_by = u.id';          
		
		 $query .= ' WHERE a.state = 1 '. ($noauth ? ' AND a.access <= ' . ( int ) $aid . ' AND cc.access <= ' . ( int ) $aid : '') . ' AND (a.publish_up = ' . $db->Quote ( $nullDate ) . ' OR a.publish_up <= ' . $db->Quote ( $now ) . ' ) ' . ' AND cc.id '. $ary . ' AND (a.publish_down = ' . $db->Quote ( $nullDate ) . ' OR a.publish_down >= ' . $db->Quote ( $now ) . ' )' . ' AND cc.published = 1';
		
		if ($show_front == 0) {
			$query .= ' AND a.id not in (SELECT content_id FROM #__content_frontpage )';
		} else if ($show_front == 1) {
			$query .= ' AND a.id in (SELECT content_id FROM #__content_frontpage )';
		}

		$query .= $orderby . $limit;  
		//echo $query; die();
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$i		= 0;
		$lists	= array();
		$article_count = count($rows);
		
		foreach ( $rows as $row )
		{
			$lists[$i]->title = mod_YT_TitleFlashHelper::wordLimit($row->title,$intro_lenght,'...');		
			$lists[$i]->alias = $row->alias;
			//$lists[$i]->cat_title = $row->cat_title;
			//$lists[$i]->stitle = $row->stitle;
			$lists[$i]->created = $row->created;
			$lists[$i]->link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug, $row->sectionid));
			$i++;
		}
		return $lists;
	}
	function wordLimit($str, $limit = 100, $end_char = '&#8230;') {				
		if (trim($str) == '') return $str;
		// always strip tags for text
		$str = strip_tags($str);
		preg_match('/\s*(?:\S*\s*){'.(int)$limit.'}/', $str, $matches);		
		if (strlen($matches[0]) == strlen($str))$end_char = '';
		return rtrim($matches[0]).$end_char;
	}
}
?>