<?php
/*------------------------------------------------------------------------
 # Yt Tittle flash  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://addon.ytcvn.com
 -------------------------------------------------------------------------*/

defined('_JEXEC') or die('Restricted access');// no direct access
require_once (dirname(__FILE__).DS.'helper.php');

$list = mod_YT_TitleFlashHelper::getList($params);

require(JModuleHelper::getLayoutPath('mod_yt_titleflash'));
?>