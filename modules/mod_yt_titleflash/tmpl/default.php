<?php 
/*------------------------------------------------------------------------
 # Yt Tittle flash  - Version 1.0
 # ------------------------------------------------------------------------
 # Copyright (C) 2009-2010 The YouTech Company. All Rights Reserved.
 # @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 # Author: The YouTech Company
 # Websites: http://addon.ytcvn.com
 -------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access'); ?>

<div class="yt-yttitleflash">
	<?php if($params->get('showtitle') == 1 ) : ?><div id="breakingnews-title"><span class="title"><?php echo $params->get('title'); ?>:</span></div><?php endif; ?>
	<div id="yttitleflash<?php echo $module->id; ?>" class="sliderwrapper">
		<?php foreach($list as $itemSubject) : ?>
		<div class="contentdiv">
			 <a href="<?php echo $itemSubject->link; ?>" class="toc"><span><?php echo $itemSubject->title; ?></span></a>
             <?php if($params->get('showdate') == 1): ?>
                 <span class="yt-titleflash-time"> <?php if($params->get('style_display')==1 || $params->get('style_display') == 3 ) : ?>- <?php else : ?>(<?php endif; ?><?php echo JHTML::_('date', $itemSubject->created, JText::_('DATE_FORMAT_LC2')) ?><?php if($params->get('style_display')==2 || $params->get('style_display') == 4 ) : ?>)<?php endif; ?>
                 </span>
             <?php endif; ?>
		</div>
		<?php endforeach; ?>
	</div>
    
    <div id="paginate-yttitleflash<?php echo $module->id; ?>" class="yt-titleflash-pagination">
    	<?php if($params->get('showbutton') == 1) : ?>
		<ul>        
			<li><a href="#" class="prev"></a></li>
			<li><a href="#" class="next"></a></li>           
		</ul>
        <?php endif; ?>
	</div>
</div>
<script type="text/javascript">
<!--
/* <![CDATA[ */
	window.addEvent('domready',function(){
		featuredcontentslider.init({
			id: "yttitleflash<?php echo $module->id; ?>",  //id of main slider DIV
			contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]
			toc: "markup",  //Valid values: "#increment", "markup", ["label1", "label2", etc]
			nextprev: ["Previous", "Next"],  //labels for "prev" and "next" links. Set to "" to hide.
			revealtype: "click", //Behavior of pagination links to reveal the slides: "click" or "mouseover"
			enablefade: [true, 0.1],  //[true/false, fadedegree]
			autorotate: [true, 5000],  //[true/false, pausetime]
			onChange: function(previndex, curindex){  //event handler fired whenever script changes slide
				//previndex holds index of last slide viewed b4 current (1=1st slide, 2nd=2nd etc)
				//curindex holds index of currently shown slide (1=1st slide, 2nd=2nd etc)
			}
		});
	});
/* ]]> */
-->
</script>