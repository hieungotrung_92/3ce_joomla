<?php
// Set flag that this is a parent file
define( '_JEXEC', 1 );
define( 'DS', DIRECTORY_SEPARATOR );
define('JPATH_BASE', dirname(__FILE__).DS.'..'.DS.'..'.DS.'..' );

require_once ( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once ( JPATH_BASE .DS.'includes'.DS.'framework.php' );

		
JDEBUG ? $_PROFILER->mark( 'afterLoad' ) : null;

 // CREATE THE APPLICATION
$app = JRequest::getString('app', 'site');

$app = $app == 'site' ? 'site' : ($app == 'administrator' ? 'administrator' : 'site');
$app_path = $app == 'site' ? '../../..' : '../../../administrator';

$mainframe =& JFactory::getApplication($app);
$doc =& JFactory::getDocument();

/** @var jlanguage $lang */
$lang =& JFactory::getLanguage();
$lang->load('plg_editors-xtd_googledocs', realpath(JPATH_BASE.DS.'administrator'));

jimport( 'joomla.filesystem.path' ) ;
jimport( 'joomla.html.parameter' ) ;

jimport( 'joomla.plugin.plugin' ) ;
jimport( 'joomla.plugin.helper' ) ;
$plugin = JPluginHelper::getPlugin( 'content', 'googledocs' ) ;
if( empty( $plugin ->params )) {$plugin= new stdClass; $plugin->params = ''; }
$pluginParams = new JParameter( $plugin->params ) ;

$btnPlugin = JPluginHelper::getPlugin( 'editors-xtd', 'googledocs' ) ;

if( empty( $btnPlugin ->params )) {
	$btnPlugin= new stdClass; $btnPlugin->params = '';
}
$btnPluginParams = new JParameter( $btnPlugin->params ) ;
$extensionsTable = jtable::getInstance('extension');
$gDocsBtnExtensionId = $extensionsTable->find(array('folder' => 'editors-xtd', 'element' => 'googledocs'));

$doctypes[] =  JHTML::_('select.option',  'document' , JText::_( 'Doc' ));
$doctypes[] =  JHTML::_('select.option',  'spreadsheet', JText::_( 'Spreadsheet' ));
$doctypes[] =  JHTML::_('select.option',  'presentation', JText::_( 'Presentation' ));
$doctypes[] =  JHTML::_('select.option',  'pdf', JText::_( 'PDF' ));
$doctypes[] =  JHTML::_('select.option',  'drawing' , JText::_( 'Drawing' ));
$doctypes[] =  JHTML::_('select.option',  'form' , JText::_( 'Form' ));
$doctypelist = JHTML::_('select.genericlist', $doctypes, 'doctype', 'onchange="doGDocRequest()"', 'value', 'text', $pluginParams->get('default_type') );
$doctypelist = str_replace( 'value="document"', 'style="background: url(icon_10_document_list.png) no-repeat;height: 17px;padding-left:20px;" value="document"', $doctypelist );
$doctypelist = str_replace( 'value="presentation"', 'style="background: url(icon_10_presentation_list.png) no-repeat;height: 17px;padding-left:20px;" value="presentation"', $doctypelist );
$doctypelist = str_replace( 'value="spreadsheet"', 'style="background: url(icon_10_spreadsheet_list.png) no-repeat;height: 17px;padding-left:20px;" value="spreadsheet"', $doctypelist );
$doctypelist = str_replace( 'value="drawing"', 'style="background: url(icon_10_drawing_list.png) no-repeat;height: 17px;padding-left:18px;" value="drawing"', $doctypelist );
$doctypelist = str_replace( 'value="pdf"', 'style="background: url(icon_10_pdf_list.png) no-repeat;height: 17px;padding-left:18px;" value="pdf"', $doctypelist );
$doctypelist = str_replace( 'value="form"', 'style="background: url(icon_10_form_list.png) no-repeat;height: 17px;padding-left:18px;" value="form"', $doctypelist );
$sizes[] =  JHTML::_('select.option',  's', JText::_( 'small' ));
$sizes[] =  JHTML::_('select.option',  'm', JText::_( 'medium' ));
$sizes[] =  JHTML::_('select.option',  'l' , JText::_( 'large' ));
$sizelist = JHTML::_('select.genericlist', $sizes, 'size', null, 'value', 'text', $pluginParams->get('default_size') );

$frameborderlist = JHTML::_('select.integerlist', 0, 4, 1, 'frameborder', array('id' => 'frameborder'), $pluginParams->get('frameborder'));
$juri = JURI::getInstance();
 ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="<?php echo $app_path ?>/templates/<?php echo $mainframe->getTemplate() ?>/css/template.css" />
<title>GoogleDrive Embed Plugin Helper</title>
	<script type="text/javascript" src="../../../media/system/js/mootools-core.js"></script>
	<script type="text/javascript" src="../../../media/system/js/mootools-more.js"></script>
	<script type="text/javascript">
	// Load the Google data JavaScript client library
    // Set init() to be called after JavaScript libraries load

    var clientId = '<?php echo $btnPluginParams->get('google_client_id') ?>';
    var apiKey = '<?php echo $btnPluginParams->get('google_api_key') ?>';
    var scopes = 'https://docs.google.com/feeds/ https://docs.googleusercontent.com/ https://spreadsheets.google.com/feeds/';
	      
  	// Use a button to handle authentication the first time.
	function handleClientLoad() {
    	gapi.client.setApiKey(apiKey);
    	if( clientId == '753761437253.apps.googleusercontent.com' && '<?php echo $_SERVER['HTTP_HOST'] ?>' != 'localhost') {
    		window.parent.location.href='<?php echo $app_path ?>/index.php?option=com_plugins&task=plugin.edit&extension_id=<?php echo $gDocsBtnExtensionId ?>'
    		/*$("loadingContainer").setStyle( "display", "none" );
    		$('windowContent').set({
	     	      'display': 'none'
	     	 	});
     	 	
    		authenticationLink.set( { title: "ENTER_GOOGLE_CLIENT_ID", 
				html: "<?php echo JText::_('ENTER_GOOGLE_CLIENT_ID', true) ?>",
				onclick: redirect
			} );*/
    	} else {
        	window.setTimeout(checkAuth,1);
    	}
  	}

  	function checkAuth() {
    	gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true}, handleAuthResult);
  	}

  	function handleAuthResult(authResult) {
	  	$("loadingContainer").setStyle( "display", "none" );
      	var authenticationLink = $(gDocListAPI.AUTHENTICATION_LINK);
      	$("authenticationDiv").setStyle( "display", "" );
    	if (authResult && !authResult.error) {
    		$("document_entries_row").setStyle( "display", "");
    		$("authenticationDiv").morph( {
        		"text-align": "right",
        		"font-size": "0.8em"
    		});
    		$('windowContent').morph({
     	      	'opacity': '1.0'
     	 	});
			doGDocRequest();
      		authenticationLink.set( { title: "<?php echo JText::_('SIGN_OUT', true) ?>",
									html: "<?php echo JText::_('SIGN_OUT', true) ?>" } );
      		authenticationLink.onclick = handleLogoutClick;
		
    	} else {
        	
	      	$("document_entries_row").setStyle( "display", "none" );
	      	$("authenticationDiv").setStyle( "text-align", "center");
	    	$("authenticationDiv").setStyle( "font-size", "3.0em" );
	    	$('windowContent').morph({
	     	     'opacity': '0.3'
	     	});
	      	$(gDocListAPI.STATUS_CONTAINER).setStyle("display", "none");
	      	authenticationLink.set( { title: "<?php echo JText::_('SIGN_IN', true) ?>", 
									html: "<?php echo JText::_('SIGN_IN', true) ?>" } )
			authenticationLink.onclick = handleAuthClick;
        	
    	}
  	}

  	function handleAuthClick(event) {
    	gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, handleAuthResult);
    	return false;
  	}
  	
  	function handleLogoutClick(event) {
	  	token = gapi.auth.getToken();
    	req = new Request.JSONP({ url: 'https://accounts.google.com/o/oauth2/revoke?token=' + token.access_token, method: 'get' });
    	req.send();
    	handleAuthResult();
    	return false;
  	}
     
      // Object for storing constants and global variables
	var gDocListAPI = {
        CURRENT_DOCUMENT: null,
        SELECTED_DOCTYPE: null,
        JSON_RESPONSE: null,
        AUTHENTICATION_LINK: 'authenticationLink',
        MAIN_CONTENT_TABLE: 'mainContentTable',
        STATUS_CONTAINER: 'statusContainer',
        DOCTYPE_SELECT: 'doctype',
        DOCUMENT_SELECT: 'document_entries'
    };

    /**
     * Onload handler: calls two initialization subroutines
    */
    function init() {
        initializeUI();
    } 
    
	/**
	* Function to retrieve the document feed from Google
	* Automatically filters by selected Document Type
	*/
    function doGDocRequest() {
    	
    	$(gDocListAPI.DOCUMENT_SELECT).setStyle("display", "none");
    	$(gDocListAPI.STATUS_CONTAINER).setStyle("display", "");
    	if( gDocListAPI.JSON_RESPONSE != null ) {
    		getDocFeedHandler( gDocListAPI.JSON_RESPONSE );
    	} else {
        	
			args = { 
				path: '/drive/v2/files',
				callback: getDocFeedHandler
			}
			gapi.client.request( args );
    	}
	}

    /**
    * Called after successful retrieval of document feed; populates drop-down
    * menu with titles of user's documents
    *
    * @param {Object} docFeedRoot Feed object containing collection of 
    * 							document entries
	*/
	function getDocFeedHandler(json, req) {
		if(gDocListAPI.JSON_RESPONSE == null) gDocListAPI.JSON_RESPONSE = json;
      	$(gDocListAPI.STATUS_CONTAINER).setStyle("display", "none");
      	$(gDocListAPI.DOCUMENT_SELECT).setStyle("display", "block");
      	type = $(gDocListAPI.DOCTYPE_SELECT).getSelected()[0].value;
        var documentSelect = $(gDocListAPI.DOCUMENT_SELECT);
        documentSelect.getChildren().each( function(el) { try{el.destroy() } catch(e) {} } );
        var i = 0;
        json.items.each( function( item) {
        		
		    if( item.mimeType.contains(type) ) {
			  	embedValue = (typeof item.embedLink == 'undefined' ? item.alternateLink.replace(/edit/, 'preview') : item.embedLink)
				var newOption = new Element("option", {
													value: embedValue
				});
		        newOption.innerHTML = item.title;
		        newOption.inject( documentSelect );
		        i++;
		    }
       		
       	});
       	if( i == 0 ) {
       		var newOption = new Element("option", { value: '' });
			newOption.innerHTML = ' ./. ';
			newOption.inject( documentSelect );
			$('insertBtn').setProperty('disabled', 'true');
       	} else {
       		$('insertBtn').setProperty('disabled', '');
       	}
       	documentSelect.onchange();
    }
    /**
     * Called if Google Docs service is unable to retrieve a feed or insert an
     * entry properly; creates a popup alert notifying user of error of cause
     * 
     * @param {Object} e Object containing error information
     */
    function errorHandler(e) {
        alert(e.cause.status ? e.message + " (" + e.cause.status + ")" : e.message);
    }
	function getDocId( link ) {
		return link;
	}
	function setDocId( id ) {
		$("docid").value = id;
	}
	function insertGoogleDoc(editor) {
		// Get the pagebreak title
		var docid = $("docid").get("value");
		if( docid == "" ) {
			alert( "<?php echo JText::_( 'Document_ID_is_required', true ); ?>" );
			$("docid").focus();
			return false;
		}
		var type = $("doctype").getSelected()[0].value;
		if( type == "document" ) type = "doc";
		var size = $("size").getSelected()[0].value;
		var frameborder = $("frameborder").getSelected()[0].value;
		var width = $("width").get("value");
		var height = $("height").get("value");
		
		if (width != '') {
			width = " width=\""+width+"\" ";
		}
		if (height != '') {
			height = " height=\""+height+"\" ";
		}
		
		var tag = " {GoogleDoc docid=\""+docid+"\" type=\"" + type + "\"  size=\"" + size + "\" frameborder=\"" + frameborder + "\""+ width + height + "} ";
	
		window.parent.jInsertEditorText(tag, "<?php echo preg_replace( '#[^A-Z0-9\-\_\[\]]#i', '', JRequest::getVar('e_name') ); ?>");
		window.parent.SqueezeBox.close();
		return false;
	}
	/**
	*	Debug Function, that works like print_r for Objects in Javascript
	*/
	function var_dump(obj) {
		var vartext = "";
		for (var prop in obj) {
			if( isNaN( prop.toString() )) {
				vartext += "\t->"+prop+" = "+ eval( "obj."+prop.toString()) +"\n";
			}
	    }
	   	if(typeof obj == "object") {
	    	return "Type: "+typeof(obj)+((obj.constructor) ? "\nConstructor: "+obj.constructor : "") + "\n" + vartext;
	   	} else {
	      	return "Type: "+typeof(obj)+"\n" + vartext;
		}
	}
</script>
<script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>
<style type="text/css">
.roundedb { margin-bottom: 10px;
  padding: 10px;
  border: 1px solid #c0c0c0;
  border-radius: 5px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
 }
.gdmessage {
	background: url(<?php echo $app_path ?>/templates/<?php echo $mainframe->getTemplate() ?>/images/notice-info.png) no-repeat;
	padding: 3px 3px 3px 30px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 1.091em;
	background-color: #eee;
	border: 1px solid #e0e0e0;
	margin-bottom: 10px;
	font-weight: bold;
	color: #333;
} </style>
</head>
<body>
	<div style="width:100%;font-size: 3em;" align="center" style="display:none;" id="authenticationDiv">
		<h2><img src="checked_out.png" alt="lock" align="bottom" />
		<a href="#" id="authenticationLink" title="1. <?php echo JText::_('SIGN_IN') ?>"></a></h2>
	</div>
	<div id="loadingContainer" style="width:100%;text-align: middle;"><img src="googledocs_indicator.gif" alt="-*-"  align="middle" /> <?php echo JText::_( 'Loading') ?></div>
	<div style="width:100%;opacity:0.3;" id="windowContent">
	<hr />
	<h1>&nbsp;&nbsp;&nbsp;<img src="googledocs.gif" alt="GoogleDocs" align="middle" />&nbsp;&nbsp;
	<?php echo JText::_('EMBEDGOOGLEDOCS_FORM_TITLE') ?></h1>
	<form class="roundedb">
		<table style="margin-left: 15px;width:90%;" class="admintable" id="mainContentTable">
			<tr>
				<td width="30%" class="key" align="right">
					<label for="title"><?php echo JText::_( 'Document_Type' ); ?></label>
				</td>
				<td width="70%"><?php echo $doctypelist ?>	</td>
			</tr>
			<tr style="display:none;" id="document_entries_row">
				<td class="key" align="right"><?php echo JText::_( 'Select_a_Document' ); ?></td>
				<td>
					<select name="document_entries" id="document_entries" onchange="setDocId(getDocId(this.getSelected()[0].value))"><option value=""><?php echo JText::_( 'Select' ); ?></option></select>
					<span id="statusContainer"><img src="googledocs_indicator.gif" alt="-*-"  align="middle" /> <?php echo JText::_( 'Loading') ?></span>
				</td>
			</tr>
			<tr>
				<td class="key" align="right">
					<label for="alias"><?php echo JText::_( 'Document_ID' ); ?></label>
				</td>
				<td>
					<input type="text" id="docid" name="docid" size="50" class="inputbox" />
				</td>
			</tr>
			<tr>
				<td class="key" align="right"><label for="size"><?php echo JText::_( 'Size' ); ?></label></td>
				<td><?php echo $sizelist ?></td>
			</tr>
			<tr>
				<td class="key" align="right"><?php echo JText::_( 'Height' ). ' x '.JText::_( 'Width' ); ?></td>
				<td><input type="text" id="height" name="height" size="5" class="inputbox" value="<?php echo $pluginParams->get('iframe_height_custom') ?>" /> x
				<input type="text" id="width" name="width" size="5" class="inputbox" value="<?php echo $pluginParams->get('iframe_width_custom') ?>" /></td>
			</tr>
			<tr>
				<td class="key" align="right"><label for="frameborder"><?php echo JText::_( 'Frameborder' ); ?></label></td>
				<td><?php echo $frameborderlist ?></td>
			</tr>
			<tr>
			<td colspan="2">
			<br /><br />
		<input type="button" style="font-size: 1.2em;background-color:#FED11E;" class="button" onclick="insertGoogleDoc();" value="<?php echo JText::_( 'Insert_Placeholder' ); ?>" id="insertBtn" />
		</td>
		</table>
		</form>
		<div class="gdmessage"><?php echo jtext::_('GDOCS_PUBLISH_MESSAGE') ?></div>
	</div>
</body>
</html>