<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
?>
<?php
// no direct access

defined( '_JEXEC' ) or die( 'Restricted access' );
if (!class_exists('fancyboxClass')) {
	class fancyboxClass extends MegaPopupHelper{
		// Modal name
		var $_modal_name;
		// Plugin params
		var $_pluginParams;
		// Param in {ytpopup} tag
		var $_tagParams;
		
		// Constructor
		function __construct($pluginParams){
			parent::__construct("fancybox", $pluginParams);
			$this->_modal_name = "fancybox";
			$this->_pluginParams = $pluginParams;
           
		}
		
		/**
		 * Get Library for FancyBox
		 * @param 	Array	$pluginParams	Plugin paramaters
		 * @return 	String	Include JS, CSS string.
		 * */
		function getHeaderLibrary($bodyString){			
			// Base path string
            
            $hs_baseScript    = JURI::base().'plugins/system/plg_ytmegapopup/assets/js/'.$this->_modal_name.'/';
            
            $hs_baseCss    = JURI::base().'plugins/system/plg_ytmegapopup/assets/css/'.$this->_modal_name.'/';
			// Tag array
			$headtag    = array();
			$headtag[] = '<script src="'.$hs_baseScript.'ytc.jquery-1.5.min.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'jquery.easing.1.3.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'jquery.fancybox-1.2.1.js" type="text/javascript" ></script>';
			$headtag[] = '<link href="'.$hs_baseCss.'jquery.fancybox.css" type="text/css" rel="stylesheet" />';
			 
			$bodyString = parent::getHeaderLibrary($bodyString, '/ytc.jquery-1.5.min.js', $headtag);
			
			return $bodyString;
		}

		/**
		 * Get content to display in Front-End.
		 * @param 	Array	$paras	Key and value in {ytpopup} tag
		 * @return 	String	HTML string to display
		 * */
		function getContent($paras, $content){
			$arrData = parent::getCommonValue($paras, $content);
			
			// Generate random id
			$ranID = rand(0,10000);
			// To standard content
			$content = html_entity_decode($content);
			
			// Proccess group tag
			$modalGroup 	= $this->getValue("group");
			if(!empty($modalGroup))
				$relGroup = ' rel ="ytmega'.$modalGroup.'"';
			else{
				$relGroup = '';
				$modalGroup = $ranID;
			}
			$arrData['rel'] 	= $relGroup;
			$arrData['group'] 	= $modalGroup;
			$arrData['class'] 	= "fancybox".$modalGroup;
			$arrData['frameWidth'] 	= $this->_pluginParams->get("width", "500");
			$arrData['frameHeight'] 	= $this->_pluginParams->get("height", "500");
			$arrData['zoomSpeedIn'] 	= $this->_pluginParams->get("fancyboxspeedin", "500");
			$arrData['zoomSpeedOut'] 	= $this->_pluginParams->get("fancyboxspeedout", "500");
			$arrData['overlayShow'] 	= $this->_pluginParams->get("overlay", "1");
			$arrData['overlayOpacity'] 	= $this->_pluginParams->get("overlay_opacity", "0.7");	
			$arrData["imageScale"] 		= $this->_pluginParams->get("fancyboximageScale", "1");
			$arrData["centerOnScroll"] 	= $this->_pluginParams->get("fancyboxcenterOnScroll", "1");			
			
			$type = $this->getValue("type");
            
			$str = "";
            if($type == "ajax"){
                $str .= $this->showDataInTemplate("fancybox", "ajax", $arrData); 
            }elseif($type == "iframe"){
                $arrData['group'] = "fancybox".$arrData['group'];
                $arrData['class'] .= " iframe";
                $str .= $this->showDataInTemplate("fancybox", "iframe", $arrData); 
            }elseif($type == "inline"){
                $arrData['href'] = "#".$arrData['href'];
                $str .= $this->showDataInTemplate("fancybox", "ajax", $arrData);
            }elseif($type == "image"){
                $str .= $this->showDataInTemplate("fancybox", "image", $arrData);
            }elseif($type == "slideshow"){
                if($arrData['imageNumber'] == "one")
                $arrData['imageNumber'] = $content;
				$arrData['class'] 	= "group".$ranID;
				$arrData['rel'] 	= "slideshow".$ranID;
				$str .= $this->showDataInTemplate("fancybox", "slideshow", $arrData);
            }elseif($type == "youtube"){
                $arrData['YoutubeLink']	= str_replace("&amp;", "&", $arrData['href']);
				$arrData['YoutubeLink']	= str_replace("&", "&amp;", $arrData['href']);
				$arrData['href'] 		= "youtubeID".$ranID;
				$arrData['useragent'] 	= $this->get_user_browser();
				$str .= $this->showDataInTemplate("fancybox", "youtube", $arrData);
            }
			// Return value string.
			return $str;
		}	
	}
}
?>