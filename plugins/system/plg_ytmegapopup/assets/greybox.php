<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
if (!class_exists('greyboxClass')) {
	class greyboxClass extends MegaPopupHelper{
		// Modal name
		var $_modal_name;
		// Plugin params
		var $_pluginParams;
		// Param in {ytpopup} tag
		var $_tagParams;
		// Constructor
		function __construct($pluginParams){
			parent::__construct("greybox", $pluginParams);
			$this->_modal_name = "greybox";
			$this->_pluginParams = $pluginParams;
		}
				
		/**
		 * Get Library for GreyBox
		 * @param 	Array	$pluginParams	Plugin paramaters
		 * @return 	String	Include JS, CSS string.
		 * */
		function getHeaderLibrary($bodyString){				
			// Base path string
			$hs_loadHtml    = JURI::base().'plugins/system/plg_ytmegapopup/assets/';
            $hs_baseScript    = JURI::base().'plugins/system/plg_ytmegapopup/assets/js/'.$this->_modal_name.'/';
            $hs_baseCss    = JURI::base().'plugins/system/plg_ytmegapopup/assets/css/'.$this->_modal_name.'/';
			// Tag array
			$headtag    = array();				
			$headtag[] = '<script type="text/javascript" > var GB_ROOT_DIR = "'.$hs_loadHtml.'"; </script>';
			$headtag[] = '<script src="'.$hs_baseScript.'AJS.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'AJS_fx.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'gb_scripts.js" type="text/javascript" ></script>';			
			$headtag[] = '<link href="'.$hs_baseCss.'gb_styles.css" type="text/css" rel="stylesheet" />';		
			$bodyString = parent::getHeaderLibrary($bodyString, '/AJS.js', $headtag);	
			return $bodyString;
		}
		
		/**
		 * Get content to display in Front-End.
		 * @param 	Array	$paras	Key and value in {ytpopup} tag
		 * @return 	String	HTML string to display
		 * */
		function getContent($paras, $content){			
			$arrData = parent::getCommonValue($paras, $content);
			$modalContent = parent::checkFolder($arrData);
			// Generate random id
			$ranID = time().rand(0,100);
			// To standard content
			$content = html_entity_decode($content);
			
			// Config for GreyBox
			$modalBehavior  = "gb_page".$this->_pluginParams->get("greyboxbehavior");
			$modalGroup 	= $this->getValue("group");
			if(!empty($modalGroup))
				$relGroup = 'gb_pageset['.$modalGroup.']';
			else{
				$relGroup = ''.$modalBehavior.'['.$arrData['frameWidth'].', '.$arrData['frameHeight'].']';
				$modalGroup = $ranID;
			}
			$arrData["rel"] 	= $relGroup;
			$arrData["group"]	= $modalGroup;
			$arrData["overlayShow"]		= $this->_pluginParams->get("overlay");
			$arrData["overlayOpacity"]	= $this->_pluginParams->get("overlay_opacity");
									
			$type = $this->getValue("type");
			$str = "";
			if($type == "ajax"||$type == "iframe"){
                $str .= $this->showDataInTemplate("greybox", "default", $arrData);
			}elseif($type == "image"){
                if(!empty($modalGroup))
					$arrData['rel'] = 'gb_imageset['.$modalGroup.']';
				else{
					$arrData['rel'] = 'gb_imageset['.$ranID.']';
					$modalGroup = $ranID;
				}					
				$str .= $this->showDataInTemplate("greybox", "default", $arrData);
			}elseif($type == "slideshow"){
                $show = false;
				foreach ($modalContent as $k=>$v){
					$image_url = trim($v);
					$arrData['href'] 	= $image_url;
					$arrData['rel'] 	= "gb_imageset[".$ranID."]";
					$arrData['content']	= "";
					
					if($arrData['imageNumber'] == "all"){
						$arrData['content']	= "<img src=\"".$image_url."\" width=\"".$arrData['frameWidth']."\"/>";
					}elseif($show === false){
						$show = true;
						$arrData['content']	= $content;
					}
					$str .= $this->showDataInTemplate("greybox", "default", $arrData);
				}
			}elseif($type == "youtube"){
                $str .= $this->showDataInTemplate("greybox", "default", $arrData);
			}
			// Return value string.
			return $str;
		}
		
	}
}
?>