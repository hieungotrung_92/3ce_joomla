<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!class_exists('highslideClass')) {
	class highslideClass extends MegaPopupHelper{
		// Modal name
		var $_modal_name;
		
		// Plugin params
		var $_pluginParams;
		
		// Param in {ytpopup} tag
		var $_tagParams;
		
		// Constructor
		function __construct($pluginParams){
			parent::__construct("highslide", $pluginParams);
			$this->_modal_name = "highslide";
			$this->_pluginParams = $pluginParams;
		}
		
		/**
		 * Get Library for HighSlide
		 * @param 	Array	$pluginParams	Plugin paramaters
		 * @return 	String	Include JS, CSS string.
		 * */
		function getHeaderLibrary($bodyString){
				
			// Base path string
            $hs_loadHtml    = JURI::base().'plugins/system/plg_ytmegapopup/assets/';
			$hs_baseScript    = JURI::base().'plugins/system/plg_ytmegapopup/assets/js/'.$this->_modal_name.'/';
            $hs_baseCss    = JURI::base().'plugins/system/plg_ytmegapopup/assets/css/'.$this->_modal_name.'/';
			// Tag array
			$headtag    = array();
			$headtag[] = '<script src="'.$hs_baseScript.'do_cookie.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'highslide-full.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'swfobject.js" type="text/javascript" ></script>';
			$headtag[] = "<script type='text/javascript' > 
							hs.graphicsDir = '".$hs_loadHtml."images/'; 
							hs.wrapperClassName = 'draggable-header';										
							</script>";
			
			// CSS		
			$headtag[] = '<link href="'.$hs_baseCss.'highslide.css" type="text/css" rel="stylesheet" />';
			
			$bodyString = parent::getHeaderLibrary($bodyString, '/highslide-full.packed.js', $headtag);
				
			return $bodyString;
		}
		
		/**
		 * Get content to display in Front-End.
		 * @param 	Array	$paras	Key and value in {ytpopup} tag
		 * @return 	String	HTML string to display
		 * */
		function getContent($paras, $content){			
			$arrData = parent::getCommonValue($paras, $content);
			$modalContent = parent::checkFolder($arrData);
			// Generate random id
			$ranID = rand(0,10000);
			// To standard content
			$content = html_entity_decode($content);
			
			// Get common value
								
			$str = "";
			$modalGroup 	= $this->getValue("group");
			if(!empty($modalGroup)){
				$captionId = " , captionId: 'CaptionArea".$modalGroup.date("dHis")."'";
				if(!isset($_SESSION["CaptionArea".$modalGroup.date("dHis")])) {
					$str .= $this->showCaptionArea("CaptionArea".$modalGroup.date("dHis"));
					$_SESSION["CaptionArea".$modalGroup.date("dHis")] = "true";
				}
			}else{
				$captionId = '';
			}
			
			$arrData["outlineType"] = " '".$this->_pluginParams->get("highslideOutline")."' ";
           
			$arrData["group"] 		= $modalGroup;
			$arrData["class"] 		= "highslide".$modalGroup;
			$arrData["expandDuration"] 	= $this->_pluginParams->get("group1-highslide-speed_in");
			$arrData["restoreDuration"] = $this->_pluginParams->get("group1-highslide-speed_out");
			$arrData["captionId"] 		= $captionId;
			
			// Even proccess
			$arrData['eventStr']	= "";
			if($arrData['onopen'] != "" || $arrData['onclose'] != ""){
				$arrData['onopen'] 		= ($arrData['onopen'] != '')?",onOpen: ".$arrData['onopen']:"";
				$arrData['onclose'] 	= ($arrData['onclose'] != '')?",onClose: ".$arrData['onclose']:"'";
				$arrData['eventStr']	= $arrData['onopen'].$arrData['onclose'];
			}
			
			$type = $this->getValue("type");	
			if($type == "ajax"){
                $arrData['objectType'] = "ajax";
				$str .= $this->showDataInTemplate("highslide", "default", $arrData); 
			}elseif($type == "iframe"){
                $arrData['objectType'] = "iframe";
				$str .= $this->showDataInTemplate("highslide", "default", $arrData); 
			}elseif($type == "inline"){
                $str .= $this->showDataInTemplate("highslide", "inline", $arrData);
			}elseif($type == "image"){
                $arrData['class'] = "highslide";
                $str .=  '<a class="highslide" href="'.$arrData['href'].'" onclick="return hs.expand(this, {outlineType:'.$arrData['outlineType'].' '.$arrData['captionId'].'});" >'.$content."</a>";
			}elseif($type == "slideshow"){
                $show = false;
				foreach ($modalContent as $k=>$v){
					$image_url = trim($v);
					$arrData['class'] 	= "highslide";
					$arrData['href'] 	= $image_url;
					$arrData['captionId'] 	= ", captionId: '".$ranID."'";
					$arrData['content']	= "";
					
					if($arrData['imageNumber'] == "all"){
						$arrData['content'] 	= "<img src='".$image_url."' width='".$arrData['frameWidth']."'/>";
					}elseif($show === false){
						$show = true;
						$arrData['content']	= $content;
					}
					$str .=  '<a class="highslide" href="'.$image_url.'" onclick="return hs.expand(this, {outlineType:'.$arrData['outlineType'].', captionId: \'ytmega-highslide'.$ranID.'\'});" >'.$arrData['content']	.'</a>';
				}
				// Caption Area
				$str .= $this->showCaptionArea($ranID);	
			}elseif($type == "youtube"){
                $arrData['objectType'] = "iframe";
				$str .= $this->showDataInTemplate("highslide", "default", $arrData);
			}
			// Return value string.
			return $str;
		}
		
		/**
		 * Show caption area
		 */
		function showCaptionArea($captionID){
			$arrData['captionID'] = "ytmega-highslide".$captionID;
			return $this->showDataInTemplate("highslide", "caption", $arrData);;
		}		
	}
}
?>