<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class MegaPopupHelper extends JObject {
	// Modal name
	var $_modal_name;
	
	// Plugin params
	var $_pluginParams;
	
	// Param in {ytpopup} tag
	var $_tagParams;
	
	// Constructor for PHP 5
	function __construct($modalName, $pluginParams){
		$this->_modal_name = $modalName;
		$this->_pluginParams = $pluginParams;
	}
	
	/**
	 * Get paramater value accross key
	 * @param string $key Key of array
	 * @return string Value of key.
	 * */
	function getValue($key, $defaultValue=""){			
		if(isset($this->_tagParams[$key])){
			return $this->_tagParams[$key];
		}else {				
			return $defaultValue;
		}
	}
	
	// Return common value in {ytpopup} tag and config file
	function getCommonValue($arrYTPopupTag, $innerContent){
		$this->_tagParams = $arrYTPopupTag;
		// Content
		$modalContent 	= $this->getValue("content", "");
		$modalContent	= str_replace("&amp;", "&", $modalContent);
		$modalContent	= str_replace("&", "&amp;", $modalContent);
		// Title
		$modalTitle 	= $this->_pluginParams->get("add_title", "1");
		$modalTitle		= ($modalTitle != "0")?$this->getValue("title"):"";
		// Description
		$modalDesc 	= $this->_pluginParams->get("add_desc", "1");
		$modalDesc	= ($modalDesc != "0")?$this->getValue("description"):"";
		// Slide show
		$imageNumber 		= $this->getValue("show", "");
		if($imageNumber == "")
			$imageNumber 	= $this->_pluginParams->get("image_slideshow", "one");
				
		$arrData = array(
				"frameWidth"	=> $this->getValue("width", 600),
				"frameHeight"	=> $this->getValue("height", 400),
				"content"		=> html_entity_decode($innerContent),
				"title"			=> $modalTitle,
				"desc"			=> $modalDesc,
				"href"			=> $modalContent,
				"onclose"		=> $this->getValue("onclose", ""),
				"onopen"		=> $this->getValue("onopen", ""),
				"imageNumber"	=> $imageNumber,
			);
			
		if( $this->getValue("type") == "slideshow")
			$arrData['content'] = $this->checkFolder($arrData);
		//echo "<pre>";print_r($arrData);die;
		return $arrData;
	}
	
	// Show data in template file
	function showDataInTemplate($modal="fancybox", $type="ajax", $arrData){
		$tmpFile = dirname(dirname( __FILE__ )).DS.'tmpl'.DS.$modal.DS.$type.".php";		
		
		if(file_exists($tmpFile)){
			ob_start();
			require($tmpFile);
			$content = ob_get_contents();
			ob_end_clean();
			return $content;
		}else{
			return "Template file not found.";
		}
	}
	// Return header library 
	function getHeaderLibrary($bodyString, $identifierString, $headerString){
		if ( strpos( $bodyString, $identifierString ) === false  ) {		
			$bodyString = str_replace( '</head>', "\t".implode("\n", $headerString)."\n</head>", $bodyString );
		}
		return $bodyString;
	}
	
	// Return user agent
	function get_user_browser(){
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$ub = '';
		if(preg_match('/MSIE/i',$u_agent)){
			$ub = "ie";
		}elseif(preg_match('/Firefox/i',$u_agent)){
			$ub = "firefox";
		}elseif(preg_match('/Safari/i',$u_agent)){
			$ub = "safari";
		}elseif(preg_match('/Chrome/i',$u_agent)){
			$ub = "chrome";
		}elseif(preg_match('/Flock/i',$u_agent)){
			$ub = "flock";
		}elseif(preg_match('/Opera/i',$u_agent)){
			$ub = "opera";
		}
		return $ub;
	}
	
	// Check input data is Joomla folder and return image array
	// Else return base data
	function checkFolder($arrData){
		jimport('joomla.filesystem.folder');
		$modalContent = array();
		
		if ( JFolder::exists(JPATH_ROOT.DS.$arrData['href']) ){
			$files = JFolder::files(JPATH_ROOT.DS.$arrData['href']);
			if( is_array($files) ){
				foreach ($files as $fileName){
					$info = pathinfo($fileName);
						if( in_array($info['extension'], array("jpg", "jpeg", "bmp", "png")) ){
							$modalContent[] = JURI::root()."/". str_replace(DS, "/", $arrData['href'])."/". $fileName;
						}
				}
			}else break;						
		}else 
			$modalContent = explode(", ", $arrData['href']);
            //var_dump($modalContent);die;
		return $modalContent;
	}
}

?>