<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!class_exists('multiboxClass')) {
	class multiboxClass extends MegaPopupHelper{
		// Modal name
		var $_modal_name;
		
		// Plugin params
		var $_pluginParams;
		
		// Param in {ytpopup} tag
		var $_tagParams;
		
		// Constructor
		function __construct($pluginParams){
			parent::__construct("multibox", $pluginParams);
			$this->_modal_name = "multibox";
			$this->_pluginParams = $pluginParams;
            
		}
		
		/**
		 * Get Library for MultiBox
		 * @param 	Array	$pluginParams	Plugin paramaters
		 * @return 	String	Include JS, CSS string.
		 * */
		function getHeaderLibrary($bodyString){	
		  
			// Base path string
			$hs_baseScript    = JURI::base().'plugins/system/plg_ytmegapopup/assets/js/'.$this->_modal_name.'/';
            $hs_baseCss    = JURI::base().'plugins/system/plg_ytmegapopup/assets/css/'.$this->_modal_name.'/';
			// Tag array
			$headtag    = array();
            //$headtag[] = '<script src="'.$hs_baseScript.'mootools.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'overlay.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'multibox.js" type="text/javascript" ></script>';				
			$headtag[] = '<link href="'.$hs_baseCss.'multibox.css" type="text/css" rel="stylesheet" />';			
			$bodyString = parent::getHeaderLibrary($bodyString, '/multibox.js', $headtag);
				
			return $bodyString;
		}
		
		/**
		 * Get content to display in Front-End.
		 * @param 	Array	$paras	Key and value in {ytpopup} tag
		 * @return 	String	HTML string to display
		 * */
		function getContent($paras, $content){			
			$arrData = parent::getCommonValue($paras, $content);
            $modalContent = parent::checkFolder($arrData);
            
            //var_dump($modalContent);die;
			// Generate random id
			$ranID = rand(0,10000);
			// To standard content
			$content = html_entity_decode($content);
			
			$eventStr	= "";
			if($arrData['onopen'] != "" || $arrData['onclose'] != ""){
				$arrData['onopen'] = ($arrData['onopen'] != '')?",onOpen: ".$arrData['onopen']:"";
				$arrData['onclose'] = ($arrData['onclose'] != '')?",onClose: ".$arrData['onclose']:"'";
				$eventStr	= $arrData['onopen'].$arrData['onclose'];
			}
			
			$str = "";
			$modalGroup 	= $this->getValue("group");
			if(!empty($modalGroup)){
				$classGroup = 'ytmega'.$modalGroup;
				$str .= '<script type="text/javascript" > if(! box'.$modalGroup.' ) {var box'.$modalGroup.' = {}; window.addEvent("domready", function(){ box'.$modalGroup.' = new MultiBox("ytmega'.$modalGroup.'", {descClassName: "multiBoxDesc", useOverlay: '.$this->_pluginParams->get("overlay").', contentColor: "'.$this->_pluginParams->get("url('../../images/fancybox/").'", showControls: '.$this->_pluginParams->get("multiboxshowControls").' '.$eventStr.' }); }); } </script>';
			}else{
				$classGroup = 'ytmega'.$ranID;
				$str .= '<script type="text/javascript" > var box'.$ranID.' = {}; window.addEvent("domready", function(){ box'.$ranID.' = new MultiBox("ytmega'.$ranID.'", {descClassName: "multiBoxDesc", useOverlay: '.$this->_pluginParams->get("overlay").', contentColor: "'.$this->_pluginParams->get("url('../../images/fancybox/").'", showControls: '.$this->_pluginParams->get("multiboxshowControls").' '.$eventStr.' }); }); </script>';
			}
			$arrData["group"] 	= $modalGroup;
			$arrData["id"] 		= "mb".$ranID;
			$arrData["class"] 	= $classGroup;
            $arrData["multiboxborderColor"] 	= $this->_pluginParams->get("multiboxborderColor", "1");
            $arrData["multiboxborderthickness"] 	= $this->_pluginParams->get("multiboxborderthickness", "20");
            $arrData["multiboxbgcontrol"] 	= $this->_pluginParams->get("multiboxbgcontrol", "#FF8880");
            
            $arrData["bordercolor"] = " '".$this->_pluginParams->get("url('../../images/multibox/")."' ";
			$type = $this->getValue("type");
            if($type == "ajax" || $type == "iframe"){
                $arrData['rel'] = "type:iframe,width:".$arrData["frameWidth"].",height:".$arrData["frameHeight"];
				$str .= $this->showDataInTemplate("multibox", "default", $arrData);
            }elseif($type == "inline"){
                $arrData['href'] = "#".$arrData['href'];
				$arrData['rel'] = "type:element,width:".$arrData["frameWidth"].",height:".$arrData["frameHeight"];
				$str .= $this->showDataInTemplate("multibox", "default", $arrData);
            }elseif($type == "image"){
                $arrData['rel'] = "type:jpg";
				$str .= $this->showDataInTemplate("multibox", "default", $arrData);
            }elseif($type == "slideshow"){
                $show = false;					
				foreach ($modalContent as $k=>$v){
					$image_url = trim($v);			
					$arrData['rel'] = "type:jpg";
					$arrData['href'] = $image_url;
					$arrData['content'] = "";
					
					if($arrData['imageNumber'] == "all"){
					  //echo $arrData['content'];die;
						$arrData['content'] = "<img src=\"".$image_url."\" width=\"".$arrData["frameWidth"]."\"/>";
					}elseif($show === false){
						$show = true;
						$arrData['content']	= $content;
					}					
					
					$str .= $this->showDataInTemplate("multibox", "default", $arrData);
				}
            }elseif($type == "youtube"){
                $arrData['rel'] = "type:swf";
				$str .= $this->showDataInTemplate("multibox", "default", $arrData);
            }
			// Return value string.
			return $str;
		}	
	}
}
?>