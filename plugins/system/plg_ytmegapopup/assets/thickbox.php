<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

if (!class_exists('thickboxClass')) {
	class thickboxClass extends MegaPopupHelper{
		// Modal name
		var $_modal_name;
		
		// Plugin params
		var $_pluginParams;
		
		// Param in {ytpopup} tag
		var $_tagParams;
		
		// Constructor
		function __construct($pluginParams){
			parent::__construct("thickbox", $pluginParams);
			$this->_modal_name = "thickbox";
			$this->_pluginParams = $pluginParams;
		}
		
		/**
		 * Get Library for Thickbox
		 * @param 	Array	$pluginParams	Plugin paramaters
		 * @return 	String	Include JS, CSS string.
		 * */
		function getHeaderLibrary($bodyString){
			// Base path string
            $hs_baseScript    = JURI::base().'plugins/system/plg_ytmegapopup/assets/js/'.$this->_modal_name.'/';
            $hs_baseCss    = JURI::base().'plugins/system/plg_ytmegapopup/assets/css/'.$this->_modal_name.'/';
			// Tag array
			$headtag    = array();				
			$headtag[] = '<script src="'.$hs_baseScript.'jquery.js" type="text/javascript" ></script>';
			$headtag[] = '<script src="'.$hs_baseScript.'thickbox.js" type="text/javascript" ></script>';
			$headtag[] = '<script type="text/javascript" >if (jQuery && jQuery.noConflict) jQuery.noConflict( );</script>';
			$headtag[] = '<link href="'.$hs_baseCss.'thickbox.css" type="text/css" rel="stylesheet" />';
			
			$bodyString = parent::getHeaderLibrary($bodyString, '/thickbox.js', $headtag);
				
			return $bodyString;
								
		}
		
		/**
		 * Get content to display in Front-End.
		 * @param 	Array	$paras	Key and value in {ytpopup} tag
		 * @return 	String	HTML string to display
		 * */
		function getContent($paras, $content){			
			$arrData = parent::getCommonValue($paras, $content);
			$modalContent = parent::checkFolder($arrData);
			// Generate random id
			$ranID = time().rand(0,100);
			$content = html_entity_decode($content);
			
			$modalGroup 	= $this->getValue("group");
            $arrData["thickboxborderColor"] 	= $this->_pluginParams->get("thickboxborderColor", "1");
            $arrData["thickboxborderthickness"] 	= $this->_pluginParams->get("thickboxborderthickness", "5");
            $arrData["thickboxbackground"]	= $this->_pluginParams->get("thickboxbackground", "#FF8880");
			if(!empty($modalGroup))
				$relGroup = 'ytmega'.$modalGroup;
			else{
				$relGroup = '';
			}
			$arrData['group'] 	= $modalGroup;
			$arrData['rel'] 	= $relGroup;
			
			$eventStr	= "";
			if($arrData['onopen'] != "" || $arrData['onclose'] != ""){
				$arrData['onopen'] = ($arrData['onopen'] != '')?"&amp;onOpen=".$arrData['onopen']:"";
				$arrData['onclose'] = ($arrData['onclose'] != '')?"&amp;onClose=".$arrData['onclose']:"'";
				$eventStr	= $arrData['onopen'].$arrData['onclose'];
			}
			
			$type = $this->getValue("type");
			$str = "";
			
			if($type == "ajax"){
				$arrData['href'] = $arrData['href']."?height=".$arrData['frameHeight']."&amp;width=".$arrData['frameWidth'].$eventStr;
				$str .= $this->showDataInTemplate("thickbox", "default", $arrData);
			}elseif($type == "iframe"){
				$arrData['href'] =  $arrData['href']."?keepThis=true&amp;TB_iframe=true&amp;height=".$arrData['frameHeight']."&amp;width=".$arrData['frameWidth'].$eventStr;
				$str .= $this->showDataInTemplate("thickbox", "default", $arrData);
			}
			elseif($type == "inline"){
				$arrData['href'] =  "#TB_inline?height=".$arrData['frameHeight']."&amp;width=".$arrData['frameWidth']."&amp;inlineId=".$arrData['href'].$eventStr;
				$str .= $this->showDataInTemplate("thickbox", "default", $arrData);
			}
			elseif($type == "image"){
				$arrData['href'] =  $arrData['href']."?keepThis=true&amp;TB_iframe=true".$eventStr;
				$str .= $this->showDataInTemplate("thickbox", "default", $arrData);
			}
			elseif($type == "slideshow"){
				$show = false;					
				foreach ($modalContent as $k=>$v){
					$image_url = trim($v);
					$arrData['href'] = $image_url;
					$arrData['rel'] = "gallery-plants";
					$arrData['content'] = "";
					
					if($arrData['imageNumber'] == "all"){
						$arrData['content'] = "<img src=\"".$image_url."\" width=".$arrData['frameWidth']."/>";
					}elseif($show === false){
						$show = true;
						$arrData['content']	= $content;
					}					
					
					$str .= $this->showDataInTemplate("thickbox", "default", $arrData);
				}										
			}
			elseif($type == "youtube"){
				$arrData['YouTubeLink'] = $arrData['href'];
				$arrData['YouTubeID']	= "youtube".$ranID;
				$arrData['href'] = "#TB_inline?height=".$arrData['frameHeight']."&amp;width=".$arrData['frameWidth']."&amp;inlineId=youtube".$ranID.$eventStr;
				$str .= $this->showDataInTemplate("thickbox", "youtube", $arrData);
			}
			// Return value string.   
			return $str;
		}		
	}
}
?>