<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<a <?php echo $arrData['rel'];?> class="<?php echo $arrData['class']; ?>"  href="<?php echo $arrData['href']; ?>" title="<?php echo $arrData['title'] ?>" ><?php echo $arrData['content'] ?></a>
<?php if ($arrData['rel'] != ''){ ?>
	<script language="javascript" type="text/javascript"> 
		$jYtc(document).ready(function() {
		if( ! $jYtc("a.<?php echo $arrData['group']; ?>").fancybox({
					hideOnContentClick: false,
					<?php if($arrData['onopen'] != "") echo "callbackOnShow: ".$arrData['onopen'].","; ?>
					<?php if($arrData['onclose'] != "") echo "callbackOnClose: ".$arrData['onclose'].","; ?>
					zoomSpeedIn: <?php echo $arrData['zoomSpeedIn']; ?>,
					zoomSpeedOut: <?php echo $arrData['zoomSpeedOut']; ?>,
					overlayShow: <?php echo $arrData['overlayShow']; ?>,
					overlayOpacity: <?php echo $arrData['overlayOpacity']; ?>,
					centerOnScroll: <?php echo $arrData['centerOnScroll']; ?>,
					frameWidth: <?php echo $arrData['frameWidth']; ?>,
					frameHeight: <?php echo $arrData['frameHeight']; ?> }) ) 
			$jYtc("a.<?php echo $arrData['group']; ?>").fancybox();
		});
		</script>
<?php 
	}else{
	?>
		<script language="javascript" type="text/javascript"> 
			$jYtc(document).ready(function() {
			$jYtc("a.<?php echo $arrData['group']; ?>").fancybox({
						hideOnContentClick: false,
						<?php if($arrData['onopen'] != "") echo "callbackOnShow: ".$arrData['onopen'].","; ?>
						<?php if($arrData['onclose'] != "") echo "callbackOnClose: ".$arrData['onclose'].","; ?>
						zoomSpeedIn: <?php echo $arrData['zoomSpeedIn']; ?>,
						zoomSpeedOut: <?php echo $arrData['zoomSpeedOut']; ?>,
						overlayShow: <?php echo $arrData['overlayShow']; ?>,
						overlayOpacity: <?php echo $arrData['overlayOpacity']; ?>,
						centerOnScroll: <?php echo $arrData['centerOnScroll']; ?>,
						frameWidth: <?php echo $arrData['frameWidth']; ?>,
						frameHeight: <?php echo $arrData['frameHeight']; ?> })
			});
			</script>
		<?php
	}
?>