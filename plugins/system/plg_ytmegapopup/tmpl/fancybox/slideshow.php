<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<?php
	$show = false;
	foreach ($arrData['content'] as $v){
	$image_url = trim($v);
?>
<a class="<?php echo $arrData['class']; ?>" rel="<?php echo $arrData['rel']; ?>"  href="<?php echo $image_url; ?>" title="<?php echo $arrData['title'] ?>" >
<?php if($arrData['imageNumber'] == "all"){ ?>
	<img src="<?php echo $image_url; ?>" width="<?php echo $arrData['frameWidth']; ?>"/>
<?php }elseif($show === false){ ?>
	<?php $show = true; echo $arrData['imageNumber']; ?>
<?php } ?>
</a>
<?php
	}
?>
<script language="javascript" type="text/javascript"> 
$jYtc(document).ready(function() {
	$jYtc("a.<?php echo $arrData['class']; ?>").fancybox({
		imageScale:<?php echo $arrData['imageScale']?>,
		zoomSpeedIn:<?php echo $arrData['zoomSpeedIn']?>,
		zoomSpeedOut:<?php echo $arrData['zoomSpeedOut']?>,
		frameWidth:<?php echo $arrData['frameWidth']?>,
		frameHeight:<?php echo $arrData['frameHeight']?>,
		<?php if($arrData['onopen'] != "") echo "callbackOnShow: ".$arrData['onopen'].","; ?>
		<?php if($arrData['onclose'] != "") echo "callbackOnClose: ".$arrData['onclose'].","; ?>
		centerOnScroll: <?php echo $arrData['centerOnScroll']; ?>});
});
</script>