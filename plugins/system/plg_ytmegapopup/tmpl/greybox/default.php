<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<a class="<?php echo $arrData['onopen'];?>,<?php echo $arrData['onclose'];?>" href="<?php echo $arrData['href'];?>" title="<?php echo $arrData['title'];?>" rel="<?php echo $arrData['rel'];?>" ><?php echo $arrData['content'];?></a>