<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<a href="<?php echo $arrData['href'];?>" title="<?php echo $arrData['title'];?>" onclick="return hs.htmlExpand(this, {headingText: '<?php echo $arrData['title'];?>', restoreDuration: '<?php echo $arrData['restoreDuration'];?>', expandDuration: '<?php echo $arrData['expandDuration'];?>', objectType: '<?php echo $arrData['objectType'];?>', width:<?php echo $arrData['frameWidth'];?>, height: <?php echo $arrData['frameHeight'];?>, outlineType:<?php echo $arrData['outlineType'];?> <?php echo $arrData['captionId'];?><?php echo $arrData['eventStr'];?>});" ><?php echo $arrData['content'];?></a>