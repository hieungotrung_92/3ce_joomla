<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<style type="text/css">
<!--
	.MultiBoxContainer{
	   border: <?php echo $arrData["multiboxborderthickness"];?>px <?php echo $arrData["multiboxborderColor"];?> solid !important;
	}
    .MultiBoxControls{
        background-color:<?php echo $arrData["multiboxbgcontrol"];?> !important;
    }
-->
</style>
<a class="<?php echo $arrData['class'];?>" id="<?php echo $arrData['id'];?>" title="<?php echo $arrData['title'];?>" href="<?php echo $arrData['href'];?>" rel="<?php echo $arrData['rel'];?>" ><?php echo $arrData['content'];?></a>
<div style="" class="multiBoxDesc <?php echo $arrData['id'];?>"><?php echo $arrData['desc'];?></div>