<?php
/*------------------------------------------------------------------------
# Plugin YT Mega Popup - Version 1.0
# Copyright (C) 2010-2011 The YouTech Company. All Rights Reserved.
# @license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Author: The YouTech Company
# Websites: http://www.ytcvn.com
-------------------------------------------------------------------------*/
// Prevent direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<style type="text/css">
<!--
	#TB_window{
        background: none repeat scroll 0 0 <?php echo $arrData["thickboxbackground"];?>;
        border: <?php echo $arrData["thickboxborderthickness"];?>px solid <?php echo $arrData["thickboxborderColor"];?>;
	}
-->
</style>
<a rel="<?php echo $arrData['rel'];?>" class="thickbox" title="<?php echo $arrData['title'];?>" href="<?php echo $arrData['href'];?>" ><?php echo $arrData['content'];?></a>