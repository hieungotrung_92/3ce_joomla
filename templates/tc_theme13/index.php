<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language;?>" >
<head>

<meta name="yandex-verification" content="fcbc48b975a430f3" />

<!-- Google mail verify 3celectric.com of BinhLT-->
<meta name="google-site-verification" content="g09S-w7Or-_Q4_r_uezGWD7olW_JkYiw7RGRfmzqF6M" />
<!-- End Google mail verify 3celectric.com -->

<!-- RPC setup in robots.txt -->
<meta http-equiv="content-language" content="vi">
<link rel="pingback" href="http://www.3ce.vn/xmlrpc.php" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- End RPC setup -->

<!-- Google Master tool -->
<meta name="google-site-verification" content="8Qyq-DH66Hka4_ywyOq800mcE61txLQ9pihI0YWcH1M" />
<!-- End Google Master tool -->





</head>
<?php
/* @package     tc_theme13 Template
 * @author    Themescreative http://www.themescreative.com
 * @copyright  Copyright (c) 2006 - 2012 themescreative. All rights reserved
 */
defined( '_JEXEC' ) or die( 'Restricted access' );
if(!defined('DS')){define('DS',DIRECTORY_SEPARATOR);}
$tcParams = '';
include_once(dirname(__FILE__).DS.'tclibs'.DS.'head.php');
include_once(dirname(__FILE__).DS.'tclibs'.DS.'settings.php');
$tcParams .= '<body id="tc" class=" logo-'.$logo.'">';
$tcParams .= '<div id="tc_wrapper" class="tc_wrapper">';
$tcParams .= TCShowModule('adverts', 'tc_xhtml', 'container');
$tcParams .=  TCShowModule('header', 'tc_xhtml', 'container');
include_once(dirname(__FILE__).DS.'tclibs'.DS.'slider.php');
include_once(dirname(__FILE__).DS.'tclibs'.DS.'social.php');
$tcParams .=  TCShowModule('slider', 'tc_xhtml', 'container');
$tcParams .=  TCShowModule('top', 'tc_xhtml', 'container');
$tcParams .=  TCShowModule('info', 'tc_xhtml', 'container');
$tcParams .=  TCShowModule('maintop', 'tc_xhtml', 'container');
$tcParams .= '<main class="tc_main container clearfix">'.$component.'</main>';
$tcParams .=  TCShowModule('mainbottom', 'tc_xhtml', 'container').
TCShowModule('feature', 'tc_xhtml', 'container').
TCShowModule('bottom', 'tc_xhtml', 'container').
TCShowModule('footer', 'tc_xhtml', 'container');
$tcParams .= '<footer class="tc_wrapper_copyright tc_section">'.
'<div class="container clearfix">'.
'<div class="col-md-12">'.($copyright ? '<div style="padding:10px;">'.$cpright.' </div>' : ''). ''.($show_Designed ? '<div style="padding-bottom:10px; text-align:right; ">Designed by <a href="http://www.themescreative.com/" title="Visit themescreative.com!" target="blank">themescreative.com</a></div>' : '').
'</div>'.
'</div>'.
'</footer>';
$tcParams .='</div>';     
include_once(dirname(__FILE__).DS.'tclibs'.DS.'debug.php');
?>
<!-- Facebook -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.4&appId=460655357424981";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!-- google plus -->
<script src="https://apis.google.com/js/platform.js" async defer>
  {lang: 'vi'}
</script>

<?php
$tcParams .='</body>';

$tcParams .='</html>';
echo $tcParams;
?>