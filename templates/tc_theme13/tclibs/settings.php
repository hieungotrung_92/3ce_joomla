<?php
defined('_JEXEC') or die('Restricted access');
///  Renderer modules  //
function TCShowModule($name, $style, $customClass = '', $compos = -1){
$modmodule = $modstep = $showlogo = $showfont = $showmenu = $tcExtPosition = $tcDesktop = $tcTablet = $tcPhone = $tcCustomClass = '';
jimport( 'joomla.application.module.helper' );
$customParams = JFactory::getApplication()->getTemplate(true)->params;
$doc = JFactory::getDocument();
$app = JFactory::getApplication();

//// MENU /////
$menucontrol = $customParams->get("menucontrol", 1);
$menu_name = $customParams->get("menutype", "mainmenu");
$startLevel = $customParams->get("startLevel", 0);
$endLevel = $customParams->get("endLevel", 10);
$renderer	= $doc->loadRenderer('module');
$module = JModuleHelper::getModule('mod_menu', '$menu_name');
$attribs['style'] = 'none';
$module->params	= "menutype=$menu_name\nshowAllChildren=1\nstartLevel=$startLevel\nendLevel=$endLevel\nclass_sfx=tc_nav tc-menu\ntag_id=tcdefaultmenu";
if($menucontrol == 1)
$showmenu = '<div id="tc_main_menu" class="tc_menufix clearfix">'.$renderer->render($module, $attribs).'</div>';


//// LOGO /////
if($customParams->get('logo') == 2)
$showlogo .= '<a id="tc_logo" href="'.JURI::root().'"><h1>'.$customParams->get('logotext').'</h1><span>'.$customParams->get('slogan').'</span></a>';
elseif($customParams->get('logo') == 1)
$showlogo .= '<a id="tc_logo" href="'.JURI::root().'"><img src="'.$customParams->get('logoimage').'" alt="Logo" /></a>';
else
$showlogo .= '<a id="tc_logo" href="'.JURI::root().'"><img src="'.JURI::root().'templates/'.$app->getTemplate().'/images/logo.png" alt="Logo" /></a>';


//// MODULES /////
$logopos = $customParams->get('logopos', 'header1');
$menupos = $customParams->get('menupos', 'header2');
$posValue = explode("|",$customParams->get($name, '1|'.$name+'1:12/0/0/0/'));
$posCount = $posValue[0];
$posName = (isset($posValue[1]) ? $posValue[1] : $name+'1:12/0/0/0/');
$modulecount = 0;
for ($i = 0; $i < $posCount; $i++) {
$tcCountPositions = explode(",", $posName);
$tcCountPosition = (isset($tcCountPositions[$i])) ? $tcCountPositions[$i] : '';
if($tcCountPosition != ''){
$tcPositionName = explode(":", $tcCountPosition);
$tcPositionNameValue = $tcPositionName[0];
if(isset($tcPositionName[1]) != ''){
$tcExtPosition = explode("/", $tcPositionName[1]);
if (isset($tcExtPosition[0])) $tcPositionNameGrid = $tcExtPosition[0];
if (isset($tcExtPosition[1])) $tcDesktop = $tcExtPosition[1];
if (isset($tcExtPosition[2])) $tcTablet = $tcExtPosition[2];
if (isset($tcExtPosition[3])) $tcPhone = $tcExtPosition[3];
if (isset($tcExtPosition[4])) $tcCustomClass = $tcExtPosition[4];
}
}
if (count(JModuleHelper::getModules($tcPositionNameValue)) || $logopos == ($tcPositionNameValue) || $menupos == ($tcPositionNameValue) || ($i == $compos && $compos >= 0)) :
$modmodule .='<div class="col-md-'.$tcPositionNameGrid.''.($tcDesktop ? 'hidden-desktop hidden-md hidden-lg' : '').''.($tcTablet ? 'hidden-tablet hidden-sm' : '').''.($tcPhone ? 'hidden-phone hidden-xs' : '').' '.$tcCustomClass.''.($i > 0 ? 'separator_'.$name : '').''.(($i == $compos && $compos >= 0) ? 'tc_component':'tc_block').'">';
$modmodule .=($logopos == $tcPositionNameValue ? $showlogo : '').($menupos == $tcPositionNameValue ? $showmenu : '');
$modmodule .=(($i == $compos && $compos >= 0) ? '<jdoc:include type="message" /><jdoc:include type="component" />' : '');
if (count(JModuleHelper::getModules($tcPositionNameValue)))
$modmodule .='<jdoc:include type="modules" name="'.$tcPositionNameValue.'" style="'.$style.'" />';
$modmodule .='</div>';
$modulecount = $modulecount + 1;
endif;
}
if($modulecount > 0)
$modmodule = '<section class="tc_wrapper_'.$name.' tc_section"><div class="'.$customClass.' tc_group"><div id="tc_'.$name.'" class="tc_'.$name.' row-fluid clearfix">'
.$modmodule.
'</div></div></section>';

return $modmodule;
}
///  Cookies  //
$cookie_prefix = $this->template;
$cookie_time = time()+30000000;
$tc_temp = array('TemplateStyle','Layout');
foreach ($tc_temp as $tprop) {
$tc_session = JFactory::getSession();

if (isset($_REQUEST[$tprop])) {
$$tprop = JRequest::getString($tprop, null, 'get');
$tc_session->set($cookie_prefix.$tprop, $$tprop);
setcookie ($cookie_prefix. $tprop, $$tprop, $cookie_time, '/', false);   
global $$tprop; 
}
}
jimport( 'joomla.application.module.helper' );
$customParams = JFactory::getApplication()->getTemplate(true)->params;
$pageview = JRequest::getVar('view', '');
$pageoption = JRequest::getVar('option', '');
$pageID = JRequest::getVar('Itemid', '');
$slides	     = $this->params->get('slides');
$template_baseurl = $this->baseurl.'/templates/'.$this->template;
$Default_TemplateStyle 	= $this->params->get("TemplateStyle", "style1");
$Default_Layout	= $this->params->get("layout", "lbr");
$copyright = $this->params->get("copyright", 1);
$cpright = $this->params->get("cpright", "");
$show_Designed = $this->params->get("show_Designed", 1);
$IE6Warning = $this->params->get("IE6Warning", 1);
$GoogleAnalytics = $this->params->get("GoogleAnalytics", 0);
$googcode = $this->params->get("googcode", '');
$logo = $this->params->get("logo", 0);
$menuTrigger = $this->params->get("menuTrigger", "hover");
$menuEffect = $this->params->get("menuEffect", "fade");
$menuStick = $this->params->get("menuStick", 1);
//FEATURES
$selectors = $this->params->get("selectors", '');
$fontSquirrel = $this->params->get("fontSquirrel", '');
$linkGoogle = $this->params->get("linkGoogle", '');
$fontGoogle = $this->params->get("fontGoogle", '');
$h1_font_size = $this->params->get('h1_font_size');
$h2_font_size = $this->params->get('h2_font_size');
$h3_font_size = $this->params->get('h3_font_size');
$h4_font_size = $this->params->get('h4_font_size');
$h5_font_size = $this->params->get('h5_font_size');
$h6_font_size = $this->params->get('h6_font_size');	
$fontfamily = $this->params->get("fontfamily", 'Arial, Helvetica, sans-serif');
$fontsize = $this->params->get("fontsize", '12px');
$Menufontsize = $this->params->get("Menufontsize", '');
$menucontrol = $this->params->get("menucontrol", 1);
$totop = $this->params->get("totop", 1);
$totopText = $this->params->get("totopText", "Go to Top");
$responsive = $this->params->get("responsive", 1);
$jquery = $this->params->get("jquery", 0);
$document	= JFactory::getDocument();
$jversion = new JVersion;
$CustomStyle = $customParams->get("CustomStyle", "");
$HeadColor = $customParams->get("HeadColor", "");
$TemlateBgr = $customParams->get("TemlateBgr", "");
$TopColor = $customParams->get("TopColor", "");
$TopTextColor = $customParams->get("TopTextColor", "");
$BottomTextColor = $customParams->get("BottomTextColor", "");
$BottomColor = $customParams->get("BottomColor", "");
$MenuColor = $customParams->get("MenuColor", "");
$MenuTextColor = $customParams->get("MenuTextColor", "");
$TempWidth = $customParams->get("TempWidth", "");
$elasticslideshow_SlideStyle= $this->params->get('elasticslideshow_SlideStyle','');
$elasticslideshow_AutoPlay= $this->params->get('elasticslideshow_AutoPlay','');
$elasticslideshow_Interval= $this->params->get('elasticslideshow_Interval','');
$elasticslideshow_S_Easing= $this->params->get('elasticslideshow_S_Easing','');
$elasticslideshow_T_Easing= $this->params->get('elasticslideshow_T_Easing','');
$elasticslideshow_T_Speed= $this->params->get('elasticslideshow_T_Speed','');
$document->addStyleSheet($template_baseurl.'/css/bootstrap/css/bootstrap.css');
$document->addStyleSheet($template_baseurl.'/tclibs/menus/css/menu.css');
$document->addStyleSheet($template_baseurl.'/css/template.css');
$document->addStyleSheet($template_baseurl.'/css/styles/'.$customParams->get('TemplateStyle').'.css');

//// FONT /////
if($customParams->get('font') == 2)
{ $document->addStyleSheet($linkGoogle);
$document->addStyleDeclaration(''. $selectors .' { font-family: ' . $fontGoogle . ', Arial, sans-serif; }');}
elseif($customParams->get('font') == 1)
{$document->addStyleSheet($template_baseurl.'/fonts/' . $fontSquirrel . '/stylesheet.css');
$document->addStyleDeclaration(''. $selectors . '{ font-family: ' . $fontSquirrel . ', Arial, sans-serif; }');}

if($h1_font_size !='') { $document->addStyleDeclaration('h1 { font-size: '.$h1_font_size.' !important;}');	}
if($h2_font_size !='') { $document->addStyleDeclaration('h2 { font-size: '.$h2_font_size.' !important;}');	}
if($h3_font_size !='') { $document->addStyleDeclaration('h3 { font-size: '.$h3_font_size.' !important;}');	}
if($h4_font_size !='') { $document->addStyleDeclaration('h4 { font-size: '.$h4_font_size.' !important;}');	}
if($h5_font_size !='') { $document->addStyleDeclaration('h5 { font-size: '.$h5_font_size.' !important;}');	}
if($h6_font_size !='') { $document->addStyleDeclaration('h6 { font-size: '.$h6_font_size.' !important;}');	}
if($TempWidth !='') { $document->addStyleDeclaration('.container { max-width: '.$TempWidth.' !important;}');	}
if($CustomStyle !='') { $document->addStyleDeclaration('
.itemComments a,.itemCategory a,
.btn-primary:hover, .btn.btn-primary:hover,
h4 a:hover,.fa-angle-double-right, a:hover .fa-angle-double-right,
.pagination > li > a,.pagination > li > a:hover,
.blog h2 a:hover, a:hover, a:focus,a, ul.latestnews li a:hover,
ul.menu li a:hover, ul.weblinks li a:hover, ul.menu li.active.current a, 
ul.menu li.parent a:hover, ul.menu li.active li.active.curren a,
.btn-link,.btn-link:hover,.page-header h2 a,
.main-tc-menu li.mega-hover > .sub-container.mega > a, 
.main-tc-menu li.mega-hover > .sub-container.mega > a:hover, 
.main-tc-menu .sub-container li.active > a,
.main-tc-menu li.mega-hover > .sub-container.mega > a, 
.main-tc-menu li.mega-hover > .sub-container.mega > a:hover, 
.main-tc-menu .sub-container li.mega-hover > a,
.main-tc-menu .sub-container li.current.active > a,
.active.parent .sub-container li.active > a,
.menutc_nav .current.active > a,
.menutc_nav > .active > a:focus{ color:'.$CustomStyle.';}
.tc-menu-wrapper > ul.main-tc-menu > li.current > a, 
.menutc_nav > .current.active > a, 
.main-tc-menu > li.mega-hover > a, 
.main-tc-menu > li.mega-hover > a:hover, 
.menutc_nav > .active > a, 
.menutc_nav > .active > a:hover, 
.menutc_nav > .active > a:focus,
.main-tc-menu > li.mega-hover > a, 
.main-tc-menu > li.mega-hover > a:hover, 
.menutc_nav > .current.active > a,
.menutc_nav > .active > a, 
.menutc_nav > .active > a:hover, 
.menutc_nav > .active > a:focus,
#comment-form .btn.btn-primary:after,.btn.btn-primary:after, .ei_slider_thumbs, .tc_wrapper_adverts { background: '.$CustomStyle.' !important;}
.btn.btn-primary, #comment-form .btn.btn-primary{ border:1px solid '.$CustomStyle.';}');	}
if($TemlateBgr !='') { $document->addStyleDeclaration('#tc { background-color: '.$TemlateBgr.' !important;}');	}
if($HeadColor !='') { $document->addStyleDeclaration('.tc_wrapper_slider { background: '.$HeadColor.' !important;}');	}
if($TopColor !=''|| $TopTextColor !='') { $document->addStyleDeclaration('.tc_wrapper_top {color: '.$TopTextColor.'; background: '.$TopColor.';}');	}
if($MenuColor !='') { $document->addStyleDeclaration('.tc_wrapper_header { background: '.$MenuColor.' !important;}');	}
if($MenuTextColor !='') { $document->addStyleDeclaration('.main-tc-menu > li > a { color: '.$MenuTextColor.'}');	}
if($Menufontsize !='' ) { $document->addStyleDeclaration('.main-tc-menu > li > a {font-size:'.$Menufontsize.' !important;}');	}
if($BottomColor !='' || $BottomTextColor !='' ) { $document->addStyleDeclaration('.tc_wrapper_bottom { color: '.$BottomTextColor.'; background: '.$BottomColor.';}');	}
$document->addStyleDeclaration('body#tc{font-family:'.$fontfamily.' !important; font-size:'.$fontsize.' !important;}');	
$document->addStyleSheet($template_baseurl.'/css/font-awesome/css/font-awesome.min.css');
if(!file_exists(JPATH_SITE.DS.'components'.DS.'com_k2'.DS.'helpers'.DS.'route.php')) {
$document->addStyleSheet($template_baseurl.'/css/k2.css');
}
if ($responsive == 1) 
$document->addStyleSheet($template_baseurl.'/css/responsive.css');
else $document->addStyleSheet($template_baseurl.'/css/fixed.css');
if ($jquery == 1) 
$document->addScript($template_baseurl.'/tclibs/helper/jquery-1.7.2.min.js');
if ($jquery == 2){
if (version_compare($jversion->getShortVersion(), '3.0.0', '<'))
$document->addScript($template_baseurl.'/tclibs/helper/jquery-1.7.2.min.js');
}
$document->addScript($template_baseurl.'/css/bootstrap/js/bootstrap.min.js');
$document->addScript($template_baseurl.'/tclibs/helper/browser-detect.js');
if ($menucontrol == 1) {
$document->addScript($template_baseurl.'/tclibs/menus/jquery.hoverIntent.minified.js');
$document->addScript($template_baseurl.'/tclibs/menus/jquery.menu.js');
}
if(($this->countModules('slider') && $slides == 2) || ($slides == 1)){ 
$document->addStyleSheet($template_baseurl.'/slider/css/style.css');
$document->addScript($template_baseurl.'/slider/js/jquery.easing.min.js');
$document->addScript($template_baseurl.'/slider/js/jquery.eislideshow.min.js');

$document->addScriptDeclaration('
jQuery(document).ready(function($){
$(function() {
$("#elasticslideshow").eislideshow({
animation			: "'.$elasticslideshow_SlideStyle.'",
autoplay			: '.$elasticslideshow_AutoPlay.',
slideshow_interval	: '.$elasticslideshow_Interval.',
titlesFactor		: 0.60,
easing				: "'.$elasticslideshow_S_Easing.'",
titleeasing			: "'.$elasticslideshow_T_Easing.'",
titlespeed			: '.$elasticslideshow_T_Speed.'
});
});
});
');

}

if ($totop == 1) 
$document->addScript($template_baseurl.'/tclibs/helper/scrolltotop.js');
if ($totop == 1) 
$document->addScriptDeclaration("
jQuery(document).ready(function() {
jQuery(document.body).SLScrollToTop({
'text':			'$totopText',
'title':		'$totopText',
'className':	'scrollToTop',
'duration':		500
});
});");
if ($menucontrol == 1) 
$document->addScriptDeclaration("
var tcDefaultMenu = jQuery.noConflict();
jQuery(document).ready(function(){
jQuery('#tcdefaultmenu').oMenu({
theme: 'default-menu',
effect: '$menuEffect',
mouseEvent: '$menuTrigger'
});
});");
require_once (dirname(__FILE__).DS.'calls.php');
require_once (dirname(__FILE__).DS.'browsers.php');
?>


